#include "AchievementScene.h"
#include "Definition.h"
#include "SpriteSheetLoader.h"
#include "AppDelegate.h"
#include "MainMenuScene.h"
#include "AudioEngine.h"

cocos2d::Scene * AchievementScene::createScene()
{
	// scene is an autorelease object
	auto scene = Scene::create();

	// layer is an autorelease object
	auto layer = AchievementScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}


bool AchievementScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	//background
	auto backgroundSprite_1 = Sprite::create("scrollingBackGround.png");
	backgroundSprite_1->setAnchorPoint(Vec2(0, 0));
	SpriteSheetLoader::RescaleToScreenSize(backgroundSprite_1);
	this->addChild(backgroundSprite_1);

	//back button
	backButton = Button::create("backLogo.png");
	backButton->setPosition(Vec2(origin.x + backButton->getContentSize().width / 1.2f,
		visibleSize.height - backButton->getContentSize().height / 2.0f));
	backButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::ReturnToMainMenu, this));
	SpriteSheetLoader::RescaleToScreenSize(backButton);
	this->addChild(backButton);

	if (AchievementManager::bombTrophyUnlocked)
		BombTrophyButton = Button::create("bombtrophy.png");
	else
		BombTrophyButton = Button::create("locked.png");
	BombTrophyButton->setPosition(Vec2(visibleSize.width / 5.0f, visibleSize.height / 2.0f));
	SpriteSheetLoader::RescaleToScreenSize(BombTrophyButton);
	BombTrophyButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::DisplayBombTrophyText, this));
	this->addChild(BombTrophyButton);

	if (AchievementManager::skullTrophyUnlocked)
		SkullTrophyButton = Button::create("skulltrophy.png");
	else
		SkullTrophyButton = Button::create("locked.png");
	SkullTrophyButton->setPosition(Vec2(visibleSize.width / 5.0f + (float)BombTrophyButton->getContentSize().width, visibleSize.height / 2.0f));
	SpriteSheetLoader::RescaleToScreenSize(SkullTrophyButton);
	SkullTrophyButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::DisplaySkullTrophyText, this));
	this->addChild(SkullTrophyButton);

	if (AchievementManager::bossTrophyUnlocked)
		BossTrophyButton = Button::create("bosstrophy.png");
	else
		BossTrophyButton = Button::create("locked.png");
	BossTrophyButton->setPosition(Vec2(visibleSize.width / 5.0f + (float)BombTrophyButton->getContentSize().width + BossTrophyButton->getContentSize().width, visibleSize.height / 2.0f));
	SpriteSheetLoader::RescaleToScreenSize(BossTrophyButton);
	BossTrophyButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::DisplayBossTrophyText, this));
	this->addChild(BossTrophyButton);

	resetButton = Button::create("reset.png");
	resetButton->setScale(resetButton->getScaleX() / 5, resetButton->getScaleY() / 5);
	resetButton->setPosition(Vec2(visibleSize.width - (resetButton->getContentSize().width / 2 * resetButton->getScaleX()), visibleSize.height - (resetButton->getContentSize().height / 2 * resetButton->getScaleY())));
	SpriteSheetLoader::RescaleToScreenSize(resetButton);
	resetButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::ResetAchievements, this));
	this->addChild(resetButton);

	achievementText = Text::create("Achievement Locked","fonts/Marker Felt.ttf", 12);
	achievementText->setPosition(Vec2(visibleSize.width / 2.0f, visibleSize.height / 10.0f));
	achievementText->setAnchorPoint(Vec2(0.5f, 0.5f));
	achievementText->setTextHorizontalAlignment(TextHAlignment::CENTER);
	achievementText->setTextVerticalAlignment(TextVAlignment::CENTER);
	//achievementText->setTextColor(Color4B(1, 0, 0, 1));
	achievementText->setVisible(false);
	
	
	SpriteSheetLoader::RescaleToScreenSize(achievementText);
	this->addChild(achievementText);

	//Run update loop
	this->scheduleUpdate();

	return true;
}

void AchievementScene::update(float dt)
{

}

void AchievementScene::ReturnToMainMenu(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);
	//Scale down the button when is pressed
	if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {

		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, false);
	}
	//Scale down the button when is not pressed
	else if (cocos2d::ui::Widget::TouchEventType::ENDED == eventType)
	{
		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, true);

		auto scene = MainMenuScene::createScene();
		Director::getInstance()->replaceScene(TransitionSlideInL::create(TRANSIT_TIME, scene));
	}
}

void AchievementScene::DisplayBombTrophyText(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	if (AchievementManager::bombTrophyUnlocked)
	{
		achievementText->setText("Achievement Unlocked: Plant a bomb once");
		achievementText->setVisible(true);
	}
	else
	{
		achievementText->setText("Plant a bomb once to unlock");
		achievementText->setVisible(true);
	}
}

void AchievementScene::DisplaySkullTrophyText(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	if (AchievementManager::skullTrophyUnlocked)
	{
		achievementText->setText("Achievement Unlocked: Kill 1 enemy");
		achievementText->setVisible(true);
	}
	else
	{
		achievementText->setText("Kill 1 enemy to unlock");
		achievementText->setVisible(true);
	}
}

void AchievementScene::DisplayBossTrophyText(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	if (AchievementManager::bossTrophyUnlocked)
	{
		achievementText->setText("Achievement Unlocked: Kill the boss");
		achievementText->setVisible(true);
	}
	else
	{
		achievementText->setText("Kill the boss to unlock");
		achievementText->setVisible(true);
	}
}

void AchievementScene::ResetAchievements(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	UserDefault::getInstance()->setBoolForKey("bombtrophy", false);
	UserDefault::getInstance()->setBoolForKey("skulltrophy", false);
	UserDefault::getInstance()->setBoolForKey("bosstrophy", false);
	AchievementManager::bombTrophyUnlocked = false;
	AchievementManager::skullTrophyUnlocked = false;
	AchievementManager::bossTrophyUnlocked = false;

	achievementText->setVisible(false);

	BombTrophyButton->setVisible(false);
	BombTrophyButton = NULL;
	BombTrophyButton = Button::create("locked.png");
	BombTrophyButton->setPosition(Vec2(visibleSize.width / 5.0f, visibleSize.height / 2.0f));
	SpriteSheetLoader::RescaleToScreenSize(BombTrophyButton);
	BombTrophyButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::DisplayBombTrophyText, this));
	this->addChild(BombTrophyButton);

	SkullTrophyButton->setVisible(false);
	SkullTrophyButton = NULL;
	SkullTrophyButton = Button::create("locked.png");
	SkullTrophyButton->setPosition(Vec2(visibleSize.width / 5.0f + (float)BombTrophyButton->getContentSize().width, visibleSize.height / 2.0f));
	SpriteSheetLoader::RescaleToScreenSize(SkullTrophyButton);
	SkullTrophyButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::DisplaySkullTrophyText, this));
	this->addChild(SkullTrophyButton);

	BossTrophyButton->setVisible(false);
	BossTrophyButton = NULL;
	BossTrophyButton = Button::create("locked.png");
	BossTrophyButton->setPosition(Vec2(visibleSize.width / 5.0f + (float)BombTrophyButton->getContentSize().width + (float)SkullTrophyButton->getContentSize().width, visibleSize.height / 2.0f));
	SpriteSheetLoader::RescaleToScreenSize(BossTrophyButton);
	BossTrophyButton->addTouchEventListener(CC_CALLBACK_2(AchievementScene::DisplayBossTrophyText, this));
	this->addChild(BossTrophyButton);
}
