#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class AchievementScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	void update(float dt);

	void ReturnToMainMenu(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);
	void DisplayBombTrophyText(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);
	void DisplaySkullTrophyText(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);
	void DisplayBossTrophyText(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);
	void ResetAchievements(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

	//implement the "static create()" method manually
	CREATE_FUNC(AchievementScene);

private:
	Size visibleSize;
	Vec2 origin;

	Button * backButton;
	Button * BombTrophyButton;
	Button * SkullTrophyButton;
	Button * BossTrophyButton;
	Button * resetButton;
	Text * achievementText;

};