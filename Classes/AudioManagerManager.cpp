#include "AudioManagerManager.h"


//std::string AudioManagerManager::CurrentBGM = "";
//std::string AudioManagerManager::CurrentSFX = "";

float AudioManagerManager::GlobalBGMvalue = 0.0f;
float AudioManagerManager::GlobalSFXvalue = 0.0f;

vector<int> AudioManagerManager::BGM_IDs = {};
vector<int> AudioManagerManager::SFX_IDs = {};

int AudioManagerManager::tempid = 0;

bool AchievementManager::bombTrophyUnlocked = false;
bool AchievementManager::skullTrophyUnlocked = false;
bool AchievementManager::bossTrophyUnlocked = false;

float AchievementManager::achieve_timer = 0.0f;
float AchievementManager::achieve_countdown = 4.0f;

bool AchievementManager::bomb_yay = false;
bool AchievementManager::skull_yay = false;
bool AchievementManager::boss_yay = false;