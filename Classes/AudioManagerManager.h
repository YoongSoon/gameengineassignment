#pragma once

#include <string>
#include <vector>

using std::vector;
//enum AudioManagerEnums{
//	AUDIO_ID_BGM = 0,
//	AUDIO_ID_SFX,
//
//};


class AudioManagerManager {

public:
	//static std::string CurrentBGM;
	//static std::string CurrentSFX;
	static float GlobalBGMvalue; // value range 0 to 1
	static float GlobalSFXvalue; // value range 0 to 1
	static vector<int> BGM_IDs; //store ids of all BGMs
	static vector<int> SFX_IDs; //store ids of all SFX
	static int tempid;
};

class AchievementManager {

public:
	static bool bombTrophyUnlocked;
	static bool skullTrophyUnlocked;
	static bool bossTrophyUnlocked;
	static float achieve_timer;
	static float achieve_countdown;
	static bool bomb_yay;
	static bool skull_yay;
	static bool boss_yay;
};
