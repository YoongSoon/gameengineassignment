#include "EnemyBase.h"
#include "MapHandler.h"
#include "EnemyPlaceBomb.h"
#include "EnemyChomping.h"
#include "EnemyBoss.h"

void EnemyBase::Init()
{
	// Render enemy on the map
	m_Map->AddToMap(this);
	//Set the current tile position
	m_curr = m_Map->GetTilePosFromPos(getPosition());

	theStrategy = new EnemyStrategy();
	theStrategy->Init();

	//Set the map handler in the EnemyStrategy class
	theStrategy->SetMapHandler(m_Map);

	//set name as an identification
	setName("Enemy");

	m_visited.resize(m_Map->MAP_WIDTH * m_Map->MAP_HEIGHT, false);
	std::fill(m_visited.begin(), m_visited.end(), false);

}

void EnemyBase::update(float dt)
{

	if (m_Health <= 0)
	{
		//indicate is dead
		b_isDead = true;

		//sto sprite animation action and moving action when enemy dies
		stopAllActions();
		//remove the physic body to prevent any collison detection and response
		if(getPhysicsBody() != nullptr)
		removeComponent(getPhysicsBody());

		//when the sprite is fully transparent , destroy the enemy
		if (RunDeathEffect(dt) == true) 
		{
			DestroyMe();
			return;
		}
		if (!AchievementManager::skullTrophyUnlocked)
			AchievementManager::skull_yay = true;
		UserDefault::getInstance()->setBoolForKey("skulltrophy", true);
		AchievementManager::skullTrophyUnlocked = true;
	}
	else
	{
		m_invulnerableElapsedTime += dt;

		//Run the cool down to turn enemy from invulnerable to vulnerable
		if (b_isInvulnerable == true)
		{
			if (m_invulnerableElapsedTime >= m_NextVulnerableTime)
			{
				//reset all the variables
				setOpacity(255);
				b_isInvulnerable = false;
			}
			else {
				setOpacity(std::abs(sinf(m_invulnerableElapsedTime * 5)) * 255); // Fade in and out
			}

		}
	}
}

void EnemyBase::SetHealth(int amount)
{
	m_Health = amount;
}

void EnemyBase::SetDamage(int amount)
{
	m_Damage = amount;
}

void EnemyBase::SetSpeed(float amount)
{
	m_speed = amount;
}



void EnemyBase::TakeDamage(int amount)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/enemyouch.mp3", false, AudioManagerManager::GlobalSFXvalue);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	if (b_isInvulnerable == false)
	{
		m_Health -= amount;
		b_isInvulnerable = true;
	}


	m_NextVulnerableTime = m_invulnerableElapsedTime + 3; // Invulerable for X seconds
}


bool EnemyBase::RunDeathEffect(float dt)
{
	if (m_defaultOpacity > 20)
	{
		m_defaultOpacity -= OPACITY_FACTOR * dt;
		setOpacity(m_defaultOpacity);
		return false;
	}
	else
	{
		setOpacity(0);
		return true;
	}

}

void EnemyBase::ClearPath()
{
	for (auto it = m_path.begin(); it != m_path.end();) {
		it = m_path.erase(it);
	}

	m_path.clear();

}

void EnemyBase::ClearStack()
{
	for (auto it = m_stack.begin(); it != m_stack.end();) {
		it = m_stack.erase(it);
	}

	m_stack.clear();
}

void EnemyBase::SetDirectionAnimation(ENEMY_DIR dir)
{
}

void EnemyBase::CaculateTheNewAnimationDir()
{
	//caculate the difference in TilePos between prev curr and next curr
	TilePos diff = TilePos(m_path[0].x - m_curr.x, m_path[0].y - m_curr.y);

	//set the next curr direction of enum
	if (diff.x < 0)
	{
		m_SpriteCurrentDir = DIR_LEFT;
	}
	else if (diff.x > 0)
	{
		m_SpriteCurrentDir = DIR_RIGHT;
	}
	else if (diff.y > 0)
	{
		m_SpriteCurrentDir = DIR_UP;
	}
	else if (diff.y < 0)
	{
		m_SpriteCurrentDir = DIR_DOWN;
	}
}

bool EnemyBase::onContactBegin(PhysicsContact & contact)
{
	auto collideA = contact.getShapeA()->getBody()->getNode();
	auto collideB = contact.getShapeB()->getBody()->getNode();

	if (collideA->getName() == "Enemy" && collideB->getName() == "explosion") {
		static_cast<EnemyBase*>(collideA)->TakeDamage(1);
		return false; // Ignore collision response
	}

	// Ignore collision response  between enemy and powerup
	if ((collideA->getName() == "Enemy" && collideB->getName() == "PowerUp") || (collideB->getName() == "Enemy"  && collideA->getName() == "PowerUp")) {
		return false; 
	}
	if ((collideA->getName() == "Enemy" && collideB->getName() == "Enemy") || (collideB->getName() == "Enemy"  && collideA->getName() == "Enemy")) {
		return false;
	}


	if (collideB->getName() == "Enemy"  && collideA->getName() == "explosion") {
		static_cast<EnemyBase*>(collideB)->TakeDamage(1);
		return false; // Ignore collision response
	}

	return true;
}

EnemyBase * Create::Enemy(MapHandler* map, const EnemyBase::ENEMY_TYPE _enemyType, const Vec2 pos)
{
	EnemyBase * theEnemy = nullptr;

	switch (_enemyType)
	{
	case EnemyBase::ENEMY_TYPE::BOMB_PLACING_ENEMY:
	{
		theEnemy = new EnemyPlaceBomb(map);
		theEnemy->InitGO("EnemyPlaceBombDown.png");
		theEnemy->SetHealth(2);

	}
	break;
	case EnemyBase::ENEMY_TYPE::CHOMPING_ENEMY:
	{
		theEnemy = new EnemyChomping(map);
		theEnemy->InitGO("EnemyChompDown.png");
		theEnemy->SetHealth(2);
	}
	break;
	case EnemyBase::ENEMY_TYPE::BOSS_ENEMY:
		theEnemy = new EnemyBoss(map);
		theEnemy->InitGO("fidgetspinner.png");
		theEnemy->SetHealth(5);

	}
	theEnemy->InitSize(pos, ZORDER_PLAYER);
	theEnemy->SetDamage(1);
	theEnemy->SetSpeed(20.0f);
	theEnemy->Init();

	return theEnemy;
}
