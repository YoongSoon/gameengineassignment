#pragma once

#include "cocos2d.h"
using namespace cocos2d;

#include "GameObject.h"
#include "EnemyStrategy.h"

#include <vector>
using std::vector;

struct TilePos;
class MapHandler;

class EnemyBase : public GameObject
{
public:
	enum ENEMY_TYPE
	{
		BOMB_PLACING_ENEMY,
		CHOMPING_ENEMY,
		BOSS_ENEMY,
	};

	enum ENEMY_DIR
	{
		DIR_UP,
		DIR_DOWN,
		DIR_LEFT,
		DIR_RIGHT,
	};

	EnemyBase(MapHandler* map) :GameObject(map) {};
	virtual ~EnemyBase() {}; //allow deletion an instance of derived class through base pointer

	virtual void Init();
	virtual void update(float dt);

	//Setters/Getters
	void SetHealth(int amount);
	void SetDamage(int amount);
	void SetSpeed(float amount);
	float GetSpeed() { return m_speed; }
	

	//Current Vec2 position of the enemy
	Vec2 pos;
	//Current tile that the enemy is in
	TilePos m_curr;
	//Container that stores the TilePos for enemy to move to
	vector<TilePos> m_path;

	//Previous tile that the enemy is in
	TilePos m_prev;

	vector<TilePos> m_stack;
	vector<bool> m_visited;

	EnemyStrategy * theStrategy;

	//Player deals damage to enemy
	void TakeDamage(int amount);

	//clear m_path
	void ClearPath();
	//clear m_stack
	void ClearStack();

	//Set the up, down ,right and left animation
	virtual void SetDirectionAnimation(ENEMY_DIR dir);
	//caculate the next current enum direction
	virtual void CaculateTheNewAnimationDir();

	//bool to indicate death
	bool b_isDead = false;

	//collision response
	bool onContactBegin(PhysicsContact& contact);

	ENEMY_DIR m_SpriteCurrentDir;
protected:
	//Stats
	int m_Damage;
	int m_Health;
	float m_speed;

	// enum to differentiate different enemy tpye
	ENEMY_TYPE m_EnemyType; 


	float m_timePast = 0.0f;

	//The rest duration before the enemy starts to move
	const float REST_DURATION = 3.5f;
	float m_restElaspedTime = 3.5f;

	//to indicate whether enemy can be damaged
	bool b_isInvulnerable = false;
	 float m_NextVulnerableTime = 0.0f;
	float m_invulnerableElapsedTime = 0.0f;

	// used to decrease/increse alpha of sprite from time to time
	const float ALTER_ALPHA_DURATION = 0.5f;
	float m_alphaElpasedTime = 0.5f;

	//Decrease the alpha(opacity) of the sprite to transparent when the enemy health reaches 0
	//return bool which if is true , indicates the sprite is fully transparent
	bool RunDeathEffect(float dt);

	// the factor that determines how fast the opacity goes down in the Function: RunDeathEffect
	const float OPACITY_FACTOR = 100.0f;
	//store default opacity
	float m_defaultOpacity = 255;


};

namespace Create
{
	EnemyBase * Enemy(MapHandler* map ,const EnemyBase::ENEMY_TYPE _enemyType , const Vec2 pos);


};