#include "EnemyBoss.h"
#include "SpriteSheetLoader.h"
#include "StateMachine.h"
#include "StateDFS.h"
#include "StateFireMissile.h"
#include "StateEscape.h"

void EnemyBoss::Init()
{
	EnemyBase::Init();

	auto m_PhysicsBody = PhysicsBody::createBox(Size(4.0f, 4.0f), PhysicsMaterial(1, 0, 1));
	m_PhysicsBody->setPositionOffset(Vec2(3, 3));
	m_PhysicsBody->setRotationEnable(false);
	m_PhysicsBody->setDynamic(false);
	m_PhysicsBody->setContactTestBitmask(true);
	this->addComponent(m_PhysicsBody);

	// Collision response
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(EnemyBase::onContactBegin, this);
	getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	m_stateMachine = new StateMachine();
	m_stateMachine->AddState(new StateDFS("DFS", this));
	m_stateMachine->AddState(new StateFireMissile("Fire_missle", this));
	m_stateMachine->AddState(new StateEscape("Escape", this));

	//First state of EnemyBoss
	m_stateMachine->SetNextState("DFS");

	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyBossMoveLeftSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyBossMoveRightSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyBossMoveUpSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyBossMoveDownSheet.plist");

	SetDirectionAnimation(DIR_DOWN);
}

void EnemyBoss::update(float dt)
{
	//call base class update
	EnemyBase::update(dt);

	//if current state is DFS
	if (m_stateMachine->GetCurrentState() == "DFS")
	{
		//run the sprite movement animation , if it has been stopped in StateFireMissile
		if (getNumberOfRunningActions() == 0)
			SetDirectionAnimation(m_SpriteCurrentDir);


		m_elapsedPrepareTime += dt;
		//After a certain duration has elasped during DFS state
		// change to StateFireMissile
		if (m_elapsedPrepareTime >= FIRING_COOL_DOWN)
		{
			AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/MissileLaunch.mp3", false, AudioManagerManager::GlobalSFXvalue);
			if (AudioManagerManager::tempid != -1)
				AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

			m_stateMachine->SetNextState("Fire_missle");
			m_elapsedPrepareTime = 0.0f;
		}
	}
	else if (m_stateMachine->GetCurrentState() == "Fire_missle")
	{
		//Ensure missile is fired before starting the count timer to change state
		if (static_cast<StateFireMissile*>(m_stateMachine->GetCurrentStatePointer())->b_missileFired == true)
		{
			m_restElaspedTime += dt;

			//After remaining stationary for a certain time , return to DFS state
			if (m_restElaspedTime >= STATIONARY_COOL_DOWN)
			{
				m_stateMachine->SetNextState("DFS");
				m_restElaspedTime = 0.0f;
			}
		}	
	}


	// If the enemy is in is a danger zone ( bomb or bomb blast tile) and either current path is empty or new bomb has been placed 
	// by enemy or player, then  find the shortest path (BFS state) to move out of the danger zone;
	if (theStrategy->IsInDangerZone(this) == true && (m_path.empty() == true || EnemyStrategy::b_isANewBombPlaced == true))
	{
		//unable to do BFS if the enemy is currently in  StateFireMissile
		if (m_stateMachine->GetCurrentState() != "Fire_missle")
		{
			//Clear any leftover TilePos in m_path;
			ClearPath();

			//Clear m_stack of DFS
			ClearStack();

			//Rum the BFS limit
			TilePos targetPoint = theStrategy->FindEscapeTargetPoint(this);
			theStrategy->BFSLimit(this, targetPoint);

			//Set the next state to StateEscape
			m_stateMachine->SetNextState("Escape");

			EnemyStrategy::b_isANewBombPlaced = false;
			return;
		}
	}
	else  if (theStrategy->IsInDangerZone(this) == false && m_path.empty() == true)
	{
		if (m_stateMachine->GetCurrentState() != "DFS")
		{
			//Cool down before switching to DFS state
			m_elapsedChangingTime += dt;
			if (m_elapsedChangingTime >= CHANGE_COOL_DOWN)
			{
				m_stateMachine->SetNextState("DFS");
				m_elapsedChangingTime = 0.0f;
			}
		}
	}

	m_stateMachine->Update(dt);
}

void EnemyBoss::SetDirectionAnimation(ENEMY_DIR dir)
{
	stopAllActions();
	Vector<SpriteFrame*> frames;

	switch (dir)
	{
	case DIR_UP:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyBossMoveUp", 3);
		break;
	case DIR_DOWN:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyBossMoveDown", 3);
		break;
	case DIR_RIGHT:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyBossMoveRight", 3);
		break;
	case DIR_LEFT:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyBossMoveLeft", 3);
		break;

	}
	for (int i = 0; i < frames.size(); ++i)
	{
		//the sprite orginally is not aligned centerally , so offset the sprite by abit
		frames.at(i)->setAnchorPoint(Vec2(0.1f,0.1f));
	}

	auto animation = Animation::createWithSpriteFrames(frames, 1.0f / 3);
	auto  animate = Animate::create(animation);
	auto spriteAction = runAction(RepeatForever::create(animate));
	spriteAction->setTag(11);
}

void EnemyBoss::CaculateTheNewAnimationDir()
{
	//if current state is DFS
	if (m_stateMachine->GetCurrentState() == "DFS")
	{
		//caculate the difference in TilePos between prev curr and next curr
		TilePos diff = TilePos( m_curr.x - m_prev.x, m_curr.y - m_prev.y);

		//set the next curr direction of enum
		if (diff.x < 0)
		{
			m_SpriteCurrentDir = DIR_LEFT;
		}
		else if (diff.x > 0)
		{
			m_SpriteCurrentDir = DIR_RIGHT;
		}
		else if (diff.y > 0)
		{
			m_SpriteCurrentDir = DIR_UP;
		}
		else if (diff.y < 0)
		{
			m_SpriteCurrentDir = DIR_DOWN;
		}
	}
	//if current state is escape
	else if (m_stateMachine->GetCurrentState() == "Escape")
	{
		//caculate the difference in TilePos between prev curr and next curr
		TilePos diff = TilePos(m_path[0].x - m_curr.x, m_path[0].y - m_curr.y);

		//set the next curr direction of enum
		if (diff.x < 0)
		{
			m_SpriteCurrentDir = DIR_LEFT;
		}
		else if (diff.x > 0)
		{
			m_SpriteCurrentDir = DIR_RIGHT;
		}
		else if (diff.y > 0)
		{
			m_SpriteCurrentDir = DIR_UP;
		}
		else if (diff.y < 0)
		{
			m_SpriteCurrentDir = DIR_DOWN;
		}
	}

}






