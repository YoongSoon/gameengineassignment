#pragma once

#include "EnemyBase.h"

class StateMachine;

class EnemyBoss : public EnemyBase
{
public:
	EnemyBoss(MapHandler* map) :EnemyBase(map) { };
	~EnemyBoss() {};

	virtual void Init();
	virtual void update(float dt);

	virtual void SetDirectionAnimation(ENEMY_DIR dir);
	virtual void CaculateTheNewAnimationDir();
private:
	StateMachine * m_stateMachine;

	const float FIRING_COOL_DOWN = 10.0f;
	float m_elapsedPrepareTime = 0.0f;

	float m_elapsedStationaryTime = 0.0f;
	const float STATIONARY_COOL_DOWN = 10.0f;

	const float CHANGE_COOL_DOWN = 5.0f;
	float m_elapsedChangingTime = 0.0f;
};