#include "EnemyChomping.h"
#include "SpriteSheetLoader.h"


void EnemyChomping::Init()
{
	EnemyBase::Init();

	// Init Physics Body
	auto m_PhysicsBody = PhysicsBody::createCircle(3, PhysicsMaterial(1, 0, 1));
	m_PhysicsBody->setRotationEnable(false);
	m_PhysicsBody->setDynamic(false);
	m_PhysicsBody->setContactTestBitmask(true);
	this->addComponent(m_PhysicsBody);


	// Collision response
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(EnemyBase::onContactBegin, this);
	getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	//Add sprite sheet into cache
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyChompMoveDownSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyChompMoveUpSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyChompMoveLeftSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyChompMoveRightSheet.plist");

	SetDirectionAnimation(DIR_DOWN);

}

void EnemyChomping::update(float dt)
{
	//call base class update
	EnemyBase::update(dt);

	if (m_Health <= 0)
	{
		return;
	}

	//If the player is near , follow the player
	if (theStrategy->IsPlayerNear(this , 0.3f) == true && m_path.empty() == true)
	{
		TilePos playerTilePos = m_Map->GetTilePosFromPos(theStrategy->GetPlayerInstance()->GetPlayerSprite()->getPosition());
		theStrategy->BFSLimit(this, playerTilePos, 1000);
	}
	// else move randomly
	else if(m_path.empty() == true)
	{
		theStrategy->MoveRandomly(this);
	}

	if (m_path.empty() == false && getNumberOfRunningActions() == 1)
	{
		// caculate the next current enum direction
		CaculateTheNewAnimationDir();

		m_curr = m_path[0];

		//Set the Vec2 pos of the sprite
		pos = m_Map->m_Map[m_curr.x][m_curr.y].tileSprite->getPosition();

		//set the current sprite animation 
		SetDirectionAnimation(m_SpriteCurrentDir);

		//Move action 
		float timeToMove = (pos - getPosition()).length() / m_speed;
		auto moveEvent = MoveBy::create(timeToMove, pos - getPosition());
		runAction(moveEvent);

		m_path.erase(m_path.begin());

	}
}

void EnemyChomping::SetDirectionAnimation(ENEMY_DIR dir)
{

	stopAllActions();
	Vector<SpriteFrame*> frames;

	switch (dir)
	{
	case DIR_UP:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyChompMoveUp", 3);
		break;
	case DIR_DOWN:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyChompMoveDown", 3);
		break;
	case DIR_RIGHT:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyChompMoveRight", 3);
		break;
	case DIR_LEFT:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyChompMoveLeft", 3);
		break;

	}
	auto animation = Animation::createWithSpriteFrames(frames, 1.0f / 3);
	auto  animate = Animate::create(animation);
	auto spriteAction = runAction(RepeatForever::create(animate));
}