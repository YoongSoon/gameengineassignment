#pragma once

#include "EnemyBase.h"

class EnemyChomping : public EnemyBase
{
public:
	virtual void Init();
	EnemyChomping(MapHandler* map) :EnemyBase(map) { };
	~EnemyChomping() {};

	virtual void update(float dt);

	virtual void SetDirectionAnimation(ENEMY_DIR dir);
private:


};