#include "EnemyPlaceBomb.h"
#include "SpriteSheetLoader.h"


void EnemyPlaceBomb::Init()
{
	//Call base class init
	EnemyBase::Init();

	// Init Physics Body
	auto m_PhysicsBody = PhysicsBody::createCircle(3, PhysicsMaterial(1, 0, 1));
	m_PhysicsBody->setRotationEnable(false);
	m_PhysicsBody->setDynamic(false);
	m_PhysicsBody->setContactTestBitmask(true);
	this->addComponent(m_PhysicsBody);


	// Collision response
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(EnemyBase::onContactBegin, this);
	getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

	//Add sprite sheet into cache
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyPlaceBombMoveBackSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyPlaceBombMoveUpSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyPlaceBombMoveRightSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("EnemyPlaceBombMoveLeftSheet.plist");

	SetDirectionAnimation(DIR_DOWN);



}
void EnemyPlaceBomb::update(float dt)
{
	//call base class update
	EnemyBase::update(dt);

	if (m_Health <= 0)
	{
		return;
	}

	m_timePast += dt;

	// If the enemy is in is a danger zone ( bomb or bomb blast tile) and either current path is empty or new bomb has been placed 
	// by enemy or player, then  find the shortest path (BFS state) to move out of the danger zone;
	if (theStrategy->IsInDangerZone(this) == true && (m_path.empty() == true || EnemyStrategy::b_isANewBombPlaced == true))
	{

		//Clear any leftover TilePos in m_path;
		ClearPath();

		TilePos targetPoint = theStrategy->FindEscapeTargetPoint(this);
		theStrategy->BFSLimit(this, targetPoint, 100);


		EnemyStrategy::b_isANewBombPlaced = false;
	}
	else  if (theStrategy->IsInDangerZone(this) == false && m_path.empty() == true)
	{
		theStrategy->MoveRandomly(this);
	}

	//Place the dam bomb
	if (m_timePast >= m_nextBombTime && theStrategy->IsInDangerZone(m_curr) == false)
	{
		pos = m_Map->GetPosFromTilePos(m_curr);

		if (m_Map->m_Map[m_curr.x][m_curr.y].tileType == MapTile::TILE_FLOOR)
		{
			Bomb * m_bomb = new Bomb(m_Map, pos);
			m_bomb->SetBombMagnitude(2);
			m_Map->AddToMap(m_bomb);

			m_nextBombTime = m_timePast + PLACE_COOL_DOWN;

			EnemyStrategy::b_isANewBombPlaced = true;
		}
	}

	// Move to next tile only when there is no moving actions(only left the sprite animation actions thats why  getNumberOfRunningActions() == 1)
	if (m_path.empty() == false && getNumberOfRunningActions() == 1 )
	{
	     // caculate the next current enum direction
		CaculateTheNewAnimationDir();

		//get the next current TilePos of enemy
		m_curr = m_path[0];
	  
		//Check whether the next tile is a danger zone
		if (theStrategy->IsThereImpendingDangerZone(m_curr) == false)
		{
			//Set the Vec2 pos of the sprite
			pos = m_Map->m_Map[m_curr.x][m_curr.y].tileSprite->getPosition();

			//set the current sprite animation 
			SetDirectionAnimation(m_SpriteCurrentDir);

			//Move action 
			float timeToMove = (pos - getPosition()).length() / m_speed;
			auto moveEvent = MoveBy::create(timeToMove, pos - getPosition());
			 runAction(moveEvent);

			m_path.erase(m_path.begin());
		}
	
	
	}
	
}

void EnemyPlaceBomb::SetDirectionAnimation(ENEMY_DIR dir)
{
	stopAllActions();
	Vector<SpriteFrame*> frames;

	switch (dir)
	{
	case DIR_UP:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyPlaceBombMoveUp", 3);
		break;
	case DIR_DOWN:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyPlaceBombMoveBack", 3);
		break;
	case DIR_RIGHT:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyPlaceBombMoveRight", 3);
		break;
	case DIR_LEFT:
		frames = SpriteSheetLoader::GetSpriteFrames("EnemyPlaceBombMoveLeft", 3);
		break;

	}
	auto animation = Animation::createWithSpriteFrames(frames, 1.0f / 3);
	auto  animate = Animate::create(animation);
	auto spriteAction = runAction(RepeatForever::create(animate));

}
