#pragma once

#include "EnemyBase.h"

class EnemyPlaceBomb : public EnemyBase
{
public:
	EnemyPlaceBomb(MapHandler* map) :EnemyBase(map) { };
	~EnemyPlaceBomb() {};

	virtual void Init();
	virtual void update(float dt);

	virtual void SetDirectionAnimation(ENEMY_DIR dir);
private:
	float m_nextBombTime;
	const float PLACE_COOL_DOWN = 6.0f;


};