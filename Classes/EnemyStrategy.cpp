#include "EnemyStrategy.h"
#include "EnemyBase.h"
#include "Player.h"

bool EnemyStrategy::b_isANewBombPlaced = false;



void EnemyStrategy::Init()
{
	height = m_mapHandler->MAP_HEIGHT;
	width = m_mapHandler->MAP_WIDTH;

}

void EnemyStrategy::SetMapHandler(MapHandler * m_handler)
{
	m_mapHandler = m_handler;
}

void EnemyStrategy::SetPlayerInstance(Player * player)
{
	m_player = player;
}

Player * EnemyStrategy::GetPlayerInstance()
{
	return m_player;
}

bool EnemyStrategy::MoveRandomly(EnemyBase * goEnemy)
{
	int randomDir = RandomHelper::random_int(1, 4);


	TilePos next;

	switch (randomDir)
	{
	case 1:
		next.Set(goEnemy->m_curr.x, goEnemy->m_curr.y + 1);
		break;
	case 2:
		next.Set(goEnemy->m_curr.x, goEnemy->m_curr.y - 1);
		break;
	case 3:
		next.Set(goEnemy->m_curr.x + 1, goEnemy->m_curr.y);
		break;
	case 4:
		next.Set(goEnemy->m_curr.x - 1, goEnemy->m_curr.y);
		break;
	}


	if (IsWithinMapArray(next) && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
	{
		if (IsInDangerZone(next) == false)
		{
			goEnemy->m_path.push_back(next);
			return true;
		}
	}
	return false;
}


bool EnemyStrategy::BFSLimit(EnemyBase * goEnemy, TilePos target, int limit)
{
	//Reset / clear m_visited, m_queue, m_shortestPath, m_previous
	std::vector<bool> Visited(width * height, false);
	std::vector<TilePos> Previous(width * height);
	std::queue<TilePos> Queue = std::queue<TilePos>();

	//Push start onto queue
	Queue.push(goEnemy->m_curr);

	int nearestDistance = INT_MAX;
	TilePos nearestTile = goEnemy->m_curr;
	int loop = 0;
	TilePos nearest;
	TilePos curr;

	//while stack is not empty and loop < limit
	while (Queue.empty() == false && loop < limit) {
		++loop;
		// curr = pop queue
		curr = Queue.front();
		Queue.pop();

		//Get distance from curr to end
		int xDiff = curr.x - target.x;
		int yDiff = curr.y - target.y;
		int distance = abs(xDiff - yDiff);

		if (distance < nearestDistance) {
			nearestDistance = distance;
			nearest = curr;
		}

		if (curr.x == target.x && curr.y == target.y) {

			//Shortest path to end tile
			while (curr.x != goEnemy->m_curr.x || curr.y != goEnemy->m_curr.y) {

				//Insert curr at front 
				goEnemy->m_path.insert(goEnemy->m_path.begin(), curr);
				//Set curr as curr's prev
				curr = Previous[curr.y * height + curr.x];
			}
			//Insert curr at front
			goEnemy->m_path.insert(goEnemy->m_path.begin(), curr);
			return true;
		}
		TilePos next;
		next.Set(curr.x, curr.y + 1);
		if (next.y < height && !Visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsCurrentTileDangerZone(next) == false)
			{
				//Set next�s previous as curr
				Previous[next.y * height + next.x] = curr;
				//	Push next onto queue
				Queue.push(next);
				//	Mark next as visited
				Visited[next.y * height + next.x] = true;
			}
		}

		next.Set(curr.x, curr.y - 1);
		if (next.y >= 0 && !Visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsCurrentTileDangerZone(next) == false)
			{
				//Set next�s previous as curr
				Previous[next.y * height + next.x] = curr;
				//	Push next onto queue
				Queue.push(next);
				//	Mark next as visited
				Visited[next.y * height + next.x] = true;

			}
		}
		next.Set(curr.x - 1, curr.y);
		if (next.x >= 0 && !Visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsCurrentTileDangerZone(next) == false)
			{
				//Set next�s previous as curr
				Previous[next.y * height + next.x] = curr;
				//	Push next onto queue
				Queue.push(next);
				//	Mark next as visited
				Visited[next.y * height + next.x] = true;

			}
		}
		next.Set(curr.x + 1, curr.y);
		if (next.x < width && !Visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsCurrentTileDangerZone(next) == false)
			{
				//Set next�s previous as curr
				Previous[next.y * height + next.x] = curr;
				//	Push next onto queue
				Queue.push(next);
				//	Mark next as visited
				Visited[next.y * height + next.x] = true;

			}
		}

	}

	curr = nearest;

	while (curr.x != goEnemy->m_curr.x || curr.y != goEnemy->m_curr.y) {

		//Insert curr at front of m_shortestPath
		goEnemy->m_path.insert(goEnemy->m_path.begin(), curr);
		//Set curr as curr�s prev
		curr = Previous[curr.y * height + curr.x];
	}
	//Insert curr at front of m_shortestPath
	goEnemy->m_path.insert(goEnemy->m_path.begin(), curr);
	return false;
}

TilePos EnemyStrategy::FindEscapeTargetPoint(EnemyBase * go)
{
	bool b_foundEscape = false;

	queue<TilePos> m_queue = queue<TilePos>();
	vector<bool> m_visited(m_mapHandler->MAP_WIDTH * m_mapHandler->MAP_HEIGHT, false);

	//Set the current tile as visited
	m_visited[go->m_curr.y * height + go->m_curr.x] = true;
	//Push enemyObj->m_curr onto queue
	m_queue.push(go->m_curr);


	//current tile pos of from m_queue
	TilePos curr;
	//target tile pos to move to
	TilePos target;


	while (m_queue.empty() == false && b_foundEscape == false)
	{
		curr = m_queue.front();
		m_queue.pop();

		TilePos next;
		next.Set(curr.x, curr.y + 1);
		if (next.y < height && !m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsInDangerZone(next) == false)
			{
				target = next;
				b_foundEscape = true;
				break;
			}
			else
			{
				//	Push next onto queue
				m_queue.push(next);
				//	Mark next as visited
				m_visited[next.y * height + next.x] = true;
			}

		}
		next.Set(curr.x, curr.y - 1);
		if (next.y >= 0 && !m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsInDangerZone(next) == false)
			{
				target = next;
				b_foundEscape = true;
				break;
			}
			else
			{
				//	Push next onto queue
				m_queue.push(next);
				//	Mark next as visited
				m_visited[next.y * height + next.x] = true;
			}
		}
		next.Set(curr.x - 1, curr.y);
		if (next.x >= 0 && !m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsInDangerZone(next) == false)
			{
				target = next;
				b_foundEscape = true;
				break;
			}
			else
			{
				//	Push next onto queue
				m_queue.push(next);
				//	Mark next as visited
				m_visited[next.y * height + next.x] = true;
			}
		}
		next.Set(curr.x + 1, curr.y);
		if (next.x < width && !m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
		{
			if (IsInDangerZone(next) == false)
			{
				target = next;
				b_foundEscape = true;
				break;
			}
			else
			{
				//	Push next onto queue
				m_queue.push(next);
				//	Mark next as visited
				m_visited[next.y * height + next.x] = true;
			}
		}

	}


	return target;
}

bool EnemyStrategy::IsInDangerZone(EnemyBase * goEnemy)
{

	//********* 1)have to check for  the tile where the bomb blast game object at **************//
	//********** 2) have to check for the tile the explosion is  at **********//

	//iterate through the map to find the bomb location
	for (int x = 0; x < width; ++x)
	{
		for (int y = 0; y < height; ++y)
		{
			auto* mapTile = &m_mapHandler->m_Map[x][y];
			auto* m_GoList = &mapTile->m_Objects;

			if (m_GoList->empty() == true)
				continue;

			for (auto it = m_GoList->begin(); it != m_GoList->end(); ++it) {
				GameObject* go = *it;


				if (go->getName() == "bomb")
				{
					Bomb * theBomb = static_cast<Bomb*>(go);

					vector<TilePos> explosionTiles = theBomb->GetTheExplosionTiles();

					//check whether enemy is in any of the tile containng the explosion
					for (size_t i = 0; i < explosionTiles.size(); ++i)
					{
						if (goEnemy->m_curr.x == explosionTiles[i].x && goEnemy->m_curr.y == explosionTiles[i].y)
						{
							return true;
						}
					}
				}
				else if (go->getName() == "explosion")
				{

					//Convert Vec2 position of explosion to TilePos
					TilePos explodePos = m_mapHandler->GetTilePosFromPos(go->getPosition());

					//check whether startPos is equals to the TilePos of explosion
					if (goEnemy->m_curr.x == explodePos.x && goEnemy->m_curr.y == explodePos.y)
					{
						return true;
					}

				}
			}
		}
	}
	return false;
}

bool EnemyStrategy::IsInDangerZone(TilePos startPos)
{
	for (int x = 0; x < width; ++x)
	{
		for (int y = 0; y < height; ++y)
		{
			auto* mapTile = &m_mapHandler->m_Map[x][y];
			auto* m_GoList = &mapTile->m_Objects;

			if (m_GoList->empty() == true)
				continue;

			for (auto it = m_GoList->begin(); it != m_GoList->end(); ++it) {
				GameObject* go = *it;


				if (go->getName() == "bomb")
				{
					Bomb * theBomb = static_cast<Bomb*>(go);

					vector<TilePos> explosionTiles = theBomb->GetTheExplosionTiles();

					//check whether enemy is in any of the tile containng the explosion
					for (size_t i = 0; i < explosionTiles.size(); ++i)
					{
						if (startPos.x == explosionTiles[i].x && startPos.y == explosionTiles[i].y)
						{
							return true;
						}
					}

				}
				else if (go->getName() == "explosion")
				{

					//Convert Vec2 position of explosion to TilePos
					TilePos explodePos = m_mapHandler->GetTilePosFromPos(go->getPosition());

					//check whether startPos is equals to the TilePos of explosion
					if (startPos.x == explodePos.x && startPos.y == explodePos.y)
					{
						return true;
					}

				}
			}
		}
	}
	return false;
}



bool EnemyStrategy::IsCurrentTileDangerZone(TilePos startPos)
{
	auto* mapTile = &m_mapHandler->m_Map[startPos.x][startPos.y];
	auto* m_GoList = &mapTile->m_Objects;


	for (auto it = m_GoList->begin(); it != m_GoList->end(); ++it) {
		GameObject* go = *it;

		//If the gameobject is a bomb
		if (go->getName() == "bomb")
		{
			Bomb* theBomb = static_cast<Bomb*>(go);

			//if the bomer timer is equal or lesser to half then proceed on with the check
			if (theBomb->GetTimer() <= theBomb->GetDefaultTime() * 0.8f)
			{
				//Convert Vec2 position of explosion to TilePos
				TilePos bombPos = m_mapHandler->GetTilePosFromPos(go->getPosition());
				//check whether startPos is equals to the TilePos of explosion
				if (startPos.x == bombPos.x && startPos.y == bombPos.y)
				{
					return true;
				}

			}
		}
		else if (go->getName() == "explosion")
		{
			//Convert Vec2 position of explosion to TilePos
			TilePos explodePos = m_mapHandler->GetTilePosFromPos(go->getPosition());

			//check whether startPos is equals to the TilePos of explosion
			if (startPos.x == explodePos.x && startPos.y == explodePos.y)
			{
				return true;
			}

		}
	}


	return false;
}

bool EnemyStrategy::IsThereImpendingDangerZone(TilePos startPos)
{
	for (int x = 0; x < width; ++x)
	{
		for (int y = 0; y < height; ++y)
		{
			auto* mapTile = &m_mapHandler->m_Map[x][y];
			auto* m_GoList = &mapTile->m_Objects;

			if (m_GoList->empty() == true)
				continue;

			for (auto it = m_GoList->begin(); it != m_GoList->end(); ++it) {
				GameObject* go = *it;


				if (go->getName() == "bomb")
				{
					Bomb * theBomb = static_cast<Bomb*>(go);

					if (theBomb->GetTimer() <= theBomb->GetDefaultTime() * 0.2)
					{
						vector<TilePos> explosionTiles = theBomb->GetTheExplosionTiles();

						//check whether enemy is in any of the tile containng the explosion
						for (size_t i = 0; i < explosionTiles.size(); ++i)
						{
							if (startPos.x == explosionTiles[i].x && startPos.y == explosionTiles[i].y)
							{
								return true;
							}
						}
					}

				}
				else if (go->getName() == "explosion")
				{

					//Convert Vec2 position of explosion to TilePos
					TilePos explodePos = m_mapHandler->GetTilePosFromPos(go->getPosition());

					//check whether startPos is equals to the TilePos of explosion
					if (startPos.x == explodePos.x && startPos.y == explodePos.y)
					{
						return true;
					}

				}
			}
		}
	}
	return false;
}


bool EnemyStrategy::IsPlayerNear(EnemyBase * goEnemy, float distFactor)
{
	//Retrieve TilePos of player
	TilePos playerTilePos = m_mapHandler->GetTilePosFromPos(m_player->GetPlayerSprite()->getPosition());

	//Get distance between enmy and player
	int xDiff = goEnemy->m_curr.x - playerTilePos.x;
	int yDiff = goEnemy->m_curr.y - playerTilePos.y;
	int distance = abs(xDiff - yDiff);

	//Get the size of one tile sprite
	Size tileSize = m_mapHandler->m_Map[0][0].tileSprite->getBoundingBox().size;

	if (distance <= tileSize.width * distFactor || distance <= tileSize.height *distFactor)
	{
		return true;
	}


	return false;
}



void EnemyStrategy::DFSOnce(EnemyBase * goEnemy)
{
	goEnemy->m_stack.push_back(goEnemy->m_curr);
	goEnemy->m_visited[goEnemy->m_curr.y * height + goEnemy->m_curr.x] = true;

	goEnemy->m_prev = goEnemy->m_curr;

	//Generate the random direction order
	GenerateRandomDirs();

	TilePos next;

	for (size_t k = 0; k < randDirsVector.size(); ++k)
	{
		switch (randDirsVector[k])
		{
		case 1:
		{
			next.Set(goEnemy->m_curr.x, goEnemy->m_curr.y + 1);
			if (next.y < height && !goEnemy->m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
			{
				if (IsInDangerZone(next) == false)
				{
					goEnemy->m_curr = next;
					return;
				}
			}
		}
		break;
		case 2:
		{
			next.Set(goEnemy->m_curr.x, goEnemy->m_curr.y - 1);
			if (next.y >= 0 && !goEnemy->m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
			{
				if (IsInDangerZone(next) == false)
				{
					goEnemy->m_curr = next;
					return;
				}
			}
		}
		break;
		case 3:
		{
			next.Set(goEnemy->m_curr.x - 1, goEnemy->m_curr.y);
			if (next.x >= 0 && !goEnemy->m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
			{
				if (IsInDangerZone(next) == false)
				{
					goEnemy->m_curr = next;
					return;
				}
			}
		}
		break;
		case 4:
		{
			next.Set(goEnemy->m_curr.x + 1, goEnemy->m_curr.y);
			if (next.x < width && !goEnemy->m_visited[next.y * height + next.x] && m_mapHandler->m_Map[next.x][next.y].tileType == MapTile::TILE_FLOOR)
			{
				if (IsInDangerZone(next) == false)
				{
					goEnemy->m_curr = next;
					return;
				}
			}
		}
		break;

		}
	}


	//Pop go stack
	goEnemy->m_stack.pop_back();

	if (goEnemy->m_stack.empty() == false) {
		goEnemy->m_curr = goEnemy->m_stack.back();
		goEnemy->m_stack.pop_back();
		return;
	}

}

void EnemyStrategy::GenerateRandomDirs()
{
	//clear the ranDirsVector if there is leftover in the contianer
	randDirsVector.clear();

	//add int value of 1 to 4 into ranDirsVector;
	for (int i = 0; i < 4; i++)
		randDirsVector.push_back(i + 1);

	std::srand(time(0));

	// using built-in random generator:
	random_shuffle(randDirsVector.begin(), randDirsVector.end());
}

bool EnemyStrategy::IsWithinMapArray(TilePos pos)
{
	if ((pos.x < 0 || pos.x >= width) || (pos.y < 0 || pos.y >= height))
		return false;
	else
		return true;
}


