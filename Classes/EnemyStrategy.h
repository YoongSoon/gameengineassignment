#pragma once

#include "MapHandler.h"

#include <vector>
#include <queue>
#include <algorithm>    // std::random_shuffle
#include <cstdlib> // time

using std::vector;
using std::queue;
using std::random_shuffle;

class EnemyBase;
class Player;

class EnemyStrategy
{
public:

	void Init();
	void SetMapHandler(MapHandler * m_handler);
	void SetPlayerInstance(Player * player);
	Player * GetPlayerInstance();


	bool MoveRandomly(EnemyBase * goEnemy);

	// Parameter 1 - object that you are applying the BFS on
	// Parameter 2 - the tile position of where the object should move after the BFS is caculated
	// Parameter 3 - the maximum limit of steps the object should take in the BFS
	bool BFSLimit(EnemyBase * goEnemy, TilePos target, int limit = 1000);

	//Find the target tilePos for enemy to move to
	TilePos FindEscapeTargetPoint(EnemyBase * goEnemy);

	//Check whether enemy is in either  preexisting/exisiting bomb explosion o or tile that has bomb
	bool IsInDangerZone(EnemyBase * goEnemy);
	// Check whether the given argument :startPos is in either preexisting/exisiting bomb explosion or tile that has bomb
	bool IsInDangerZone(TilePos startPos);

	//Check whether current tile contains prexisiting bomb explosion or bomb 
	bool IsCurrentTileDangerZone(TilePos startPos);

	//check whether stepping on the next tile is a tile that gona have explosion tile or already has explosion tile
	bool IsThereImpendingDangerZone(TilePos startPos);

	//Check whether player is near
	bool IsPlayerNear(EnemyBase * goEnemy , float distFactor);

	//Indicate whether a bomb has been placed by either player or enemy
	static bool b_isANewBombPlaced;

	void DFSOnce(EnemyBase * goEnemy);

	//Generate the random direction(Up , down , left and right) for DFS
	void GenerateRandomDirs();

private:

	bool IsWithinMapArray(TilePos pos);

	Player * m_player;
	MapHandler * m_mapHandler;

	//Which tile is the enemy currently in
	TilePos pos;
	//widht and height of the 2d map array
	int height;
	int width;

	vector<int> randDirsVector;
};