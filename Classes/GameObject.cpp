#include "GameObject.h"

std::vector<TilePos>  Bomb::GetTheExplosionTiles(bool destroyWall) {

	TilePos temp_tile;//temporary variable to store TilePos type variables for putting into temp vector

	std::vector<TilePos> temp;//temporary vector to store all possible bombarded location
		
	Vec2 pos = this->getPosition();
	pos += this->getContentSize() / 2;

	//Convert bomb position into Tile Position
	int x_value = m_Map->GetTilePosFromPos(pos).x;
	int y_value = m_Map->GetTilePosFromPos(pos).y;

	//include the current tile the bomb is on
	temp_tile = { x_value,y_value };
	temp.push_back(temp_tile);

	// log("Bomb Origin: %i, %i", temp_tile.x, temp_tile.y);

	//Check right path 
	for (int x = 1; x <= magnitude; x++) {//check if adjacent tile is a floor tile
		TilePos temp_tile2 = { x_value + x, y_value };

		if (m_Map->m_Map[x_value + x][y_value].tileType == MapTile::TILE_FLOOR) {
			temp.push_back(temp_tile2);//put tile into temp vector
			// log("Checking: %i, %i", temp_tile2.x, temp_tile2.y);
		}
		else if (destroyWall == true && m_Map->m_Map[x_value + x][y_value].tileType == MapTile::TILE_DESTRUCTIBLE) {
			DestroyDestructibleWall(temp_tile2);
			break;
		}
		else
			break;
	}
	//Check down path
	for (int y = 1; y <= magnitude; y++) {//check if adjacent tile is a floor tile
		TilePos temp_tile2 = { x_value, y_value + y };
		if (m_Map->m_Map[x_value][y_value + y].tileType == MapTile::TILE_FLOOR) {
			temp.push_back(temp_tile2);//put tile into temp vector
		}
		else if (destroyWall == true && m_Map->m_Map[x_value][y_value + y].tileType == MapTile::TILE_DESTRUCTIBLE) {
			DestroyDestructibleWall(temp_tile2);
			break;
		}
		else
			break;
	}
	//Check left path
	for (int x = -1; x >= -magnitude; x--) {//check if adjacent tile is a floor tile
		TilePos temp_tile2 = { x_value + x, y_value };
		if (m_Map->m_Map[x_value + x][y_value].tileType == MapTile::TILE_FLOOR) {
			temp.push_back(temp_tile2);//put tile into temp vector
		}
		else if (destroyWall == true && m_Map->m_Map[x_value + x][y_value].tileType == MapTile::TILE_DESTRUCTIBLE) {
			DestroyDestructibleWall(temp_tile2);
			break;
		}
		else
			break;
	}
	//Check down path
	for (int y = -1; y >= -magnitude; y--) {//check if adjacent tile is a floor tile
		TilePos temp_tile2 = { x_value, y_value + y };
		if (m_Map->m_Map[x_value][y_value + y].tileType == MapTile::TILE_FLOOR) {
			temp.push_back(temp_tile2);//put tile into temp vector
		}
		else if (destroyWall == true && m_Map->m_Map[x_value][y_value + y].tileType == MapTile::TILE_DESTRUCTIBLE) {
			DestroyDestructibleWall(temp_tile2);
			break;
		}
		else
			break;
	}

	return temp;
}

std::vector<TilePos> Bomb::GetSunnyExplosionTiles() {

	TilePos temp_tile;//temporary variable to store TilePos type variables for putting into temp vector

	std::vector<TilePos> temp;//temporary vector to store all possible bombarded location

	Vec2 pos = this->getPosition();
	pos += this->getContentSize() / 2;

	//Convert bomb position into Tile Position
	int x_value = m_Map->GetTilePosFromPos(pos).x;
	int y_value = m_Map->GetTilePosFromPos(pos).y;

	//include the current tile the bomb is on
	temp_tile = { x_value, y_value };

	int magnitude = 2; // Probably not a good idea to use player's magnitude

	// 3 x 3 tile check
	for (int x = -magnitude; x <= magnitude; x++) {
		for (int y = -magnitude; y <= magnitude; y++) {
			TilePos tilePos = { x_value + x, y_value + y };

			if (tilePos.x < 0 || tilePos.x >= m_Map->MAP_WIDTH || tilePos.y < 0 || tilePos.y >= m_Map->MAP_HEIGHT)
				continue;

			if (m_Map->m_Map[tilePos.x][tilePos.y].tileType == MapTile::TILE_DESTRUCTIBLE)
				DestroyDestructibleWall(tilePos);

			temp.push_back(tilePos);
		}
	}

	return temp;
}

void Bomb::Explosion() {
	std::vector<TilePos> explodingTiles;

	if (m_IsSunnyBomb) {
		explodingTiles = GetSunnyExplosionTiles();
	}
	else {
		explodingTiles = GetTheExplosionTiles(true);
	}

	//Generate Fire Tile at all the coords in the temp vector
	for (int a = 0; a < explodingTiles.size(); a++) {
		Vec2 temp_pos = m_Map->GetPosFromTilePos(explodingTiles[a]);
		m_Map->AddToMap(new ExplosionObject(m_Map, temp_pos));//replace with fire animation tile 
	}
}
