#pragma once

#include "cocos2d.h"
#include "MapHandler.h"
#include "Definition.h"
#include "SpriteSheetLoader.h"
#include "AudioEngine.h"
#include "AudioManagerManager.h"

USING_NS_CC;

class GameObject : public Sprite {

public:
	GameObject(MapHandler* map) {
		this->scheduleUpdate();
		m_Map = map;
	};

	virtual ~GameObject() {};

	void InitGO(const std::string fileName) {
		initWithFile(fileName);
		m_Map->AddToRenderMap(this);
	}

	void InitSize(Vec2 spawnPos, int zOrder) {
		setAnchorPoint(Vec2(0, 0));
		setContentSize(Size(16, 16)); // 16 is the radius for 32 diameter
		setPosition(spawnPos);
		setZOrder(zOrder);
	}

	virtual void update(float dt) {};

	bool IsDestroyed() { return isDestroyed; }

	void DestroyMe() { isDestroyed = true; this->unscheduleUpdate(); };

	MapHandler * GetMapHandler() { return m_Map; }

protected:
	MapHandler * m_Map;

private:
	bool isDestroyed = false;
};

// Bomb example
class Bomb : public GameObject {

public:
	Bomb(MapHandler* map, Vec2 pos, bool isPlayer = false) : GameObject(map) {
		InitGO("TileMaps/Individual/bomb.png");
		InitSize(pos, ZORDER_BOMBS);
		//Set the bomb tag as 3 , idk why
		setName("bomb");
		bombpos = pos;

		m_TimeLeft = 3.0f;
		m_DefaultTime = m_TimeLeft;

		m_IsPlayer = isPlayer;

		// Tween the scale
		runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleTo::create(0.2f, 1.05f), ScaleTo::create(0.2f, 0.95f))));

		if (!AchievementManager::bombTrophyUnlocked)
			AchievementManager::bomb_yay = true;
		UserDefault::getInstance()->setBoolForKey("bombtrophy", true);
		AchievementManager::bombTrophyUnlocked = true;
	/*	Scene * currentScene = CCDirector::getInstance()->getRunningScene();
		sparkParticle = ParticleSystemQuad::create("spark.plist");
		sparkParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
		sparkParticle->setPosition(Vec2(this->getPosition().x , this->getPosition().y ));
		sparkParticle->setDuration(ParticleSystemQuad::DURATION_INFINITY);
		currentScene->addChild(sparkParticle);*/
		
	}

	//To retrieve the explosion tile pos beforehand (before the bomb explodes)
	std::vector<TilePos> GetTheExplosionTiles(bool destroyWall = false);

	// To retrieve explosition tiles while weather is sunnny
	std::vector<TilePos> GetSunnyExplosionTiles();

	void Explosion();

	virtual void update(float dt) {
		m_TimeLeft -= dt;


		// Explode after X seconds
		if (m_TimeLeft <= 0) {

			AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/explosion.mp3", false, AudioManagerManager::GlobalSFXvalue);
			for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
				experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
			if (AudioManagerManager::tempid != -1)
				AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

			Explosion();//Explode Tiles
			DestroyMe(); // Destroy myself
		}
	}

	float GetTimer() { return m_TimeLeft; }
	float GetDefaultTime() { return m_DefaultTime; }

	void SetBombMagnitude(int _magnitude) { magnitude = _magnitude; }

	void DestroyDestructibleWall(TilePos pos) {
		m_Map->DestroyDestructibleWall(pos);
	}

	bool IsPlayerBomb() { return m_IsPlayer; }

	void SetSunnyBomb(bool enabled) { m_IsSunnyBomb = enabled; }

private:
	float m_TimeLeft;
	float m_DefaultTime;
	Vec2 bombpos;//position of bomb placed
	bool m_IsPlayer = false;
	int magnitude; // magnitude / range of the bomb
	bool m_IsSunnyBomb = false;
	//ParticleSystem * sparkParticle; // particle
};


class ExplosionObject : public GameObject {

public:
	ExplosionObject(MapHandler* map, Vec2 pos) : GameObject(map) {
		InitGO("TileMaps/Individual/lava.png");
		InitSize(pos, ZORDER_BOMBS);
		setName("explosion");

		auto m_PhysicsBody = PhysicsBody::createCircle(6, PhysicsMaterial(1, 0, 1));
		m_PhysicsBody->setRotationEnable(false);
		m_PhysicsBody->setDynamic(false);
		m_PhysicsBody->setContactTestBitmask(true);
		this->addComponent(m_PhysicsBody);

		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("lavaSheet.plist");
		RunLavaSpriteAnimation();
	}

	void RunLavaSpriteAnimation()
	{
		Vector<SpriteFrame*> frames = SpriteSheetLoader::GetSpriteFrames("lava", 11);

		auto animation = Animation::createWithSpriteFrames(frames, 1.0f / 11);
		auto  animate = Animate::create(animation);
		//Get the sprite frames from the cache
		this->runAction(RepeatForever::create(animate));
	};


	virtual void update(float dt) {
		m_TimeLeft -= dt;

		// Explode after X seconds
		if (m_TimeLeft <= 0) {
			DestroyMe(); // Destroy myself

		}
	}

	float GetTimer() { return m_TimeLeft; }


private:
	float m_TimeLeft = 1.0f;

};

class DestructibleWall : public GameObject {
public:
	DestructibleWall(MapHandler* map, Vec2 pos) : GameObject(map) {
		InitGO("TileMaps/Individual/destructible_wall.png");
		InitSize(pos, ZORDER_BOMBS);
		setName("destructible_wall");
	}

	virtual void update() {
		if (destroy)
			DestroyMe();
	}
	void DestroyWall() {
		destroy = true;
	}
private:
	bool destroy = false;
};