#include "GameSceneLevel1.h"
#include "GameSceneLevel2.h"
#include "PlayerModeSelectionScene.h"
#include "Definition.h"
#include "MainMenuScene.h"
#include "EnemyBase.h"
#include "AppDelegate.h"


GameSceneLevel1::~GameSceneLevel1() {
	m_Map.Exit();
}

cocos2d::Scene * GameSceneLevel1::createScene() {
	// scene is an autorelease object
	auto scene = Scene::createWithPhysics();

	// Physics configurations
	scene->getPhysicsWorld()->setGravity(Vec2(0, 0));
	// scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_SHAPE);

	// layer is an autorelease object
	auto layer = GameSceneLevel1::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}

bool GameSceneLevel1::init() {
	Scene::init();
	Scene::initWithPhysics();

	this->scheduleUpdate();

	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	// TileMap init
	m_Map.Init("TileMaps/campaign_level1.tmx", this);

	// Init player
	m_Player.Init(&m_Map, m_Map.GetSpawnPoint(Player::PLAYER_0), Player::PLAYER_0);
	this->addChild(m_Player.InitJoyStick());
	this->addChild(m_Player.InitJoyButton());
	this->addChild(m_Player.InitReloadBar());

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(Player::onTouchBegan, &m_Player);
	listener->onTouchMoved = CC_CALLBACK_2(Player::onTouchMoved, &m_Player);
	listener->onTouchEnded = CC_CALLBACK_2(Player::onTouchEnded, &m_Player);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

	EnemyBase * theEnemy = Create::Enemy(&m_Map, EnemyBase::CHOMPING_ENEMY, Vec2(m_Map.m_Map[7][8].tileSprite->getPosition().x, m_Map.m_Map[7][7].tileSprite->getPosition().y));
	theEnemy->theStrategy->SetPlayerInstance(&m_Player);
	m_EnemyContainer.push_back(theEnemy);

	EnemyBase * theEnemy3 = Create::Enemy(&m_Map, EnemyBase::CHOMPING_ENEMY, Vec2(m_Map.m_Map[7][5].tileSprite->getPosition().x, m_Map.m_Map[7][5].tileSprite->getPosition().y));
	theEnemy3->theStrategy->SetPlayerInstance(&m_Player);
	m_EnemyContainer.push_back(theEnemy3);

	// Init PowerUpManager
	m_PowerUpManager.Init(&m_Map);

	// Init WeatherManager
	m_WeatherManager.Init(this, &m_Player);

	//Listen for keyboard events , havee to add for every level scene
	auto listener2 = EventListenerKeyboard::create();
	listener2->onKeyReleased = CC_CALLBACK_2(GameSceneLevel1::OnKeyReleased, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener2, this);

	m_GameSize = Director::getInstance()->getWinSize();
	float m_PlayerUI_X_Center = 0.5f;
	float m_PlayerUI_Y_TOP = 0.95f;

	m_VictoryImage = Sprite::create("victory.png");
	m_VictoryImage->setAnchorPoint(Vec2(0.5f, 1));
	m_VictoryImage->setPosition(m_GameSize.width * m_PlayerUI_X_Center, m_GameSize.height * 0.75f);
	m_VictoryImage->setScale(0.5f, 0.5f);
	m_VictoryImage->setVisible(false);
	this->addChild(m_VictoryImage);

	m_DefeatImage = Sprite::create("defeat.png");
	m_DefeatImage->setAnchorPoint(Vec2(0.5f, 1));
	m_DefeatImage->setPosition(m_GameSize.width * m_PlayerUI_X_Center, m_GameSize.height * 0.75f);
	m_DefeatImage->setScale(0.5f, 0.5f);
	m_DefeatImage->setVisible(false);
	this->addChild(m_DefeatImage);

	m_RestartImage = Button::create("restart.png");
	m_RestartImage->setAnchorPoint(Vec2(0.5f, 1));
	m_RestartImage->setPosition(Vec2(m_GameSize.width * m_PlayerUI_X_Center, m_GameSize.height * 0.5f));
	m_RestartImage->setScale(0.5f, 0.5f);
	m_RestartImage->setVisible(false);
	m_RestartImage->addClickEventListener(CC_CALLBACK_0(GameSceneLevel1::OnRestartPressed, this));
	this->addChild(m_RestartImage);

	m_MainMenuImage = Button::create("mainmenu.png");
	m_MainMenuImage->setAnchorPoint(Vec2(0.5f, 1));
	m_MainMenuImage->setPosition(Vec2(m_GameSize.width * m_PlayerUI_X_Center, m_GameSize.height* 0.4f));
	m_MainMenuImage->setScale(0.5f, 0.5f);
	m_MainMenuImage->setVisible(false);
	m_MainMenuImage->addClickEventListener(CC_CALLBACK_0(GameSceneLevel1::OnBackMainMenuPressed, this));
	this->addChild(m_MainMenuImage);


	// Start of Player 0 Color
	lblPlayer_0_Health = Label::createWithTTF("[Health]\n0", "fonts/Marker Felt.ttf", 10);
	lblPlayer_0_Health->setPosition(Vec2(m_GameSize.width * 0.2f, m_GameSize.height * m_PlayerUI_Y_TOP));
	lblPlayer_0_Health->setAlignment(TextHAlignment::CENTER);
	this->addChild(lblPlayer_0_Health, 1);

	lblPlayer_0_Speed = Label::createWithTTF("[Speed]\n0", "fonts/Marker Felt.ttf", 10);
	lblPlayer_0_Speed->setPosition(Vec2(m_GameSize.width * 0.4f, m_GameSize.height * m_PlayerUI_Y_TOP));
	lblPlayer_0_Speed->setAlignment(TextHAlignment::CENTER);
	this->addChild(lblPlayer_0_Speed, 1);

	lblPlayer_0_Range = Label::createWithTTF("[Bomb Range]\n0", "fonts/Marker Felt.ttf", 10);
	lblPlayer_0_Range->setPosition(Vec2(m_GameSize.width * 0.6f, m_GameSize.height * m_PlayerUI_Y_TOP));
	lblPlayer_0_Range->setAlignment(TextHAlignment::CENTER);
	this->addChild(lblPlayer_0_Range, 1);

	lblPlayer_0_BombCount = Label::createWithTTF("[Bomb Count]\n0", "fonts/Marker Felt.ttf", 10);
	lblPlayer_0_BombCount->setPosition(Vec2(m_GameSize.width * 0.8f, m_GameSize.height * m_PlayerUI_Y_TOP));
	lblPlayer_0_BombCount->setAlignment(TextHAlignment::CENTER);
	this->addChild(lblPlayer_0_BombCount, 1);
	// End of Player 0 Color

	lblPauseTitle = Label::createWithTTF("Game Paused", "fonts/Marker Felt.ttf", 24);
	lblPauseTitle->setPosition(Vec2(m_GameSize.width * 0.5f, m_GameSize.height * 0.75f));
	lblPauseTitle->setAlignment(TextHAlignment::CENTER);
	lblPauseTitle->enableShadow();
	lblPauseTitle->setColor(Color3B::WHITE);
	lblPauseTitle->setVisible(false);
	this->addChild(lblPauseTitle, 1);

	m_PauseButton = Button::create("pausebutton.png");
	m_PauseButton->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_PauseButton->setPosition(Vec2(m_GameSize.width * 0.95f, m_GameSize.height* 0.95f));
	m_PauseButton->setScale(0.75f, 0.75f);
	m_PauseButton->addClickEventListener(CC_CALLBACK_0(GameSceneLevel1::OnPauseButtonPressed, this));
	this->addChild(m_PauseButton);

	m_ResumeButton = Button::create("resumebutton.png");
	m_ResumeButton->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ResumeButton->setPosition(Vec2(m_GameSize.width * 0.5f, m_GameSize.height* 0.5f));
	m_ResumeButton->setScale(0.75f, 0.75f);
	m_ResumeButton->setVisible(false);
	m_ResumeButton->addClickEventListener(CC_CALLBACK_0(GameSceneLevel1::OnResumeButtonPressed, this));
	this->addChild(m_ResumeButton);

	m_PauseToMainMenuButton = Button::create("quitlogo.png");
	m_PauseToMainMenuButton->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_PauseToMainMenuButton->setPosition(Vec2(m_GameSize.width * 0.5f, m_GameSize.height* 0.35f));
	m_PauseToMainMenuButton->setScale(0.75f, 0.75f);
	m_PauseToMainMenuButton->setVisible(false);
	m_PauseToMainMenuButton->addClickEventListener(CC_CALLBACK_0(GameSceneLevel1::OnBackMainMenuPressed, this));
	this->addChild(m_PauseToMainMenuButton);

	lblCurrentTimer = Label::createWithTTF("[Time]\n0s", "fonts/Marker Felt.ttf", 10);
	lblCurrentTimer->setPosition(Vec2(m_GameSize.width * 0.5f, m_GameSize.height * m_PlayerUI_Y_TOP));
	lblCurrentTimer->setAlignment(TextHAlignment::CENTER);
	lblCurrentTimer->setColor(Color3B::YELLOW);
	this->addChild(lblCurrentTimer, 1);

	lblVictoryTimer = Label::createWithTTF("Time Taken\n0s", "fonts/Marker Felt.ttf", 13);
	lblVictoryTimer->setPosition(m_GameSize.width * m_PlayerUI_X_Center, m_GameSize.height * 0.75f);
	lblVictoryTimer->setAlignment(TextHAlignment::CENTER);
	lblVictoryTimer->enableShadow(Color4B::BLACK, Size(1, -1));
	lblVictoryTimer->setColor(Color3B::YELLOW);
	lblVictoryTimer->setVisible(false);
	this->addChild(lblVictoryTimer, 1);

	m_ShareToFBButton = Button::create("facebookshare.png");
	m_ShareToFBButton->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_ShareToFBButton->setPosition(Vec2(m_GameSize.width * 0.5f, m_GameSize.height* 0.23f));
	m_ShareToFBButton->setScale(0.6f, 0.6f);
	m_ShareToFBButton->setVisible(false);
	m_ShareToFBButton->addClickEventListener(CC_CALLBACK_0(GameSceneLevel1::OnShareButtonPressed, this));
	this->addChild(m_ShareToFBButton);

	achievementunlocked = Text::create("You have just unlocked an achievement!", "fonts/Marker Felt.ttf", 12);
	achievementunlocked->setPosition(Vec2(m_GameSize.width / 2.0f, m_GameSize.height / 10.0f));
	achievementunlocked->setVisible(false);
	SpriteSheetLoader::RescaleToScreenSize(achievementunlocked);
	this->addChild(achievementunlocked);

	for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::BGM_IDs[a]);
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Stage1Music.mp3", true, AudioManagerManager::GlobalBGMvalue);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::BGM_IDs.push_back(AudioManagerManager::tempid);


#ifdef SDKBOX_ENABLED
	sdkbox::PluginFacebook::setListener(this);
	sdkbox::PluginFacebook::logout();
#endif

	return true;
}

void GameSceneLevel1::update(float dt) {

	if (!m_IsShowingGameOver) {

		m_TimeTaken += dt;
		string str = "[Time]\n" + to_string((int)m_TimeTaken) + string("s");
		lblCurrentTimer->setString(str);

		if (m_Player.m_Lives <= 0) {
			experimental::AudioEngine::stopAll();
			m_Victory = false;
			m_IsShowingGameOver = true;
			ToggleGameOverMenu(true);
		}

		int enemyAlive = 0;

		for (auto enemy : m_EnemyContainer) {
			if (!enemy->b_isDead)
				enemyAlive++;
		}

		if (enemyAlive == 0) {
			experimental::AudioEngine::stopAll();
			m_Victory = true;
			m_IsShowingGameOver = true;
			ToggleGameOverMenu(true);
		}

		lblPlayer_0_BombCount->setString("[Bomb Count]\n" + std::to_string(m_Player.m_BombCount - 1));
		lblPlayer_0_Health->setString("[Health]\n" + std::to_string(m_Player.m_Lives));
		lblPlayer_0_Range->setString("[Bomb Range]\n" + std::to_string(m_Player.m_BombRange - 2));
		lblPlayer_0_Speed->setString("[Speed]\n" + std::to_string(((int)m_Player.GetMaxVelocity() / 10) - 15));

		m_Player.Update(dt);

		if (m_Player.b_ANewPowerUpConsumed == true) {
			CreateStatsFontLabel();
			m_Player.b_ANewPowerUpConsumed = false;
		}

	}

	if (AchievementManager::bombTrophyUnlocked && AchievementManager::bomb_yay || 
		AchievementManager::skullTrophyUnlocked && AchievementManager::skull_yay)
	{
		if(AchievementManager::bomb_yay && AchievementManager::skull_yay)
			achievementunlocked->setText("Oh dayum two achievements!");
		else
			achievementunlocked->setText("You have just unlocked an achievement!");
		achievementunlocked->setVisible(true);
		AchievementManager::achieve_timer += dt;

		if (AchievementManager::achieve_timer > AchievementManager::achieve_countdown)
		{
			AchievementManager::achieve_timer = 0.0f;
			AchievementManager::bomb_yay = false;
			AchievementManager::skull_yay = false;
			achievementunlocked->setVisible(false);
		}
	}
	
	m_Map.Update(dt);
	m_PowerUpManager.Update(dt);
	m_WeatherManager.Update(dt);

}

void GameSceneLevel1::ToggleGameOverMenu(bool toggle) {

    m_RestartImage->setVisible(toggle ? true : false);
    m_MainMenuImage->setVisible(toggle ? true : false);

	if (toggle) {

		Size winSize = Director::getInstance()->getWinSize();
		Vec2 destination(winSize.width * 0.5f, winSize.height * 0.75f);
		auto moveDown = MoveTo::create(1, destination);
		auto moveDown_Ease = EaseBounceOut::create(moveDown->clone());

		Vec2 destination2(winSize.width * 0.5f, winSize.height * 0.575f);
		auto moveDown2 = MoveTo::create(1, destination2);
		auto moveDown_Ease2 = EaseBounceOut::create(moveDown2->clone());

		if (m_Victory) {
			m_VictoryImage->setVisible(true);
			m_VictoryImage->setPositionY(winSize.height);
			m_VictoryImage->runAction(moveDown_Ease);

			lblVictoryTimer->setVisible(true);
			lblVictoryTimer->runAction(moveDown_Ease2);

			string str = "Time Taken\n" + to_string((int)m_TimeTaken) + string("s");
			lblVictoryTimer->setString(str);

#ifdef SDKBOX_ENABLED
			m_ShareToFBButton->setVisible(true);
#endif
		}
		else {
			m_DefeatImage->setVisible(true);
			m_DefeatImage->setPositionY(winSize.height);
			m_DefeatImage->runAction(moveDown_Ease);
		}

	}
	else {
		m_VictoryImage->setVisible(false);
		m_DefeatImage->setVisible(false);
	}

}

void GameSceneLevel1::CreateStatsFontLabel() {
	Label * lblIncreaseStat = Label::createWithTTF("[+ 1]", "fonts/Marker Felt.ttf", 15);
	lblIncreaseStat->setTextColor(Color4B::YELLOW);
	switch (m_Player.m_consumePowerUpValue) {
	case 0:
		lblIncreaseStat->setPosition(Vec2(m_GameSize.width * 0.24f, m_GameSize.height * 0.93f));
		break;
	case 1:
		lblIncreaseStat->setPosition(Vec2(m_GameSize.width * 0.64f, m_GameSize.height * 0.93f));
		break;
	case 2:
		lblIncreaseStat->setPosition(Vec2(m_GameSize.width * 0.44f, m_GameSize.height * 0.93f));
		break;
	case 3:
		lblIncreaseStat->setPosition(Vec2(m_GameSize.width * 0.84f, m_GameSize.height * 0.93f));
		break;
	}


	lblIncreaseStat->setAlignment(TextHAlignment::CENTER);
	this->addChild(lblIncreaseStat, 1);

	auto jumpy = JumpBy::create(20, Vec2(-50, -30), 100, 1);
	auto fadehue = FadeOut::create(2.5f);

	auto jumpfade = Spawn::create(jumpy, fadehue, nullptr);
	auto remove = RemoveSelf::create(); // clean up memory
	auto doubletrouble = Sequence::create(jumpfade, remove, nullptr);

	lblIncreaseStat->runAction(doubletrouble);
}

void GameSceneLevel1::OnKeyReleased(EventKeyboard::KeyCode keycode, Event * event) {
	if (keycode == EventKeyboard::KeyCode::KEY_BACK) {
		OnPauseButtonPressed();
	}

	if (keycode == EventKeyboard::KeyCode::KEY_EQUAL) {
		m_EnemyContainer.clear();
	}
}

void GameSceneLevel1::OnRestartPressed() {

	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Stage1Music.mp3", true, AudioManagerManager::GlobalBGMvalue);
	for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::BGM_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::BGM_IDs.push_back(AudioManagerManager::tempid);
	if (m_IsShowingGameOver) 
	{

		// Restart/Next scene
		this->cleanup();

		if (m_Victory) {
			auto scene = GameSceneLevel2::createScene();
			Director::getInstance()->replaceScene(scene);
		}
		else {
			auto scene = GameSceneLevel1::createScene();
			Director::getInstance()->replaceScene(scene);
		}
	}
}

void GameSceneLevel1::OnBackMainMenuPressed() {
	experimental::AudioEngine::stopAll();
	// Back to main menu
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	this->cleanup();
	ResetResolution();
	auto scene = MainMenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionSlideInL::create(TRANSIT_TIME, scene));

	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/ThemeSong.mp3", true, AudioManagerManager::GlobalBGMvalue);
	for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::BGM_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::BGM_IDs.push_back(AudioManagerManager::tempid);

	if (Director::getInstance()->isPaused()) {
		Director::getInstance()->resume();
	}
}

void GameSceneLevel1::OnPauseButtonPressed() {
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	if (!Director::getInstance()->isPaused() && !m_IsShowingGameOver) {
		Director::getInstance()->pause();
		lblPauseTitle->setVisible(true);
		m_PauseToMainMenuButton->setVisible(true);
		m_ResumeButton->setVisible(true);
		
		experimental::AudioEngine::pauseAll();
	}
}

void GameSceneLevel1::OnResumeButtonPressed() {
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	if (Director::getInstance()->isPaused()) {
		Director::getInstance()->resume();
		lblPauseTitle->setVisible(false);
		m_PauseToMainMenuButton->setVisible(false);
		m_ResumeButton->setVisible(false);
		
		experimental::AudioEngine::resumeAll();
	}
}

void GameSceneLevel1::OnShareButtonPressed() {
#ifdef SDKBOX_ENABLED
    if (sdkbox::PluginFacebook::isLoggedIn() == false)
        sdkbox::PluginFacebook::login();
#endif
}

#ifdef SDKBOX_ENABLED
void GameSceneLevel1::onLogin(bool isLogin, const std::string &msg) {
    if (sdkbox::PluginFacebook::isLoggedIn()){
        sdkbox::FBShareInfo info;
        info.type = sdkbox::FB_LINK;
        info.title = "Ryanomb";
        info.link = "http://fliprunner.000webhostapp.com/ryanomb.html";
        info.text = "Wow, I've just got a high score of " + to_string(m_TimeTaken); // Doesnt work, can't seem to find a solution for this
        sdkbox::PluginFacebook::dialog(info);
    }
}
#endif
