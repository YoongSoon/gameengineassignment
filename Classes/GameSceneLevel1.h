#pragma once

#include "cocos2d.h"
#include "Player.h"
#include "ui/CocosGUI.h"
#include "PowerUpManager.h"
#include "WeatherManager.h"

#ifdef SDKBOX_ENABLED
#include "PluginFacebook.h"
#endif

using namespace cocos2d;
using namespace cocos2d::ui;

class EnemyBase;

class GameSceneLevel1 : public cocos2d::Scene

#ifdef SDKBOX_ENABLED
		, public sdkbox::FacebookListener
#endif

{

public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	virtual ~GameSceneLevel1();

	// void ChangeScene(float dt);

	// implement the "static create()" method manually
	CREATE_FUNC(GameSceneLevel1);

	void OnKeyReleased(EventKeyboard::KeyCode keycode, Event* event);
	void OnRestartPressed();
	void OnBackMainMenuPressed();
	void OnPauseButtonPressed();
	void OnResumeButtonPressed();
	void OnShareButtonPressed();

#ifdef SDKBOX_ENABLED
    // Facebook Listeners
    virtual void onSharedFailed(const std::string& message) {};
    virtual void onSharedCancel() {};
    virtual void onAPI(const std::string& key, const std::string& jsonData) {};
    virtual void onPermission(bool isLogin, const std::string& msg) {};
    virtual void onFetchFriends(bool ok, const std::string& msg) {};
    virtual void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends ) {};
    virtual void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg ){};
    virtual void onInviteFriendsResult( bool result, const std::string& msg ){};
    virtual void onGetUserInfo( const sdkbox::FBGraphUser& userInfo ) {};
    virtual void onRequestGiftResult(bool result, const std::string& msg) {}
    virtual void onSendGiftResult(bool result, const std::string& msg) {}
	virtual void onLogin(bool isLogin, const std::string& msg);
	virtual void onSharedSuccess(const std::string& message){
		m_ShareToFBButton->setEnabled(false);
		// TODO: Add floating text
	}
#endif

private:

	void update(float dt);
	
	Player m_Player;
	Player m_Player2;

	MapHandler m_Map;

	PowerUpManager m_PowerUpManager;
	WeatherManager m_WeatherManager;

	std::vector<EnemyBase*> m_EnemyContainer;
	
	Label* lblPlayer_0_Title;
	Label* lblPlayer_0_Health;
	Label* lblPlayer_0_BombCount;
	Label* lblPlayer_0_Speed;
	Label* lblPlayer_0_Range;

	Label* lblPlayer_1_Title;
	Label* lblPlayer_1_Health;
	Label* lblPlayer_1_BombCount;
	Label* lblPlayer_1_Speed;
	Label* lblPlayer_1_Range;

	CCSprite* m_VictoryImage;
	CCSprite* m_DefeatImage;
	Button* m_RestartImage;
	Button* m_MainMenuImage;

	Label* lblPauseTitle;
	Button * m_PauseButton;
	Button * m_ResumeButton;
	Button * m_PauseToMainMenuButton;

	// Win Timer
	Label* lblCurrentTimer;
	Label* lblVictoryTimer;
	Button * m_ShareToFBButton;
	float m_TimeTaken = 0;
	
	bool m_Victory = false;
	bool m_IsShowingGameOver = false;

	Size m_OriginalDesignResolution = Size(480, 320);

	void ResetResolution() {
		Director::getInstance()->getOpenGLView()->setDesignResolutionSize(m_OriginalDesignResolution.width, m_OriginalDesignResolution.height, Director::getInstance()->getOpenGLView()->getResolutionPolicy());
	}

	void ToggleGameOverMenu(bool toggle);

	Size m_GameSize;
	void CreateStatsFontLabel();

	Text * achievementunlocked;
};
