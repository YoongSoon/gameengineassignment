#include "LevelSelectionScene.h"
#include  "MainMenuScene.h"
#include "Definition.h"
#include "GameSceneLevel1.h"
#include "GameSceneLevel2.h"
#include "GameSceneLevel3.h"
#include "SpriteSheetLoader.h"

cocos2d::Scene * LevelSelectionScene::createScene()
{
	// scene is an autorelease object
	auto scene = Scene::create();

	// layer is an autorelease object
	auto layer = LevelSelectionScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}

bool LevelSelectionScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}


	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto backgroundSprite_1 = Sprite::create("scrollingBackGround.png");
	backgroundSprite_1->setAnchorPoint(Vec2(0, 0));
	SpriteSheetLoader::RescaleToScreenSize(backgroundSprite_1);
	this->addChild(backgroundSprite_1);


	//Level Select title sprite
	auto titleSprite = Sprite::create("levelSelectLogo.png");
	titleSprite->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height + origin.y - titleSprite->getContentSize().width / 6.0f);
	SpriteSheetLoader::RescaleToScreenSize(titleSprite);
	this->addChild(titleSprite);


	//Create the page view
	m_pageView = PageView::create();
	m_pageView->setAnchorPoint(Vec2(0.5, 0.5));
	m_pageView->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	m_pageView->setContentSize(visibleSize);
	this->addChild(m_pageView);

	//store the map file path
	char str[100] = { 0 };

	//Loop through 3 times , since there is only 3 map
	for (int i = 0; i < 3; ++i)
	{

		//Create the layout to be used for pageview
		auto layout = Layout::create();
		layout->setContentSize(m_pageView->getContentSize());

		sprintf(str, "TileMaps/campaign_level%d.tmx", i + 1);

		//Create the map 
		auto map = TMXTiledMap::create(str);
		map->setScale(map->getScaleX() / 1.25f, map->getScaleY() / 1.25f);
		SpriteSheetLoader::RescaleToScreenSize(map);
		//Center the map to center of the screen
		map->setPositionX((m_pageView->getContentSize().width / 2 - map->getContentSize().width / 2.0f) / map->getScaleX());
		map->setPositionY((m_pageView->getContentSize().height / 2 - map->getContentSize().height / 2.0f) / map->getScaleY());
		//add the map as child of layout
		layout->addChild(map);
		//insert the layout to the pageview with a certain index
		m_pageView->insertPage(layout, i);

	}


	//Disable page from scrolling when the layout is click and drag
	m_pageView->setTouchEnabled(false);

	//back button
	backButton = Button::create("backLogo.png");
	backButton->setPosition(Vec2(origin.x + visibleSize.width / 10.f,
		visibleSize.height - visibleSize.height / 10.0f));
	backButton->addTouchEventListener(CC_CALLBACK_2(LevelSelectionScene::ReturnToMainMenu, this));
	SpriteSheetLoader::RescaleToScreenSize(backButton);
	this->addChild(backButton);

	//Left arrrow button
	leftArrowButton = Button::create("leftarrowicon.png");
	leftArrowButton->setPosition(Vec2(origin.x + leftArrowButton->getContentSize().width,
		visibleSize.height / 2.0f + origin.y));
	leftArrowButton->addTouchEventListener(CC_CALLBACK_2(LevelSelectionScene::SwitchLeft, this));
	SpriteSheetLoader::RescaleToScreenSize(leftArrowButton);
	this->addChild(leftArrowButton);

	//right arrow button
	rightArrowButton = Button::create("rightarrowicon.png");
	rightArrowButton->setPosition(Vec2(visibleSize.width - rightArrowButton->getContentSize().width,
		visibleSize.height / 2.0f + origin.y));
	rightArrowButton->addTouchEventListener(CC_CALLBACK_2(LevelSelectionScene::SwitchRight, this));
	SpriteSheetLoader::RescaleToScreenSize(rightArrowButton);
	this->addChild(rightArrowButton);


	////select  button
	selectButton = Button::create("gologo.png");
	selectButton->setPosition(Vec2(origin.x + visibleSize.width / 2.0f,
		 visibleSize.height / 10));
	selectButton->addTouchEventListener(CC_CALLBACK_2(LevelSelectionScene::ChangeScene, this));
	SpriteSheetLoader::RescaleToScreenSize(selectButton);
	this->addChild(selectButton);

	auto listener1 = EventListenerTouchOneByOne::create();


	return true;
}


void LevelSelectionScene::SwitchLeft(cocos2d::Ref * sende, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	//get current page view index
	currentPageIndex = m_pageView->getCurrentPageIndex();
	if (currentPageIndex > 0)
	{
		//Scroll to the pageview with that set index
		m_pageView->scrollToItem(--currentPageIndex);

		//Scale up the button when is pressed
		if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {

			//leftArrowButton->setScale(leftArrowButton->getScaleX() * 1.2f, leftArrowButton->getScaleY() * 1.2f);
			SpriteSheetLoader::ScaleNode(*leftArrowButton, 1.2f);
		}
		//Scale down the button when is not pressed
		else if (cocos2d::ui::Widget::TouchEventType::ENDED == eventType)
		{
			SpriteSheetLoader::ScaleNode(*leftArrowButton, 1.2f, true);
		}
	
	}

}

void LevelSelectionScene::SwitchRight(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	currentPageIndex = m_pageView->getCurrentPageIndex();
	if (currentPageIndex < 2)
	{
		m_pageView->scrollToItem(++currentPageIndex);

		//Scale up the button when is pressed
		if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {

			SpriteSheetLoader::ScaleNode(*rightArrowButton, 1.2f);
		}
		//Scale down the button when is not pressed
		else if (cocos2d::ui::Widget::TouchEventType::ENDED == eventType)
		{
			SpriteSheetLoader::ScaleNode(*rightArrowButton, 1.2f, true);
		}
	}


}

void LevelSelectionScene::ChangeScene(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	//Scale up the button when is pressed
	if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {

		SpriteSheetLoader::ScaleNode(*selectButton, 1.2f);
	}
	//Scale down the button when is not pressed
	else if (cocos2d::ui::Widget::TouchEventType::ENDED == eventType)
	{
		SpriteSheetLoader::ScaleNode(*selectButton, 1.2f, true);
	}


	Scene* scene;

	switch (currentPageIndex)
	{
	case 0:
		scene = GameSceneLevel1::createScene();
		Director::getInstance()->replaceScene(TransitionSlideInR::create(TRANSIT_TIME, scene));
		break;
	case 1:
		scene = GameSceneLevel2::createScene();
		Director::getInstance()->replaceScene(TransitionSlideInR::create(TRANSIT_TIME, scene));
		break;
	case 2:
		scene = GameSceneLevel3::createScene();
		Director::getInstance()->replaceScene(TransitionSlideInR::create(TRANSIT_TIME, scene));
		break;
	}


}



void LevelSelectionScene::ReturnToMainMenu(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);
	//Scale down the button when is pressed
	if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {

		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, false);
	}
	//Scale down the button when is not pressed
	else if (cocos2d::ui::Widget::TouchEventType::ENDED == eventType)
	{
		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, true);

		auto scene = MainMenuScene::createScene();
		Director::getInstance()->replaceScene(TransitionSlideInL::create(TRANSIT_TIME, scene));
	}
}
