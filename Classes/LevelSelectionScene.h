#pragma once
#include "cocos2d.h"
#include "ui/CocosGUI.h"
using namespace cocos2d::ui;

using namespace cocos2d;

class LevelSelectionScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	void SwitchLeft(cocos2d::Ref* sende , cocos2d::ui::Widget::TouchEventType eventType);
	void SwitchRight(cocos2d::Ref* sender , cocos2d::ui::Widget::TouchEventType eventType);
	void ChangeScene(cocos2d::Ref* sender , cocos2d::ui::Widget::TouchEventType eventType);
	void ReturnToMainMenu(cocos2d::Ref* sender , cocos2d::ui::Widget::TouchEventType eventType);

	// implement the "static create()" method manually
	CREATE_FUNC(LevelSelectionScene);

private:
	PageView * m_pageView;
	Button * leftArrowButton;
	Button * rightArrowButton;
	Button * selectButton;
	Button * backButton;
	int currentPageIndex;
};
