#include "LoadingScene.h"
#include "SplashScene.h"
#include "Definition.h"
#include "SpriteSheetLoader.h"
#include "AppDelegate.h"
#include "AudioEngine.h"

cocos2d::Scene * LoadingScene::createScene()
{
	// scene is an autorelease object
	auto scene = Scene::create();

	// layer is an autorelease object
	auto layer = LoadingScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}

bool LoadingScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	Size winSize = Director::getInstance()->getWinSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//Add a label that shows  "Screen is loading"
	//create and initailize the label
	auto loadingLabel = Label::createWithTTF("Loading Next Scene", "fonts/Marker Felt.ttf", 24);
	if (loadingLabel != nullptr)
	{
		//position the label on the center of the screen
		loadingLabel->setPosition(Vec2((int)winSize.width / 2, (int)winSize.height - winSize.height / 10));
		
		//add the label as a child to this layer
		this->addChild(loadingLabel, 1);
	}

	//Add a lable that shows % loading
	 textLabel = Label::createWithTTF("0%", "fonts/Marker Felt.ttf", 24);
	if (textLabel != nullptr)
	{
		//position the label on the center of the screen
		textLabel->setPosition(Vec2((int)winSize.width /2 , (int)winSize.height  - winSize.height /5));


		//add the label as a child to this layer
		this->addChild(textLabel, 1);
	}

	//add the string name of the image

	//SplashScene Resource
	Resources.push_back("nypLogo.png");

	//MainMenuScene resource
	Resources.push_back("MenuTitle.png");
	Resources.push_back("playlogo.png");
	Resources.push_back("optionslogo.png");
	Resources.push_back("quitlogo.png");

	//LevelSelectionScene resource
	Resources.push_back("leftarrowicon.png");
	Resources.push_back("rightarrowicon.png");
	Resources.push_back("gologo.png");
	// Resources.push_back("goSelectLogo.png");
	Resources.push_back("levelSelectLogo.png");

	//PlayerModeSelectionScene resource
	Resources.push_back("playerModeLogo.png");
	Resources.push_back("backLogo.png");
	Resources.push_back("singlePlayerLogo.png");
	Resources.push_back("multiPlayerLogo.png");
	Resources.push_back("nextlogo.png");

	//GameSceneLevel1Scene  Resource
	Resources.push_back("pausebutton.png");
	Resources.push_back("resumebutton.png");
	Resources.push_back("EnemyPlaceBombDown.png");
	Resources.push_back("EnemyChompDown.png");
	Resources.push_back("fidgetspinner.png");
	Resources.push_back("TileMaps/Individual/berry.png");
	Resources.push_back("TileMaps/Individual/carrot.png");
	Resources.push_back("TileMaps/Individual/flower.png");
	Resources.push_back("TileMaps/Individual/corn.png");
	Resources.push_back("TileMaps/Individual/lava.png");
	Resources.push_back("facebookshare.png");
	Resources.push_back("Trophy.png");
	Resources.push_back("bombtrophy.png");
	Resources.push_back("skulltrophy.png");
	Resources.push_back("reset.png");
	Resources.push_back("locked.png");
	Resources.push_back("bosstrophy.png");

	//load the first resource
	auto director = Director::getInstance();
	TextureCache * textureCache = director->getTextureCache();
	textureCache->addImageAsync(Resources[0], CC_CALLBACK_1(LoadingScene::LoadingTextureFinished, this));


	//loading bar UI
	loadingbar = ui::LoadingBar::create("fidgetspinner.png");
	loadingbar->setScale(0.5);
	loadingbar->setPosition(Vec2(winSize.width / 2, winSize.height /2 - winSize.height / 10));
	loadingbar->setPercent(0);
    // SpriteSheetLoader::RescaleToScreenSize(loadingbar);
	this->addChild(loadingbar);

	auto action = RotateBy::create(5.0f, 180);
	loadingbar->runAction(RepeatForever::create(action));

	//Run update loop
	this->scheduleUpdate();
    
	experimental::AudioEngine::preload("Audio/ThemeSong.mp3");
	experimental::AudioEngine::preload("Audio/Stage1Music.mp3");
	experimental::AudioEngine::preload("Audio/Rain.mp3");
	experimental::AudioEngine::preload("Audio/Snow.mp3");
	experimental::AudioEngine::preload("Audio/Wind.mp3");
	experimental::AudioEngine::preload("Audio/Sunny.mp3");
	experimental::AudioEngine::preload("Audio/Rain.mp3");
	experimental::AudioEngine::preload("Audio/enemyouch.mp3");
	experimental::AudioEngine::preload("Audio/playerouch.mp3");
	experimental::AudioEngine::preload("Audio/Click.mp3");
	experimental::AudioEngine::preload("Audio/explosion.mp3");
	experimental::AudioEngine::preload("Audio/MissileLaunch.mp3");

	if (!UserDefault::getInstance()->getBoolForKey("BGMMuted"))
		AudioManagerManager::GlobalBGMvalue = UserDefault::getInstance()->getFloatForKey("GlobalBGMvalue", 1);
	else
		AudioManagerManager::GlobalBGMvalue = 0;
	if (!UserDefault::getInstance()->getBoolForKey("SFXMuted"))
		AudioManagerManager::GlobalSFXvalue = UserDefault::getInstance()->getFloatForKey("GlobalSFXvalue", 1);
	else
		AudioManagerManager::GlobalSFXvalue = 0;

	AchievementManager::bombTrophyUnlocked = UserDefault::getInstance()->getBoolForKey("bombtrophy", false);
	AchievementManager::skullTrophyUnlocked = UserDefault::getInstance()->getBoolForKey("skulltrophy", false);
	AchievementManager::bossTrophyUnlocked = UserDefault::getInstance()->getBoolForKey("bosstrophy", false);
	return true;
}

void LoadingScene::update(float dt)
{
}

void LoadingScene::LoadingTextureFinished(Texture2D * texture)
{
	auto director = Director::getInstance();
	currResourceIndex++;

	//Update the % in the loading screen
	int perecentage = currResourceIndex * 100 / Resources.size();
	textLabel->setString(CCString::createWithFormat("%d%%", perecentage)->getCString());
	loadingbar->setPercent(perecentage);

	if (currResourceIndex < (int)Resources.size())
	{
		//Load the next resource
		TextureCache *  textureCache = director->getTextureCache();
		textureCache->addImageAsync(Resources[currResourceIndex],
			CC_CALLBACK_1(LoadingScene::LoadingTextureFinished, this));
	}
	else
	{
		auto scene = SplashScene::createScene();
		return director->replaceScene(scene);
	}

}

