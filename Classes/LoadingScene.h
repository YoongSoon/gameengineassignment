#pragma once

#include "cocos2d.h"
#include <vector>
#include <string>

#include "ui/CocosGUI.h"
#include "AudioManagerManager.h"
using namespace cocos2d::ui;

using namespace cocos2d;
using std::vector;
using std::string;

class LoadingScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	void update(float dt);

	//Recursive function that is responsible to load all the texture 
	// before changing to next scene
	void LoadingTextureFinished(Texture2D * texture);

	// implement the "static create()" method manually
	CREATE_FUNC(LoadingScene);

private:
	//store the name of texture file in string
	vector<string> Resources;
	// current index of the Resources container
	int currResourceIndex = 0;

	// a label that shows the % loading 
	Label* textLabel;

	ui::LoadingBar * loadingbar;
};
