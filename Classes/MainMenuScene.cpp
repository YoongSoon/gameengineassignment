#include "MainMenuScene.h"
#include "LevelSelectionScene.h"
#include  "Definition.h"
#include "SpriteSheetLoader.h"
#include "OptionScene.h"
#include "AchievementScene.h"
#include "AudioEngine.h"

cocos2d::Scene * MainMenuScene::createScene()
{
	// scene is an autorelease object
	auto scene = Scene::create();

	// layer is an autorelease object
	auto layer = MainMenuScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}


bool MainMenuScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}


	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	//get window size
	winSize = Director::getInstance()->getWinSize();

	//background 1 sprite
	backgroundSprite_1 = Sprite::create("scrollingBackGround.png");
	backgroundSprite_1->setAnchorPoint(Vec2(0, 0));
	SpriteSheetLoader::RescaleToScreenSize(backgroundSprite_1);
	this->addChild(backgroundSprite_1);

	//background 2 sprite
	backgroundSprite_2 = Sprite::create("scrollingBackGround.png");
	backgroundSprite_2->setAnchorPoint(Vec2(0, 0));
	backgroundSprite_2->setPosition(-winSize.width, 0);
	SpriteSheetLoader::RescaleToScreenSize(backgroundSprite_2);
	this->addChild(backgroundSprite_2);

	//Run background sprite actions
	auto spriteActionBackground_1 = RepeatForever::create(MoveBy::create(30.0f, Vec2(winSize.width, 0)));
	backgroundSprite_1->runAction(spriteActionBackground_1);
	auto spriteActionBackground_2 = RepeatForever::create(MoveBy::create(30.0f, Vec2(winSize.width, 0)));
	backgroundSprite_2->runAction(spriteActionBackground_2);


	// Screen real dimensions
	auto vSize = Director::getInstance()->getVisibleSize();
	auto vWidth = vSize.width;
	auto vHeight = vSize.height;


	//Menu title sprite
	titleSprite = Sprite::create("MenuTitle.png");
	titleSprite->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height);
	SpriteSheetLoader::RescaleToScreenSize(titleSprite);
	this->addChild(titleSprite);

	//Play image
	playItem = MenuItemImage::create("playlogo.png", "playlogo.png", CC_CALLBACK_1(MainMenuScene::ChangeScene, this));
	playItem->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);
	SpriteSheetLoader::RescaleToScreenSize(playItem);

	//option image
	optionItem = MenuItemImage::create("optionslogo.png", "optionslogo.png", CC_CALLBACK_1(MainMenuScene::ChangeToOptionScene, this));
	optionItem->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - visibleSize.height / 6.0f);
	SpriteSheetLoader::RescaleToScreenSize(optionItem);

	//quit image
	quitItem = MenuItemImage::create("quitlogo.png", "quitlogo.png", CC_CALLBACK_1(MainMenuScene::MenuCloseCallback, this));
	quitItem->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - visibleSize.height / 3.0f);
	SpriteSheetLoader::RescaleToScreenSize(quitItem);

	//Trophy image
	trophyItem = MenuItemImage::create("Trophy.png", "Trophy.png", CC_CALLBACK_1(MainMenuScene::ChangeToAchievementScene, this));
	trophyItem->setPosition(visibleSize.width * 3 / 4 + origin.x, visibleSize.height / 2 + origin.y);
	trophyItem->setScale(trophyItem->getScaleX() / 2, trophyItem->getScaleY() / 2);
	SpriteSheetLoader::RescaleToScreenSize(trophyItem);

	//add the menu items
	auto menu = Menu::create(playItem, optionItem, quitItem, trophyItem, nullptr);
	menu->setPosition(Point::ZERO);
	this->addChild(menu);
	
	//naming convention for sprite sheet = "nameSheet.plist" / "nameSheet.png" 
	//name convention for getting individual sprite in sprite sheet = "name (value).png" , where value is just an int value like 1
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneMoveRightSheet.plist");
	auto frames = SpriteSheetLoader::GetSpriteFrames("playerOneMoveRight", 3);
	spriteMoveRight = Sprite::createWithSpriteFrame(frames.front());
	spriteMoveRight->setScale(spriteMoveRight->getScaleX() * 3.0f, spriteMoveRight->getScaleY() * 3.0f);
	SpriteSheetLoader::RescaleToScreenSize(spriteMoveRight);
	spriteMoveRight->setPosition(origin.x + spriteMoveRight->getBoundingBox().size.width / 2.0f, origin.y + spriteMoveRight->getBoundingBox().size.height / 2.0f);
	this->addChild(spriteMoveRight);

	//Run moving right animation
	auto animation = Animation::createWithSpriteFrames(frames, 1.0f / 3);
	spriteMoveRight->runAction(RepeatForever::create(Animate::create(animation)));
		
	//Run update loop
	this->scheduleUpdate();
	
	return true;
}

void MainMenuScene::update(float dt)
{
	//Title font moving down the screen
	float translateDown = titleSprite->getPositionY() - dt * TITLE_TRANSLATE_SPEED;
	totalTranslateDist += dt * TITLE_TRANSLATE_SPEED;

	float threshold = (winSize.height / 5.0f);

	//stop title front from translating after reaching the threshold distance
	if (totalTranslateDist < threshold)
		titleSprite->setPositionY(translateDown);


	const float edgeOffset = 1.2f; // offset to prevent gadge between the first background and second background srptie

	//If first background reaches the end of the screen , reset it to default position of the second background
	if (backgroundSprite_1->getPosition().x >= winSize.width)
	{
		backgroundSprite_1->setPosition(-winSize.width + edgeOffset, 0);
	}
	//If second background reaches the end of the screen , reset to its default position
	if (backgroundSprite_2->getPosition().x >= winSize.width)
	{
		backgroundSprite_2->setPosition(-winSize.width + edgeOffset, 0);
	}


	//Scale the play/quit image when it is selected
	static bool isPlayPress = false;

	if (playItem->isSelected() && isPlayPress == false)
	{
		isPlayPress = true;
		playItem->setScale(playItem->getScaleX()* SCALE_FACTOR, playItem->getScaleY() * SCALE_FACTOR);
	}
	else if (!playItem->isSelected() && isPlayPress == true)
	{
		isPlayPress = false;
		playItem->setScale(playItem->getScaleX() / SCALE_FACTOR, playItem->getScaleY() / SCALE_FACTOR);
	}

	static bool isQuitPress = false;

	if (quitItem->isSelected() && isQuitPress == false)
	{
		isQuitPress = true;
		quitItem->setScale(quitItem->getScaleX()* SCALE_FACTOR, quitItem->getScaleY() * SCALE_FACTOR);
	}
	else if (!quitItem->isSelected() && isQuitPress == true)
	{
		isQuitPress = false;
		quitItem->setScale(quitItem->getScaleX() / SCALE_FACTOR, quitItem->getScaleY() / SCALE_FACTOR);
	}

	static bool isOptionPress = false;

	if (optionItem->isSelected() && isOptionPress == false)
	{
		isOptionPress = true;
		optionItem->setScale(optionItem->getScaleX()* SCALE_FACTOR, optionItem->getScaleY() * SCALE_FACTOR);
	}
	else if (!optionItem->isSelected() && isOptionPress == true)
	{
		isOptionPress = false;
		optionItem->setScale(optionItem->getScaleX() / SCALE_FACTOR, optionItem->getScaleY() / SCALE_FACTOR);
	}



	if (spriteMoveRight->getNumberOfRunningActions() == 1 && isMoveBack == false)
	{
		spriteMoveRight->stopAllActions();

		//right movement sprite
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneMoveRightSheet.plist");
		auto frames = SpriteSheetLoader::GetSpriteFrames("playerOneMoveRight", 3);
		auto  animation = Animation::createWithSpriteFrames(frames, 1.0f / 3);

		//Run moving right animation
		spriteMoveRight->runAction(RepeatForever::create(Animate::create(animation)));

		//move event
		auto moveEvent = MoveTo::create(15.0f, Vec2(visibleSize.width + 50.0f, origin.y + spriteMoveRight->getBoundingBox().size.height / 2.0f));
		spriteMoveRight->runAction(moveEvent);
		isMoveBack = true;
	}
	else  if (spriteMoveRight->getNumberOfRunningActions() == 1 && isMoveBack == true)
	{
		spriteMoveRight->stopAllActions();

		//left movement sprite
		SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneMoveLeftSheet.plist");
		auto framesLeft = SpriteSheetLoader::GetSpriteFrames("playerOneMoveLeft", 3);
		auto  animationLeft = Animation::createWithSpriteFrames(framesLeft, 1.0f / 3);

		//Run moving left animtion
		spriteMoveRight->runAction(RepeatForever::create(Animate::create(animationLeft)));

		//move event
		auto moveEvent = MoveTo::create(15.0f, Vec2(origin.x + spriteMoveRight->getContentSize().width / 2.0f - 50.0f,
			origin.y + spriteMoveRight->getBoundingBox().size.height / 2.0f));
		spriteMoveRight->runAction(moveEvent);
		isMoveBack = false;
	}
}

void MainMenuScene::ChangeScene(cocos2d::Ref* sender)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	auto scene = LevelSelectionScene::createScene();
	Director::getInstance()->replaceScene(TransitionSlideInR::create(TRANSIT_TIME, scene));
}

void MainMenuScene::ChangeToOptionScene(cocos2d::Ref * sender)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	auto scene = OptionScene::createScene();
	Director::getInstance()->replaceScene(TransitionSlideInR::create(TRANSIT_TIME, scene));
}

void MainMenuScene::MenuCloseCallback(Ref* pSender)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);


}

void MainMenuScene::ChangeToAchievementScene(cocos2d::Ref * pSender)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	auto scene = AchievementScene::createScene();
	Director::getInstance()->replaceScene(TransitionSlideInR::create(TRANSIT_TIME, scene));
}

