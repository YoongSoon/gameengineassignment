#pragma once

#include "cocos2d.h"
#include "AudioManagerManager.h"
#include "AudioEngine.h"
using namespace cocos2d;


class MainMenuScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	void update(float dt);

	void ChangeScene(cocos2d::Ref* sender);
	void ChangeToOptionScene(cocos2d::Ref* sender);
	void MenuCloseCallback(cocos2d::Ref* pSender);
	void ChangeToAchievementScene(cocos2d::Ref* pSender);

	// implement the "static create()" method manually
	CREATE_FUNC(MainMenuScene);

private:
	Sprite * titleSprite;
	const float TITLE_TRANSLATE_SPEED = 30.0f; // speed of title font translating down
	float totalTranslateDist = 0.f; // total distance the title font travelled while translating down

	const float SCALE_FACTOR = 1.2f; // the factor that affect how much the play and quit image scale when being selected
	MenuItemImage * playItem;
	MenuItemImage * optionItem;
	MenuItemImage * quitItem;
	MenuItemImage * trophyItem;

	Sprite * spriteMoveRight;
	Size visibleSize;
	Vec2 origin;
	bool isMoveBack = false; //stupid variable

	Sprite * backgroundSprite_1;
	Sprite * backgroundSprite_2;

	Size winSize;

};
