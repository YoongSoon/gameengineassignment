#include "MapHandler.h"
#include "GameObject.h"
#include "Definition.h"



void MapHandler::Init(const std::string & mapLocation, Scene* scene) {
	m_Scene = scene;

	Size winSize = Director::getInstance()->getWinSize();

	m_TileMap = new CCTMXTiledMap();
	m_TileMap->initWithTMXFile(mapLocation);
	m_TileMap->setScale(1.45f);
	m_TileMap->setZOrder(ZORDER_MAP);
	m_TileMap->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_TileMap->setPosition(Vec2(winSize.width * 0.5f, winSize.height * 0.465f));
	scene->addChild(m_TileMap);

	m_Boundaries = m_TileMap->layerNamed("Layout");

	m_DestructibleBoundaries = m_TileMap->layerNamed("Destructible");

	const float hexRadius = 7;

	Vec2 hexagon[] = {
		Vec2(0,1) * hexRadius,
		Vec2(0.75f,0.75f) * hexRadius,
		Vec2(1,0) * hexRadius,
		Vec2(0.75f,-0.75f) * hexRadius,
		Vec2(0,-1) * hexRadius,
		Vec2(-0.75f,-0.75f) * hexRadius,
		Vec2(-1,0) * hexRadius,
		Vec2(-0.75f, 0.75f) * hexRadius
	};

	// Spawn collider for all indestructible tiles
	for (int x = 0; x < MAP_WIDTH; ++x) {
		for (int y = 0; y < MAP_HEIGHT; ++y) {

			// GID 
			// 983 = Wall
			// 998 = Floor
			auto tileGID = m_Boundaries->getTileGIDAt(Vec2(x, y));
			auto tile = m_Boundaries->getTileAt(Vec2(x, y));

			auto destroytileGID = m_DestructibleBoundaries->getTileGIDAt(Vec2(x, y));
			auto destroytile = m_DestructibleBoundaries->getTileAt(Vec2(x, y));

			if (tileGID == 983) {
				PhysicsBody* pBody = PhysicsBody::createPolygon(&hexagon[0], 8, PhysicsMaterial(1, 0, 0));
				pBody->setDynamic(false);
				pBody->setName("boundaryBody");
				tile->addComponent(pBody);

			}
			else if (destroytileGID == 718) {
				PhysicsBody* pBody = PhysicsBody::createPolygon(&hexagon[0], 8, PhysicsMaterial(1, 0, 0));
				pBody->setDynamic(false);
				pBody->setName("boundaryBody");
				destroytile->addComponent(pBody);
			}
		}
	}

	// Init all the map tiles
	for (int x = 0; x < MAP_WIDTH; ++x) {
		for (int y = 0; y < MAP_HEIGHT; ++y) {

			auto tileGID = m_Boundaries->getTileGIDAt(Vec2(x, y));
			auto tile = m_Boundaries->getTileAt(Vec2(x, y));

			// Change origin from top-left to bot-left
			int actualY = (MAP_HEIGHT - 1) - y;

			// GID 
			// 718 = DestructibleWall

			auto dTileGID = m_DestructibleBoundaries->getTileGIDAt(Vec2(x, y));
			auto dTile = m_DestructibleBoundaries->getTileAt(Vec2(x, y));

			if (dTileGID == 718) {
				m_Map[x][actualY] = MapTile(MapTile::TILE_DESTRUCTIBLE, dTile, x, y);
				log("Found a destructible at: %i, %i", x, actualY);
			}
			else if (tileGID == 983) {
				m_Map[x][actualY] = MapTile(MapTile::TILE_WALL, tile, x, y);
			}
			else if (tileGID == 998) {
				m_Map[x][actualY] = MapTile(MapTile::TILE_FLOOR, tile, x, y);
			}

		}
	}

}

void MapHandler::Exit() {}

void MapHandler::Update(float dt) {

	// Partition check
	// Migrates any objects that has moved away from its previous tiles to its current tile
	for (int x = 0; x < MAP_WIDTH; ++x) {
		for (int y = 0; y < MAP_HEIGHT; ++y) {

			auto* mapTile = &m_Map[x][y];
			auto* m_GoList = &mapTile->m_Objects;

			for (auto it = m_GoList->begin(); it != m_GoList->end();) {

				GameObject* go = *it;

				auto curr = GetTilePosFromPos(go->getPosition());

				// Check if this object has moved out of this tile
				if (curr.x != x || curr.y != y) {
					m_Map[curr.x][curr.y].m_Objects.push_back(go); // add it to its supposed tile
					it = m_Map[x][y].m_Objects.erase(it); // Remove from old tile
					continue;
				}

				++it;
			}

		}
	}

	for (int x = 0; x < MAP_WIDTH; ++x) {
		for (int y = 0; y < MAP_HEIGHT; ++y) {

			auto* mapTile = &m_Map[x][y];

			auto* m_GoQueue = &mapTile->m_ObjectsQueue;
			auto* m_GoList = &mapTile->m_Objects;

			bool isDirty = !m_GoQueue->empty();

			// Push pending objects into queue at the beginning
			while (!m_GoQueue->empty()) {
				m_GoList->push_back(m_GoQueue->front());
				m_GoQueue->front()->setPosition(Vec2(m_Map[x][y].tileSprite->getPosition())); // Align sprite to the center
				m_GoQueue->front()->setVisible(true);
				m_GoQueue->pop();
			}

			// Erase all pending destroyed objects
			for (auto it = m_GoList->begin(); it != m_GoList->end();) {
				GameObject* go = *it;

				if (go->IsDestroyed()) {
					go->removeFromParentAndCleanup(true);
					it = m_GoList->erase(it);
					continue;
				}

				++it;
			}
		}
	}

}

void MapHandler::AddToMap(GameObject * object) {
	object->setVisible(false);
	auto tilePos = GetTilePosFromPos(object->getPosition());
	m_Map[tilePos.x][tilePos.y].m_ObjectsQueue.push(object);
}

void MapHandler::AddToRenderMap(Node* object) {
	if (object)
		m_TileMap->addChild(object);
}

void MapHandler::RemoveFromRenderMap(Node * object) {
	if (object)
		m_TileMap->removeChild(object, false);
}

std::vector<GameObject*> MapHandler::FindObjectsOfName(std::string name, bool includeRenderMap) {
	std::vector<GameObject*> objects;

	for (int x = 0; x < MAP_WIDTH; ++x) {
		for (int y = 0; y < MAP_HEIGHT; ++y) {

			auto* mapTile = &m_Map[x][y];
			auto* m_GoList = &mapTile->m_Objects;

			for (auto it = m_GoList->begin(); it != m_GoList->end(); ++it) {

				GameObject* go = *it;

				if (go->getName() == name && !go->IsDestroyed()) // Make sure this object is not pending for destruction
					objects.push_back(go);

			}

		}
	}

	if (includeRenderMap) {
		auto children = m_TileMap->getChildren();

		for (size_t i = 0; i < children.size(); ++i) {

			auto go = static_cast<GameObject*>(children.at(i));

			if (go->getName() == name) {
				objects.push_back(go);
			}
		}

	}

	return objects;
}

Vec2 MapHandler::GetSpawnPoint(Player::PLAYER_INDEX pIndex) {
	CCTMXObjectGroup *objectGroup = m_TileMap->objectGroupNamed("SpawnPoint");
	ValueMap playerSpawnPoint = objectGroup->objectNamed(std::to_string(pIndex) + "_SpawnPoint");
	return Vec2(playerSpawnPoint["x"].asFloat(), playerSpawnPoint["y"].asFloat());
}

TilePos MapHandler::GetRandomEmptyTilePos() {

	std::vector<TilePos> emptyTiles;

	for (int x = 0; x < MAP_WIDTH; ++x) {
		for (int y = 0; y < MAP_HEIGHT; ++y) {
			if (m_Map[x][y].tileType == MapTile::TILE_FLOOR && m_Map[x][y].IsEmpty()) {
				emptyTiles.push_back(TilePos(x, y));
			}
		}
	}

	if (!emptyTiles.empty()) {
		return emptyTiles[RandomHelper::random_int<int>(0, emptyTiles.size() - 1)];
	}
	else {
		return TilePos(-1, -1);
	}
}

TilePos MapHandler::GetTilePosFromPos(Vec2 pos) {
	TilePos tilePos = TilePos((int)roundf(pos.x / 16), (int)roundf(pos.y / 16));
	return tilePos;
}

Vec2 MapHandler::GetPosFromTilePos(TilePos pos) {
	Vec2 Pos = { (pos.x * 16.0f), (pos.y * 16.0f) };
	return Pos;
}

MapTile* MapHandler::GetTileFromPos(TilePos pos) {
	return &m_Map[pos.x][pos.y];
}

MapTile* MapHandler::GetTileFromPos(Vec2 pos) {
	TilePos tPos = GetTilePosFromPos(pos);
	return &m_Map[tPos.x][tPos.y];
}

void MapHandler::DestroyDestructibleWall(TilePos pos) {
	int actualY = (MAP_HEIGHT - 1) - pos.y;
	Vec2 pos2 = { (float)pos.x, (float)actualY };
	Vec2 pos1 = { (float)pos.x, (float)pos.y };

	// auto tile = m_DestructibleBoundaries->getTileAt(pos2); // this one should be top left is origin pos 1 is bottom left, pos 2 is topleft

	auto tile = GetTileFromPos(pos);

	if (tile != nullptr && tile->tileType == MapTile::TILE_DESTRUCTIBLE) {
		tile->tileSprite->getPhysicsBody()->setEnabled(false);
		tile->tileSprite->removeComponent("boundaryBody");

		m_DestructibleBoundaries->setTileGID(998, Vec2(tile->tilePos.x, tile->tilePos.y));
		tile->tileType = MapTile::TILE_FLOOR;
	}

}

void MapTile::AlignObjects() {
	for (auto go : m_Objects) {
		go->setPosition(Vec2(tileSprite->getPosition()));
	}
}