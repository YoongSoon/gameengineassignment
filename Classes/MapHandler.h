#pragma once

#include "cocos2d.h"
#include "Player.h"
#include <string>

USING_NS_CC;

class GameObject;

struct TilePos {
	TilePos(int x, int y) : x(x), y(y) {}
	TilePos() : x(0), y(0) {};
	void Set(int _x = 0, int _y = 0) { x = _x; y = _y; }

	int x;
	int y;
};

class MapTile {

public:
	enum TILE_TYPE {
		TILE_NONE,
		TILE_FLOOR,
		TILE_WALL,
		TILE_DESTRUCTIBLE
	};

	MapTile() : tileType(TILE_NONE), tileSprite(nullptr), tilePos(0, 0) {}
	MapTile(TILE_TYPE tileType, Sprite* sprite, int tileX, int tileY) : tileType(tileType), tileSprite(sprite), tilePos(tileX, tileY) {}

	TILE_TYPE tileType;

	// Sprite that renders this tile
	Sprite* tileSprite;

	// 2D array position
	TilePos tilePos;

	// Pending objects to be inserted into m_Objects
	std::queue<GameObject*> m_ObjectsQueue;

	// Collection of all the GameObjects in this tile
	std::vector<GameObject*> m_Objects;

	// Aligns everything in this tile to Grid
	void AlignObjects();

	// Are there any GameObjects in this tile?
	bool IsEmpty() { return m_ObjectsQueue.empty() && m_Objects.empty(); }

};

class MapHandler {

public:
	MapHandler() {};
	void Init(const std::string& mapLocation, Scene* scene);
	void Exit();

	void Update(float dt);

	// Inserts the given GameObject into the 2D map array
	void AddToMap(GameObject* object);

	// Inserts the given Object without adding into the 2D map array
	void AddToRenderMap(Node* object);

	// Removes the Object from rendering without cleaning it up
	void RemoveFromRenderMap(Node* object);

	// Returns a collection of active GameObjects with the given name
	// args includeRenderMap: Include GameObjects which are not added to m_Map
	std::vector<GameObject*> FindObjectsOfName(std::string name, bool includeRenderMap = false);

	// Retrieves the spawn point location of players in the .tmx file
	Vec2 GetSpawnPoint(Player::PLAYER_INDEX pIndex);

	// Retrieves a random empty TilePos, returns (-1,-1) if no empty tile is found
	TilePos GetRandomEmptyTilePos();

	// Converts the Vec2 to a TilePos
	TilePos GetTilePosFromPos(Vec2 pos);

	//Convert the TilePos into a Vec2
	Vec2 GetPosFromTilePos(TilePos pos);

	// Retrieves the MapTile that correspond to this grid
	MapTile* GetTileFromPos(TilePos pos);

	// Retrieves the MapTile that correspond to this world position
	MapTile* GetTileFromPos(Vec2 pos);

	//replace destructible wall with floor
	void DestroyDestructibleWall(TilePos pos);
	// Map's grid Height
	static const int MAP_HEIGHT = 13;

	// Map's grid Width
	static const int MAP_WIDTH = 15;

	MapTile m_Map[MAP_WIDTH][MAP_HEIGHT];
private:
	Scene * m_Scene;

	CCTMXTiledMap * m_TileMap;
	CCTMXLayer *m_Boundaries;
	CCTMXLayer *m_DestructibleBoundaries;
};