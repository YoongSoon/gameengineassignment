#include "Missile.h"

Missile::Missile(MapHandler * map, Vec2 pos)
	: GameObject(map)
{
	InitGO("missile.png");
	InitSize(pos, ZORDER_BOMBS);
	setName("missile");

	// need this line of code for DestroyMe() to work
	m_Map->AddToMap(this);

	// Init Physics Body
	auto m_PhysicsBody = PhysicsBody::createCircle(4, PhysicsMaterial(1, 0, 1));
	m_PhysicsBody->setRotationEnable(false);
	m_PhysicsBody->setDynamic(false);
	m_PhysicsBody->setContactTestBitmask(true);
	this->addComponent(m_PhysicsBody);

	// Collision response
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactBegin = CC_CALLBACK_1(Missile::onContactBegin, this);
	getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);
}

void Missile::Init()
{
	//Missile's current TilePos == Enemy's current TilePos
	m_currMissileTilePos = m_Map->GetTilePosFromPos(getPosition());

	//Get the default player TilePos
	playerTilePos = m_Map->GetTilePosFromPos(thePlayer->GetPlayerSprite()->getPosition());
	defaultPlayerTilePos = playerTilePos;

}

void Missile::update(float dt)
{
	log("%d , %d" ,m_currMissileTilePos.x , m_currMissileTilePos.y);

	m_lifeTime -= dt;

	// Destroy after X seconds
	if (m_lifeTime <= 0) {
		DestroyMe(); // Destroy myself
		return;
	}

	//Retrieve the latest playerTilePos
	playerTilePos = m_Map->GetTilePosFromPos(thePlayer->GetPlayerSprite()->getPosition());

	Point p1 = Point(defaultPlayerTilePos.x, defaultPlayerTilePos.y);
	Point p2 = Point(playerTilePos.x, playerTilePos.y);

	float distSquared = p1.getDistanceSq(p2);
	float dist = p1.getDistance(p2);

	//If the defaultPlayerTilePos and latest playerTilePos is greater than 4 grid size or the pathfinding path is empty(reached its original destination)
	// then restart the pathfidning
	if ((dist > 4 || m_pathTilePosVect.empty() == true) && b_startMoving == true)
	{
		m_pathFinder.AStarPathFinding(m_currMissileTilePos, playerTilePos, this, false);
		defaultPlayerTilePos = playerTilePos;
	}


	//Run the pathfinding movement
	if (m_pathTilePosVect.empty() == false && getNumberOfRunningActions() == 0)
	{
		//get the next current TilePos of enemy
		m_currMissileTilePos = m_pathTilePosVect[0];

		//Set the Vec2 pos of the sprite
		m_currVec2MissilePos = m_Map->m_Map[m_currMissileTilePos.x][m_currMissileTilePos.y].tileSprite->getPosition();


		//Move action 
		float timeToMove = (m_currVec2MissilePos - getPosition()).length() / MISSLE_SPEED;
		auto moveEvent = MoveBy::create(timeToMove, m_currVec2MissilePos - getPosition());
		runAction(moveEvent);

		m_pathTilePosVect.erase(m_pathTilePosVect.begin());

	}

}

PathFinding Missile::GetPathFinder()
{
	return m_pathFinder;
}

bool Missile::onContactBegin(PhysicsContact & contact)
{
	auto collideA = contact.getShapeA()->getBody()->getNode();
	auto collideB = contact.getShapeB()->getBody()->getNode();

	// Ignore collision response between missile and explosion
	if ((collideA->getName() == "missile" && collideB->getName() == "explosion") || (collideB->getName() == "explosion"  && collideA->getName() == "missile")) {
		return false;
	}

	// Ignore collision response  between missile and powerup
	if ((collideA->getName() == "missile" && collideB->getName() == "PowerUp") || (collideB->getName() == "missile"  && collideA->getName() == "PowerUp")) {
		return false;
	}

	// Ignore collision response  between missile and Enemy
	if ((collideA->getName() == "Enemy" && collideB->getName() == "missile") || (collideB->getName() == "missile"  && collideA->getName() == "Enemy")) {
		return false;
	}


	if ((collideA->getName().find("Player") != std::string::npos && collideB->getName() == "missile")
		|| (collideA->getName() == "missile"  && collideB->getName().find("Player") != std::string::npos)) {
		DestroyMe();
		thePlayer->DamagePlayer();
		return false;
	}


	return true;
}
