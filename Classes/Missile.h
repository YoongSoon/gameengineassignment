#pragma once

#include "GameObject.h"
#include "MapHandler.h"
#include "Definition.h"
#include "PathFinding.h"
#include "Player.h"

#include <vector>

USING_NS_CC;


class Missile : public GameObject
{
public:
	Missile(MapHandler* map, Vec2 pos);

	void Init();

	virtual void update(float dt);

	//Return the PathFinder
	PathFinding GetPathFinder();

	//Current TilePos of the Missile
	TilePos m_currMissileTilePos;

	//Current Vec2 Pos of the missile
	Vec2 m_currVec2MissilePos;

	//Container that stores the TilePos of the pathfinding
	std::vector<TilePos> m_pathTilePosVect;

	void SetPlayerInst(Player * _player) { thePlayer = _player; }

	//indicator to start the missile movement
	bool b_startMoving = false;

	//collision response
	bool onContactBegin(PhysicsContact& contact);

private:
	float m_lifeTime = 8.0f;

	const float MISSLE_SPEED = 20.0f;

	PathFinding m_pathFinder;

	TilePos playerTilePos;
	TilePos defaultPlayerTilePos;
	Player * thePlayer;

};