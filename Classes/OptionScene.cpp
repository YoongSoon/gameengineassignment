#include "OptionScene.h"
#include "LevelSelectionScene.h"
#include  "Definition.h"
#include "SpriteSheetLoader.h"
#include "AppDelegate.h"
#include "MainMenuScene.h"
#include "AudioEngine.h"

cocos2d::Scene * OptionScene::createScene()
{
	// scene is an autorelease object
	auto scene = Scene::create();

	// layer is an autorelease object
	auto layer = OptionScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}


bool OptionScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	visibleSize = Director::getInstance()->getVisibleSize();
	origin = Director::getInstance()->getVisibleOrigin();

	//background
	auto backgroundSprite_1 = Sprite::create("scrollingBackGround.png");
	backgroundSprite_1->setAnchorPoint(Vec2(0, 0));
	SpriteSheetLoader::RescaleToScreenSize(backgroundSprite_1);
	this->addChild(backgroundSprite_1);

	//title
	auto titleSprite = Sprite::create("OptionsTitleLogo.png");
	titleSprite->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height - titleSprite->getContentSize().width / 6.0f);
	SpriteSheetLoader::RescaleToScreenSize(titleSprite);
	this->addChild(titleSprite);

	//BGM mute title
	auto muteBGMTitleSprite = Sprite::create("MuteBGMLogo.png");
	muteBGMTitleSprite->setPosition(visibleSize.width / 5.0f
		, visibleSize.height / 2.0f + muteBGMTitleSprite->getContentSize().height);
	SpriteSheetLoader::RescaleToScreenSize(muteBGMTitleSprite);
	this->addChild(muteBGMTitleSprite);

	//SFX mute title
	auto muteSFXTitleSprite = Sprite::create("MuteSFXLogo.png");
	muteSFXTitleSprite->setPosition(visibleSize.width / 1.4f
		, visibleSize.height / 2.0f + muteSFXTitleSprite->getContentSize().height);
	SpriteSheetLoader::RescaleToScreenSize(muteSFXTitleSprite);
	this->addChild(muteSFXTitleSprite);

	// mute BGM check box 
	muteBGMCheckBox = CheckBox::create("checkbox_normal.png", "checkbox_normal_pressed.png");
	muteBGMCheckBox->setPosition(Vec2(visibleSize.width / 2.4f, visibleSize.height / 2.0f + muteBGMCheckBox->getContentSize().height / 1.5f));
	muteBGMCheckBox->setTouchEnabled(true);
	muteBGMCheckBox->setEnabled(true);
	muteBGMCheckBox->setSelected(UserDefault::getInstance()->getBoolForKey("BGMMuted"));
	muteBGMCheckBox->addClickEventListener(CC_CALLBACK_1(OptionScene::OnBGMMuteCallback, this));

	SpriteSheetLoader::RescaleToScreenSize(muteBGMCheckBox);
	this->addChild(muteBGMCheckBox);

	// mute SFX check box
	muteSFXCheckBox = CheckBox::create("checkbox_normal.png", "checkbox_normal_pressed.png");
	muteSFXCheckBox->setPosition(Vec2(visibleSize.width / 1.1f, visibleSize.height / 2.0f + muteSFXCheckBox->getContentSize().height / 1.5f));
	muteSFXCheckBox->setTouchEnabled(true);
	muteSFXCheckBox->setEnabled(true);
	muteSFXCheckBox->setSelected(UserDefault::getInstance()->getBoolForKey("SFXMuted"));
	muteSFXCheckBox->addClickEventListener(CC_CALLBACK_1(OptionScene::OnSFXMuteCallback, this));

	SpriteSheetLoader::RescaleToScreenSize(muteSFXCheckBox);
	this->addChild(muteSFXCheckBox);

	//back button
	backButton = Button::create("backLogo.png");
	backButton->setPosition(Vec2(origin.x + backButton->getContentSize().width / 1.2f,
		visibleSize.height - backButton->getContentSize().height / 2.0f));
	backButton->addTouchEventListener(CC_CALLBACK_2(OptionScene::ReturnToMainMenu, this));
	SpriteSheetLoader::RescaleToScreenSize(backButton);
	this->addChild(backButton);


	// the background volume slider
	backgroundVolumeSlider = Slider::create();
	backgroundVolumeSlider->loadBarTexture("emptybar.png"); // what the slider looks like
	backgroundVolumeSlider->loadSlidBallTextures("slideBall.png"); // what the slider looks like
	backgroundVolumeSlider->loadProgressBarTexture("fullPurpleProgressBar.png");
	backgroundVolumeSlider->setPosition(Vec2(visibleSize.width / 2 + origin.x + backgroundVolumeSlider->getContentSize().width / 4.0f
		, visibleSize.height / 2 + origin.y - visibleSize.height / 12.0f));
	SpriteSheetLoader::RescaleToScreenSize(backgroundVolumeSlider);
	if(!UserDefault::getInstance()->getBoolForKey("BGMMuted"))
	backgroundVolumeSlider->setPercent((int)(AudioManagerManager::GlobalBGMvalue * 100));
	else
		backgroundVolumeSlider->setPercent(0);
	backgroundVolumeSlider->addTouchEventListener(CC_CALLBACK_2(OptionScene::SetBackgroundAudioLevel, this));
	this->addChild(backgroundVolumeSlider);

	//the sound effect volume slider
	soundEffectVolumeSlider = Slider::create();
	soundEffectVolumeSlider->loadBarTexture("emptybar.png"); // what the slider looks like
	soundEffectVolumeSlider->loadSlidBallTextures("slideBall.png"); // what the slider looks like
	soundEffectVolumeSlider->loadProgressBarTexture("fullPurpleProgressBar.png");
	SpriteSheetLoader::RescaleToScreenSize(soundEffectVolumeSlider);
	soundEffectVolumeSlider->setPosition(Vec2(visibleSize.width / 2 + +origin.x + soundEffectVolumeSlider->getContentSize().width / 4.0f
		, visibleSize.height / 2 + origin.y - visibleSize.height / 4.0f));
	if (!UserDefault::getInstance()->getBoolForKey("SFXMuted"))
	soundEffectVolumeSlider->setPercent((int)(AudioManagerManager::GlobalSFXvalue * 100));
	else
		soundEffectVolumeSlider->setPercent(0);
	soundEffectVolumeSlider->addTouchEventListener(CC_CALLBACK_2(OptionScene::SetSoundEffectAudioLevel, this));
	this->addChild(soundEffectVolumeSlider);

	//bgm image
	auto bgmItem = MenuItemImage::create("backgroundMusicLogo.png", "backgroundMusicLogo.png");
	bgmItem->setPosition(visibleSize.width / 4, visibleSize.height / 2 + origin.y - visibleSize.height / 12.0f);
	SpriteSheetLoader::RescaleToScreenSize(bgmItem);

	//sfx image
	auto sfxItem = MenuItemImage::create("soundEffectLogo.png", "soundEffectLogo.png");
	sfxItem->setPosition(visibleSize.width / 4, visibleSize.height / 2 + origin.y - visibleSize.height / 4.0f);
	SpriteSheetLoader::RescaleToScreenSize(sfxItem);


	//All MenuItemImage is added into the menu
	auto menu = Menu::create(bgmItem, sfxItem, nullptr);
	menu->setPosition(Point::ZERO);
	this->addChild(menu);

	//Run update loop
	this->scheduleUpdate();

	return true;
}

void OptionScene::update(float dt)
{

}

void OptionScene::ChangeScene(cocos2d::Ref* sender)
{

}


void OptionScene::MenuCloseCallback(Ref* pSender)
{
	//Close the cocos2d-x game scene and quit the application
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif

	/*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/

	//EventCustom customEndEvent("game_scene_close_event");
	//_eventDispatcher->dispatchEvent(&customEndEvent);

}

void OptionScene::ReturnToMainMenu(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/Click.mp3", false, AudioManagerManager::GlobalSFXvalue);
	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::SFX_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);
	//Scale down the button when is pressed
	if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {

		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, false);
	}
	//Scale down the button when is not pressed
	else if (cocos2d::ui::Widget::TouchEventType::ENDED == eventType)
	{
		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, true);

		auto scene = MainMenuScene::createScene();
		Director::getInstance()->replaceScene(TransitionSlideInL::create(TRANSIT_TIME, scene));
	}
}

void OptionScene::SetBackgroundAudioLevel(Ref * sender, Widget::TouchEventType type)
{
	// save and edit the background audio level here
	if (cocos2d::ui::Widget::TouchEventType::ENDED == type)
	{
		AudioManagerManager::GlobalBGMvalue = ((float)backgroundVolumeSlider->getPercent() / 100);
		UserDefault::getInstance()->setFloatForKey("GlobalBGMvalue", AudioManagerManager::GlobalBGMvalue);
		for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
			experimental::AudioEngine::setVolume(AudioManagerManager::BGM_IDs[a], ((float)backgroundVolumeSlider->getPercent() / 100));

		UserDefault::getInstance()->setBoolForKey("BGMMuted", false);
		muteBGMCheckBox->setSelected(false);
	}
}

void OptionScene::SetSoundEffectAudioLevel(Ref * sender, Widget::TouchEventType type)
{
	// save and edit the sound effect audio level here
	if (cocos2d::ui::Widget::TouchEventType::ENDED == type)
	{
		AudioManagerManager::GlobalSFXvalue = ((float)soundEffectVolumeSlider->getPercent() / 100);
		UserDefault::getInstance()->setFloatForKey("GlobalSFXvalue", AudioManagerManager::GlobalSFXvalue);
		for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
			experimental::AudioEngine::setVolume(AudioManagerManager::SFX_IDs[a], ((float)soundEffectVolumeSlider->getPercent() / 100));

		UserDefault::getInstance()->setBoolForKey("SFXMuted", false);
		muteSFXCheckBox->setSelected(false);
	}
}

void OptionScene::OnBGMMuteCallback(Ref* sender) {

	auto cb = (CheckBox*)sender;

	if (cb->isSelected()) {
		AudioManagerManager::GlobalBGMvalue = UserDefault::getInstance()->getFloatForKey("GlobalBGMvalue");
		for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
			experimental::AudioEngine::setVolume(AudioManagerManager::BGM_IDs[a], AudioManagerManager::GlobalBGMvalue);

		backgroundVolumeSlider->setPercent(AudioManagerManager::GlobalBGMvalue * 100);

		UserDefault::getInstance()->setBoolForKey("BGMMuted", false);
	}
	else {
		AudioManagerManager::GlobalBGMvalue = 0;
		for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
			experimental::AudioEngine::setVolume(AudioManagerManager::BGM_IDs[a], 0);

		backgroundVolumeSlider->setPercent(0);

		UserDefault::getInstance()->setBoolForKey("BGMMuted", true);
	}

}

void OptionScene::OnSFXMuteCallback(Ref* sender) {

	auto cb = (CheckBox*)sender;

	if (cb->isSelected()) {
		AudioManagerManager::GlobalSFXvalue = UserDefault::getInstance()->getFloatForKey("GlobalSFXvalue");

		for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
			experimental::AudioEngine::setVolume(AudioManagerManager::SFX_IDs[a], AudioManagerManager::GlobalSFXvalue);

		soundEffectVolumeSlider->setPercent(AudioManagerManager::GlobalSFXvalue * 100);

		UserDefault::getInstance()->setBoolForKey("SFXMuted", false);
	}
	else {
		AudioManagerManager::GlobalSFXvalue = 0.0f;

		for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
			experimental::AudioEngine::setVolume(AudioManagerManager::SFX_IDs[a], 0);

		soundEffectVolumeSlider->setPercent(0);

		UserDefault::getInstance()->setBoolForKey("SFXMuted", true);
	}

}
