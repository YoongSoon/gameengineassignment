#pragma once

#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class OptionScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	void update(float dt);

	void MenuCloseCallback(cocos2d::Ref* pSender);

	//Listener function
	void ChangeScene(cocos2d::Ref* sender);
	void ReturnToMainMenu(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);
	void SetBackgroundAudioLevel(Ref* sender, Widget::TouchEventType type);
	void SetSoundEffectAudioLevel(Ref* sender, Widget::TouchEventType type);

	// implement the "static create()" method manually
	CREATE_FUNC(OptionScene);

private:
	Size visibleSize;
	Vec2 origin;

	Button * backButton;
	Slider * backgroundVolumeSlider;
	Slider* soundEffectVolumeSlider;
	CheckBox * muteSFXCheckBox;
	CheckBox * muteBGMCheckBox;
	void OnBGMMuteCallback(Ref* sender);
	void OnSFXMuteCallback(Ref* sender);

};
