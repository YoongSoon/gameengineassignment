#include "PathFinding.h"
#include <algorithm>
#include <iostream>
#include "GameObject.h"
#include "EnemyBase.h"
#include "Missile.h"

int PathFinding::CaculateManhattanDistance(TilePos pointA, TilePos pointB)
{
	return abs(pointA.x - pointB.x) + abs(pointA.y - pointB.y);
}

void PathFinding::AStarPathFinding(TilePos start, TilePos end, GameObject * object, bool isEnemy )
{
	if (isEnemy == true)
	{
		EnemyBase* m_enemy = static_cast<EnemyBase*>(object);
		//Release any previous path
		ReleasePath(m_enemy->m_path);
	}
	else
	{
		Missile* m_missile = static_cast<Missile*>(object);
		ReleasePath(m_missile->m_pathTilePosVect);
	}

	SetMapHandler(object->GetMapHandler());

	//End point cannot be wall
	if (m_handlerMap->m_Map[end.x][end.y].tileType != MapTile::TILE_FLOOR)
	{
		return;
	}


	Node * current = nullptr;
	NodeSet openSet, closedSet;

	openSet.insert(new Node(start));

	while (!openSet.empty())
	{
		//find the node with the least F on the open list,
		current = *openSet.begin();
		for (auto node : openSet)
		{
			if (node->GetScore() <= current->GetScore())
				current = node;
		}

		//Path found
		if (current->point.x == end.x && current->point.y == end.y)
		{
			break;
		}

		// add current to closed_list
		closedSet.insert(current);
		// remove current from open_list
		openSet.erase(std::find(openSet.begin(), openSet.end(), current));

		//Up
		directions.push_back(TilePos(0, 1));
		//down
		directions.push_back(TilePos(0, -1));
		//left
		directions.push_back(TilePos(-1, 0));
		//right
		directions.push_back(TilePos(1, 0));

		for (int i = 0; i < directions.size(); ++i) {
			TilePos newCoordinates(current->point.x + directions[i].x, current->point.y + directions[i].y);

			//if latest coordinate is a wall or exist on the list , continue the loop
			if (CheckValidGrid(newCoordinates) == false ||
				FindNodeOnList(closedSet, newCoordinates)) {
				continue;
			}

		

			unsigned totalCost = current->G + ((i < 4) ? 10 : 14);

			Node *successor = FindNodeOnList(openSet, newCoordinates);
			if (successor == nullptr) {
				successor = new Node(newCoordinates, current);
				successor->G = totalCost;
				successor->H = CaculateManhattanDistance(successor->point, end);
				openSet.insert(successor);
			}
			else if (totalCost < successor->G) {
				successor->parent = current;
				successor->G = totalCost;
			}
		}

	}

	// constuct A start path
	ConstructPath(current, object , isEnemy);

	//clear up open and close nodes
	ReleaseNodes(openSet);
	ReleaseNodes(closedSet);

}

PathFinding::Node * PathFinding::FindNodeOnList(NodeSet & nodes_, TilePos coordinates_)
{
	for (auto node : nodes_) {
		if (node->point.x == coordinates_.x &&
			node->point.y == coordinates_.y) {
			return node;
		}
	}
	return nullptr;
}

void PathFinding::ConstructPath(Node * newCurr, GameObject * object, bool isEnemy )
{
	if (isEnemy == true)
	{
		EnemyBase* m_enemy = static_cast<EnemyBase*>(object);
		while (newCurr != nullptr) {
			//From start point to end point ( push back gives the opposite - end point to start point)
			m_enemy->m_path.insert(m_enemy->m_path.begin(), newCurr->point);
			//m_path.push_back(newCurr->point);
			newCurr = newCurr->parent;
		}
	}
	else
	{
		Missile * m_missile = static_cast<Missile*>(object);

		while (newCurr != nullptr) {
			//From start point to end point ( push back gives the opposite - end point to start point)
			m_missile->m_pathTilePosVect.insert(m_missile->m_pathTilePosVect.begin(), newCurr->point);
			//m_path.push_back(newCurr->point);
			newCurr = newCurr->parent;
		}
	}


}


bool PathFinding::CheckValidGrid(TilePos newCurr)
{
	if ((newCurr.x < 0 || newCurr.x >=  m_handlerMap->MAP_WIDTH) || (newCurr.y < 0 || newCurr.y >= m_handlerMap->MAP_HEIGHT)
		|| m_handlerMap->m_Map[newCurr.x][newCurr.y].tileType != MapTile::TILE_FLOOR )
		return false;
	else
		return true;

}

bool PathFinding::CheckOutOfBound(TilePos newPtr)
{
	if ((newPtr.x < 0 || newPtr.x >= m_handlerMap->MAP_WIDTH) || (newPtr.y < 0 || newPtr.y >= m_handlerMap->MAP_HEIGHT))
		return true;
	else
		return false;
}


void PathFinding::ReleaseNodes(NodeSet & nodes_)
{
	for (auto it = nodes_.begin(); it != nodes_.end();) {
		delete *it;
		it = nodes_.erase(it);
	}
}

void PathFinding::ReleasePath(std::vector<TilePos>& m_path)
{
	for (auto it = m_path.begin(); it != m_path.end();) {
		it = m_path.erase(it);
	}

	m_path.clear();
}

PathFinding::Node::Node(TilePos coord_, Node * parent_)
{
	parent = parent_;
	point.x = coord_.x;
	point.y = coord_.y;
	G = H = 0;
}


int PathFinding::Node::GetScore()
{
	return G + H;
}

