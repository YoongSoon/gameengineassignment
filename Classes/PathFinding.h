#pragma once


#include <set>
#include <vector>
#include "MapHandler.h"
#include "EnemyBase.h"

class PathFinding
{
public:
	struct  Node
	{
		TilePos point;
		Node * parent;
		//incremental cost of moving from the start node to this node.
		int G;
		// estimate of the distance between each node and the goal
		int H;

		Node(TilePos coord_, Node *parent_ = nullptr);

		int GetScore();
	};
	using NodeSet = std::set<Node*>;

	// Heuristic function - Manhattan Distance (for movement in four directions)
	int CaculateManhattanDistance(TilePos pointA, TilePos pointB);
	//PathFinding for A Star
	void AStarPathFinding(TilePos start, TilePos end, GameObject * object, bool isEnemy = true );

	Node * FindNodeOnList(NodeSet& nodes_, TilePos coordinates_);

	//Actual path coordinates from a given point to an end point
	void ConstructPath(Node * newCurr, GameObject * object , bool isEnemy = true);


	//Check for out of bounds and whether is a wall
	bool CheckValidGrid(TilePos newPtr);
	//Check ONLY for out of bounds
	bool CheckOutOfBound(TilePos newPtr);


	void ReleaseNodes(NodeSet & nodes_);
	void ReleasePath(std::vector<TilePos> &m_path);

	void SetMapHandler(MapHandler * m_map) { m_handlerMap = m_map; }


private:

	//store directions
	std::vector<TilePos> directions;

	MapHandler *  m_handlerMap;


};

