#include "Player.h"
#include "GameObject.h"
#include "MapHandler.h"
#include "Definition.h"
#include "SpriteSheetLoader.h"
#include "EnemyStrategy.h"
#include "PowerUpManager.h"

Sprite* Player::Init(MapHandler* mapHandler, const Vec2 &pos, PLAYER_INDEX pIndex) {

	m_Map = mapHandler;
	m_CurrentPIndex = pIndex;

	m_MaxVelocity = 150; // Player default move speed

	// Write to db every time we load a player
	PControls::PopulateHotkey(pIndex);

	//Add sprite sheet into cache
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneMoveBackSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneMoveFrontSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneMoveRightSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneMoveLeftSheet.plist");

	//playertwo spritesheet
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerTwoMoveBackSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerTwoMoveFrontSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerTwoMoveRightSheet.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerTwoMoveLeftSheet.plist");



	//Create the sprite accordingly to the correct player
	switch (m_CurrentPIndex) {
	case PLAYER_0:
		m_Sprite = Sprite::createWithSpriteFrameName("playerOneMoveBack (1).png");
		break;
	case PLAYER_1:
		m_Sprite = Sprite::createWithSpriteFrameName("playerTwoMoveBack (1).png");
		break;

	}

	m_Sprite->setAnchorPoint(Vec2(0.5f, 0.5f));
	//	m_Sprite->setContentSize(Size(12, 12));
	m_Sprite->setPosition(pos);
	m_Sprite->setZOrder(ZORDER_PLAYER);
	m_Sprite->setName("Player" + std::to_string(pIndex));
	m_Map->AddToRenderMap(m_Sprite); // Render player on the map

	// Init Physics Body
	m_PhysicsBody = PhysicsBody::createCircle(6, PhysicsMaterial(1, 0, 1));
	m_PhysicsBody->setRotationEnable(false);
	m_PhysicsBody->setDynamic(true);
	m_PhysicsBody->setContactTestBitmask(true);
	m_PhysicsBody->setVelocityLimit(m_MaxVelocity / 2);
	m_Sprite->addComponent(m_PhysicsBody);

	// Collision response
	auto contactListener = EventListenerPhysicsContact::create();
	contactListener->onContactPreSolve = CC_CALLBACK_1(Player::onContactPreSolve, this);
	m_Sprite->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, m_Sprite);

	m_WindowSize = Director::getInstance()->getWinSize();

	return m_Sprite;
}

void Player::Update(float dt) {
	m_ElapsedTime += dt;

	if (m_Lives <= 0)
		return;

	// log("Tile Explosion: %i", m_Map->GetTileFromPos(m_Sprite->getPosition())->tileType == MapTile::TILE_DESTRUCTIBLE);

	ProcessMovement(dt);
	ProcessBomb(dt);

	if (m_IsInvulnerable) {
		if (m_ElapsedTime >= m_NextVulnerableTime) {
			m_IsInvulnerable = false;
			m_Sprite->setOpacity(255);
		}
		else {
			m_Sprite->setOpacity(std::abs(sinf(m_ElapsedTime * 5)) * 255); // Fade in and out
		}
	}

	//Turn the joystick UI invisible after a certain duration when the user stop touching it
	if (b_IsUnTouched == true) {
		m_ElaspedUnTouchedTime += dt;

		if (m_ElaspedUnTouchedTime >= m_ThresholdTimeUnTouched) {
			//run fading out actions on both the thumb sprite and background sprite
			auto FadeOutAction1 = FadeOut::create(2);
			FadeOutAction1->setTag(39);

			//apparantely have to create this variable as FadeOutAction1 can't be reused in m_JoystickBase->getBackgroundSprite()->runAction()
			// after using it in m_JoystickBase->getThumbSprite()->runAction()  ( evidence in the thumbsprite not fading out when FadeOutAction1 is reused)
			auto FadeOutAction2 = FadeOut::create(2);
			FadeOutAction2->setTag(40);

			m_JoystickBase->getThumbSprite()->runAction(FadeOutAction1);
			m_JoystickBase->getBackgroundSprite()->runAction(FadeOutAction2);
			//reset variables
			b_IsUnTouched = false;
			m_ElaspedUnTouchedTime = 0.f;
		}
	}
	//Take care of case when the user touches the joystick UI while is fading out
	// therefore the fading out action should be stopped , and run the fading in action instead
	else {
		//if fading out and fading in action is happening , stop fading out action
		if (m_JoystickBase->getThumbSprite()->getNumberOfRunningActions() == 2) {
			//reset the variable
			m_ElaspedUnTouchedTime = 0.0f;

			//stop the fading out aciton
			m_JoystickBase->getThumbSprite()->stopActionByTag(39);
			m_JoystickBase->getBackgroundSprite()->stopActionByTag(40);

		}

	}

}

SneakyJoystickSkinnedBase * Player::InitJoyStick() {
	//Joystick UI
	Rect joystickBaseDimensions;
	joystickBaseDimensions = Rect(0, 0, 80.f, 80.0f);

	joystickBasePosition = Vec2(m_WindowSize.width * 0.2f, m_WindowSize.height*0.2f);

	m_JoystickBase = new SneakyJoystickSkinnedBase();
	m_JoystickBase->init();
	m_JoystickBase->setPosition(joystickBasePosition);
	m_JoystickBase->setBackgroundSprite(Sprite::create("joypad.png"));
	m_JoystickBase->getBackgroundSprite()->setScale(0.6f, 0.6f);
	m_JoystickBase->setThumbSprite(Sprite::create("joystick.png"));
	m_JoystickBase->getThumbSprite()->setScale(0.7f, 0.7f);

	//set the sprites to transparent by default
	m_JoystickBase->getThumbSprite()->setOpacity(0);
	m_JoystickBase->getBackgroundSprite()->setOpacity(0);

	SneakyJoystick *aJoystick = new SneakyJoystick();
	aJoystick->initWithRect(joystickBaseDimensions);
	aJoystick->autorelease();

	m_JoystickBase->setJoystick(aJoystick);
	m_JoystickBase->setPosition(joystickBasePosition);

	m_LeftJoystick = m_JoystickBase->getJoystick();
	m_LeftJoystick->retain();


	return m_JoystickBase;
}

SneakyButtonSkinnedBase * Player::InitJoyButton() {
	Rect attackButtonDimensions = Rect(0, 0, 64.0f, 64.0f);
	Point attackButtonPosition;
	attackButtonPosition = Point(m_WindowSize.width * 0.9f, m_WindowSize.height * 0.12f);

	SneakyButtonSkinnedBase *attackButtonBase = SneakyButtonSkinnedBase::create();
	attackButtonBase->setPosition(attackButtonPosition);

	attackButtonBase->setDefaultSprite(Sprite::create("joybutton.png"));
	attackButtonBase->setActivatedSprite(Sprite::create("joybutton.png"));
	attackButtonBase->setDisabledSprite(Sprite::create("joybutton.png"));
	attackButtonBase->setPressSprite(Sprite::create("joybutton.png"));

	attackButtonBase->getDefaultSprite()->setScale(0.6f, 0.6f);
	attackButtonBase->getActivatedSprite()->setScale(0.6f, 0.6f);
	attackButtonBase->getDisabledSprite()->setScale(0.6f, 0.6f);
	attackButtonBase->getPressSprite()->setScale(0.6f, 0.6f);

	SneakyButton *aAttackButton = SneakyButton::create();
	aAttackButton->initWithRect(attackButtonDimensions);

	 aAttackButton->setIsHoldable(true);
	// aAttackButton->setIsToggleable(true);

	attackButtonBase->setButton(aAttackButton);
	attackButtonBase->setPosition(attackButtonPosition);

	m_PlaceBombButton = attackButtonBase->getButton();
	m_PlaceBombButton->retain();

	return attackButtonBase;
}

LoadingBar * Player::InitReloadBar() {
	m_ReloadBombBar = ui::LoadingBar::create("reloadBombButton.png");
	m_ReloadBombBar->setPosition(Vec2(m_WindowSize.width * 0.9f, m_WindowSize.height * 0.12f));
	m_ReloadBombBar->setScale(0.6f, 0.6f);
	m_ReloadBombBar->setPercent(0);
	return m_ReloadBombBar;
}


void Player::RunScreenShake()
{
	//experiment more with these four values to get a rough or smooth effect!
	float interval = 0.f;
	float duration = 1.0f;
	float speed = 1.5f;
	float magnitude = 1.5f;

	static float elapsed = 0.f;

	Scene * currentScene = CCDirector::getInstance()->getRunningScene();

	currentScene->schedule([=](float dt) {
		float randomStart = random(-1000.0f, 1000.0f);
		elapsed += dt;

		float percentComplete = elapsed / duration;

		// We want to reduce the shake from full power to 0 starting half way through
		float damper = 1.0f - clampf(2.0f * percentComplete - 1.0f, 0.0f, 1.0f);

		// Calculate the noise parameter starting randomly and going as fast as speed allows
		float alpha = randomStart + speed * percentComplete;

		// map noise to [-1, 1]
		float x = CaculateNoise(alpha, 0.0f) * 2.0f - 1.0f;
		float y = CaculateNoise(0.0f, alpha) * 2.0f - 1.0f;

		x *= magnitude * damper;
		y *= magnitude * damper;
		currentScene->setPosition(x, y);

		if (elapsed >= duration)
		{
			elapsed = 0;
			currentScene->unschedule("Shake");
			currentScene->setPosition(Vec2::ZERO);
		}

	}, interval, CC_REPEAT_FOREVER, 0.f, "Shake");

}

float Player::CaculateNoise(int x, int y)
{
	int n = x + y * 57;
	n = (n << 13) ^ n;
	return (1.0 - ((n * ((n * n * 15731) + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

void Player::ProcessMovement(float dt) {
	PhysicsBody* body = static_cast<PhysicsBody*>(m_Sprite->getComponent("PhysicsBody"));

	body->setVelocityLimit((m_MaxVelocity / 2) * m_VelocityCap);
	
	if (m_LeftJoystick->getVelocity().y > 0 && (m_LeftJoystick->getVelocity().y > m_LeftJoystick->getVelocity().x)) {
		m_CurrentDir = FACE_UP;
		m_CurrentVelocity = Vec2(0, 1) * m_MaxVelocity;
	}
	else if (m_LeftJoystick->getVelocity().y < 0 && (m_LeftJoystick->getVelocity().y < m_LeftJoystick->getVelocity().x)) {
		m_CurrentDir = FACE_DOWN;
		m_CurrentVelocity = Vec2(0, -1) * m_MaxVelocity;
	}
	if (m_LeftJoystick->getVelocity().x < 0 && (m_LeftJoystick->getVelocity().x < m_LeftJoystick->getVelocity().y)) {
		m_CurrentDir = FACE_LEFT;
		m_CurrentVelocity = Vec2(-1, 0) * m_MaxVelocity;
	}
	// 0.2 is the offset to prevent trigger of movement of character to the right, when the thumb sprite is dragged down but slightly to the right 
	else if (m_LeftJoystick->getVelocity().x > 0.2 && (m_LeftJoystick->getVelocity().x > m_LeftJoystick->getVelocity().y)) {

		m_CurrentDir = FACE_RIGHT;
		m_CurrentVelocity = Vec2(1, 0) * m_MaxVelocity;
	}

	if (m_LeftJoystick->getStickPosition().lengthSquared() == 0) {
		m_CurrentVelocity *= 0;
		body->setVelocity(Vec2::ZERO);
	}


	//Stop all animation action when not moving
	if (m_CurrentVelocity.lengthSquared() == 0) {
		m_Sprite->stopAllActions();
	}

	body->applyImpulse(m_CurrentVelocity * 10);

}

void Player::ProcessBomb(float dt) {

	// Tile that the player is standing on
	auto playerTile = m_Map->GetTileFromPos(m_Sprite->getPosition());

	// Recharge the bomb until it reaches m_BombCount
	if (m_CurrentBombCount < m_BombCount) {
		m_CurrentBombCooldown -= dt;

		if (m_CurrentBombCooldown <= 0) {
			m_CurrentBombCount++;
			m_CurrentBombCooldown = m_BombCooldown;
		}
	}


	// Plant bomb if tile is not a wall
	if (m_PlaceBombButton->getValue()  && playerTile->tileType == MapTile::TILE_FLOOR && playerTile->IsEmpty()
		&& m_CurrentBombCount > 0 && m_CanPressBomb == true) {

		Bomb * m_bomb = new Bomb(m_Map, m_Sprite->getPosition(), true);
		m_bomb->SetBombMagnitude(m_BombRange);
		m_Map->AddToMap(m_bomb);
		EnemyStrategy::b_isANewBombPlaced = true;

		m_CurrentBombCount--;
		m_CanPressBomb = false;
	}

	//Press cool down before player can place a bomb again
	if (m_CanPressBomb == false) {
		//Update the reloading bar UI
		m_CurrentPressCooldown += dt;
		m_ReloadBombBar->setPercent(m_CurrentPressCooldown / m_BombCooldown * 100 );

		if ( m_ReloadBombBar->getPercent() >= 100) {
			m_CurrentPressCooldown = 0.0f;
			//Reset the variables
			m_ReloadBombBar->setPercent(0);
			m_CanPressBomb = true;
		}
	}



}

bool Player::onContactPreSolve(PhysicsContact & contact) {

	if (m_Lives <= 0)
		return false;

	auto collideA = contact.getShapeA()->getBody()->getNode();
	auto collideB = contact.getShapeB()->getBody()->getNode();

	// Ignore collision if both is player
	if (collideA->getName().find("Player") != std::string::npos && collideB->getName().find("Player") != std::string::npos) {
		return false;
	}

	// Swap to ensure that Collider A is always the Player
	if (collideB->getName().find("Player") != std::string::npos) {
		std::swap(collideA, collideB);
	}

	if (collideA->getName().find("Player") == std::string::npos) {
		return true;
	}

	if (collideA != m_Sprite) {
		return false;
	}

	if (collideB->getName() == "PowerUp") {
		m_consumePowerUpValue = static_cast<PowerUp*>(collideB)->GetPowerUpType();
		static_cast<PowerUp*>(collideB)->Consume(this);
		b_ANewPowerUpConsumed = true;
		return false; // Ignore collision
	}
	else if (collideB->getName() == "explosion") {
		DamagePlayer();
		return false;  //Ignore collision between explosion and player
	}
	else if (collideB->getName() == "Enemy") {
		DamagePlayer();
	}

	return true;
}



bool Player::onTouchBegan(Touch * touch, Event * event) {

	//Half of the background sprite
	Size halfBackGroundSize = (m_JoystickBase->getBackgroundSprite()->getBoundingBox().size) / 2.0f;

	//default joystick position(basically the touch position) before the clamping check below
	joystickBasePosition = Vec2(touch->getLocation().x, touch->getLocation().y);

	//Set the position of joystick to whever the user touch is provided it satisfied both of the conditions below
	// 1) left half of the screen
	// 2) not out of the screen
	if (touch->getLocation().x - halfBackGroundSize.width * 1.5f < 0) {
		joystickBasePosition.x = halfBackGroundSize.width * 1.5f;
	}
	else if (touch->getLocation().x + halfBackGroundSize.width * 2 > Director::getInstance()->getWinSize().width / 2) {
		joystickBasePosition.x = Director::getInstance()->getWinSize().width / 2 - halfBackGroundSize.width;
	}

	if (touch->getLocation().y - halfBackGroundSize.height < 0) {
		joystickBasePosition.y = halfBackGroundSize.height / 1.0f;
	}
	else if (touch->getLocation().y + halfBackGroundSize.height > Director::getInstance()->getWinSize().height) {
		joystickBasePosition.y = Director::getInstance()->getWinSize().height - halfBackGroundSize.height;
	}

	m_JoystickBase->setPosition(joystickBasePosition);

	//Run fading in actions for both the thumb sprite and background sprite

	m_JoystickBase->getThumbSprite()->runAction(FadeIn::create(2));
	m_JoystickBase->getBackgroundSprite()->runAction(FadeIn::create(2));


	return true;
}

void Player::onTouchMoved(Touch *touch, Event *event) {
	if (m_Lives <= 0) {
		return;
	}

	auto target = event->getCurrentTarget();
	auto targetSize = target->getContentSize();
	auto targetRectangle = Rect(0, 0, targetSize.width, targetSize.height);

	//check whether thumb sprite bounding box intersects with the rectangle size of the touch by the user
	if (m_JoystickBase->getThumbSprite()->getBoundingBox().intersectsRect(targetRectangle)) {

		//reset the variable 
		b_IsUnTouched = false;

		Vector<SpriteFrame*> frames;
		m_Sprite->stopAllActions();

		if (m_CurrentDir == FACE_UP) {
			frames = SpriteSheetLoader::GetSpriteFrames("playerOneMoveFront", 3);
		}
		if (m_CurrentDir == FACE_DOWN) {
			frames = SpriteSheetLoader::GetSpriteFrames("playerOneMoveBack", 3);
		}
		if (m_CurrentDir == FACE_LEFT) {
			frames = SpriteSheetLoader::GetSpriteFrames("playerOneMoveLeft", 3);
		}
		if (m_CurrentDir == FACE_RIGHT) {
			frames = SpriteSheetLoader::GetSpriteFrames("playerOneMoveRight", 3);
		}
		if (frames.empty() == false) {
			auto animation = Animation::createWithSpriteFrames(frames, 1.0f / 3);
			auto  animate = Animate::create(animation);
			//Get the sprite frames from the cache
			m_Sprite->runAction(RepeatForever::create(animate));
		}

	}
}

void Player::onTouchEnded(Touch * touch, Event * event) {
	if (b_IsUnTouched == false)
		b_IsUnTouched = true;
}



void Player::IncreaseMaxVelocity(float amount) {
	m_MaxVelocity += amount;
	m_PhysicsBody->setVelocityLimit(m_MaxVelocity / 2);
}

void Player::DamagePlayer() {

	if (m_IsInvulnerable || m_Lives <= 0)
		return;

	m_IsInvulnerable = true;

	RunScreenShake();

	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/playerouch.mp3", false, AudioManagerManager::GlobalSFXvalue);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::SFX_IDs.push_back(AudioManagerManager::tempid);

	m_Lives--;

	// Player died
	if (m_Lives <= 0) {
		m_Sprite->stopAllActions();
		m_Sprite->runAction(FadeOut::create(1));
		m_PhysicsBody->setVelocity(Vec2::ZERO);
		log("Player %i died", (int)m_CurrentPIndex);
	}

	m_NextVulnerableTime = m_ElapsedTime + 3; // Invulerable for X seconds
}


void PControls::PopulateHotkey(Player::PLAYER_INDEX pIndex) {
	log("Writing Hotkey..");

	std::string indexStr = std::to_string(pIndex);

	switch (pIndex) {
	case Player::PLAYER_0:
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveUp").c_str(), (int)EventKeyboard::KeyCode::KEY_W);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveDown").c_str(), (int)EventKeyboard::KeyCode::KEY_S);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveLeft").c_str(), (int)EventKeyboard::KeyCode::KEY_A);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveRight").c_str(), (int)EventKeyboard::KeyCode::KEY_D);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_UseBomb").c_str(), (int)EventKeyboard::KeyCode::KEY_SPACE);
		break;
	case Player::PLAYER_1:
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveUp").c_str(), (int)EventKeyboard::KeyCode::KEY_UP_ARROW);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveDown").c_str(), (int)EventKeyboard::KeyCode::KEY_DOWN_ARROW);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveLeft").c_str(), (int)EventKeyboard::KeyCode::KEY_LEFT_ARROW);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_MoveRight").c_str(), (int)EventKeyboard::KeyCode::KEY_RIGHT_ARROW);
		UserDefault::getInstance()->setIntegerForKey((indexStr + "_UseBomb").c_str(), (int)EventKeyboard::KeyCode::KEY_KP_ENTER);
		break;
	}

	log("Hotkey written to: %s", UserDefault::getInstance()->getXMLFilePath().c_str());
}

EventKeyboard::KeyCode PControls::GetKeyFromString(const std::string& keyName) {
	return (EventKeyboard::KeyCode)UserDefault::getInstance()->getIntegerForKey(keyName.c_str());
}
