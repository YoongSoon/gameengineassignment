#pragma once

#include "SneakyButton.h"
#include "SneakyButtonSkinnedBase.h"
#include "SneakyJoystickSkinnedBase.h"
#include "ui/CocosGUI.h"
using namespace cocos2d::ui;

#include "cocos2d.h"

using namespace cocos2d;

class MapHandler;


class Player {

public:
	Player() {};

	// Which controls should this player use?
	enum PLAYER_INDEX {
		PLAYER_0, // Player on the left
		PLAYER_1 // Player on the right
	};

	enum FACE_DIR {
		FACE_LEFT,
		FACE_RIGHT,
		FACE_UP,
		FACE_DOWN
	};

	Sprite* Init(MapHandler* mapHandler, const Vec2& pos, PLAYER_INDEX pIndex = PLAYER_0);

	void Update(float dt);

	//Touch inputs
	bool  onTouchBegan(Touch *touch, Event *event);
	void onTouchMoved(Touch *touch, Event *event);
	void onTouchEnded(Touch *touch, Event *event);

	Sprite * GetPlayerSprite() { return m_Sprite; }

	float GetMaxVelocity() { return m_MaxVelocity; }

	void IncreaseMaxVelocity(float amount);

	void DamagePlayer(); // Decrements m_Lives by 1 and makes player invulnerable for a short duration

	// Stats
	int m_Lives = 2;
	int m_BombCount = 1;
	int m_BombRange = 2;

	bool m_IsInvulnerable = false;

	//Init JoyStick UI
	SneakyJoystickSkinnedBase * InitJoyStick();
	//Init Bomb Placing Button UI
	SneakyButtonSkinnedBase * InitJoyButton();

	//Init bomb placing loading bar UI
	 LoadingBar * InitReloadBar();

	 //basically PowerUp::POWERUP_TYPE; (can't use enum because of undeclared identifier error despite the header is being included proprely)
	 //use an identifier to check the latest powerup consumed by the player
	 int m_consumePowerUpValue;
	 bool b_ANewPowerUpConsumed = false;

	 MapHandler * m_Map;

	 void SetVelocityCap(float value) { m_VelocityCap = value; }

	 void RunScreenShake();
	 float CaculateNoise(int x, int y);

private:
	Sprite * m_Sprite;

	void ProcessMovement(float dt);

	void ProcessBomb(float dt);

	FACE_DIR m_CurrentDir = FACE_DOWN;
	Vec2 m_CurrentVelocity;
	float m_MaxVelocity;
	float m_VelocityCap = 1;

	PLAYER_INDEX m_CurrentPIndex;

	// Stats Helper
	const float m_BombCooldown = 2;
	float m_CurrentBombCooldown = 0;
	int m_CurrentBombCount = 0;

	bool m_CanPressBomb = true;

	PhysicsBody* m_PhysicsBody;

	bool onContactPreSolve(PhysicsContact& contact);

	float m_NextVulnerableTime = 0;

	float m_ElapsedTime = 0;

	SneakyJoystick * m_LeftJoystick;
	SneakyJoystickSkinnedBase *m_JoystickBase;
	Point joystickBasePosition;
	SneakyButton * m_PlaceBombButton;

	LoadingBar * m_ReloadBombBar;
	float m_TotalFill;

	const float m_PressCooldown = 2;
	float m_CurrentPressCooldown = 0;

	Size m_WindowSize;

	//check whether the user has stopped touching the joystick control
	bool b_IsUnTouched = false;
	float m_ElaspedUnTouchedTime = 0;
	float m_ThresholdTimeUnTouched = 1.0f;

};

class PControls {

public:
	static EventKeyboard::KeyCode GetMoveUp(Player::PLAYER_INDEX pIndex) { return GetKeyFromString(std::to_string(pIndex) + "_MoveUp"); };
	static EventKeyboard::KeyCode GetMoveDown(Player::PLAYER_INDEX pIndex) { return GetKeyFromString(std::to_string(pIndex) + "_MoveDown"); };
	static EventKeyboard::KeyCode GetMoveLeft(Player::PLAYER_INDEX pIndex) { return GetKeyFromString(std::to_string(pIndex) + "_MoveLeft"); };
	static EventKeyboard::KeyCode GetMoveRight(Player::PLAYER_INDEX pIndex) { return GetKeyFromString(std::to_string(pIndex) + "_MoveRight"); };
	static EventKeyboard::KeyCode GetUseBomb(Player::PLAYER_INDEX pIndex) { return GetKeyFromString(std::to_string(pIndex) + "_UseBomb"); };

	static void PopulateHotkey(Player::PLAYER_INDEX pIndex);

private:
	PControls() {};

	static EventKeyboard::KeyCode GetKeyFromString(const std::string& keyName);

};