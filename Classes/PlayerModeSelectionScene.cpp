#include "PlayerModeSelectionScene.h"
#include "LevelSelectionScene.h"
#include "SpriteSheetLoader.h"
#include "MainMenuScene.h"
#include "Definition.h"

bool PlayerModeSelectionScene::b_isSinglePlayer = true;

cocos2d::Scene * PlayerModeSelectionScene::createScene()
{
	// scene is an autorelease object
	auto scene = Scene::create();

	// layer is an autorelease object
	auto layer = PlayerModeSelectionScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}

bool PlayerModeSelectionScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}


	//Add white color background instead of the default black color background
	auto bg = cocos2d::LayerColor::create(Color4B(255, 255, 255, 255));
	this->addChild(bg);

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto titleSprite = Sprite::create("playerModeLogo.png");
	titleSprite->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height + origin.y - titleSprite->getContentSize().width / 6.0f);
	this->addChild(titleSprite);

	//back button
	 backButton = Button::create("backLogo.png");
	backButton->setPosition(Vec2(origin.x + backButton->getContentSize().width / 1.2f,
		visibleSize.height - backButton->getContentSize().height / 2.0f));
	backButton->addTouchEventListener(CC_CALLBACK_2(PlayerModeSelectionScene::ReturnToMainMenu, this));
	this->addChild(backButton);

	//singe player select button
	 singlePlayButton = Button::create("singlePlayerLogo.png" );
	singlePlayButton->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	singlePlayButton->addTouchEventListener(CC_CALLBACK_2(PlayerModeSelectionScene::SetSinglePlayMode, this));
	this->addChild(singlePlayButton);


	//singe player select button
	 multiplePlayButton = Button::create("multiPlayerLogo.png");
	multiplePlayButton->setPosition(Vec2(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y - visibleSize.height / 6.4f));
	multiplePlayButton->addTouchEventListener(CC_CALLBACK_2(PlayerModeSelectionScene::SetMultiPlayerMode, this));
	this->addChild(multiplePlayButton);


	//select button
	auto selectButton = Button::create("nextlogo.png");
	selectButton->setPosition(Vec2(origin.x + visibleSize.width / 2.0f,
		origin.y + selectButton->getContentSize().height));
	selectButton->addTouchEventListener(CC_CALLBACK_2(PlayerModeSelectionScene::ChangeScene, this));
	this->addChild(selectButton);


	//Add sprite sheet into cache
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerOneAttackSheet.plist");
	//Get the sprite frames from the cache
	auto frames = SpriteSheetLoader::GetSpriteFrames("playerOneAttack", 8);
	//Get the first sprite from the sprite frame
	auto spritePlayerOne = Sprite::createWithSpriteFrame(frames.front());;
	//set scale of the sprite
	spritePlayerOne->setScale(spritePlayerOne->getScaleX() *3.0f, spritePlayerOne->getScaleY() * 3.0f);
	//set position of the sprite
	spritePlayerOne->setPosition( origin.x + spritePlayerOne->getBoundingBox().size.width , origin.y + visibleSize.height / 2.0f);
	//Create the animation
	auto  animation = Animation::createWithSpriteFrames(frames, 2.0f / 8);
	//run the animation
	//spritePlayerOne->runAction(RepeatForever::create(Animate::create(animation)));

	auto delay = DelayTime::create(0.1f);
	auto sequence = Sequence::create(Animate::create(animation), delay,nullptr);
	spritePlayerOne->runAction(RepeatForever::create(sequence));

	this->addChild(spritePlayerOne);


	//Add sprite sheet into cache
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("playerTwoAttackSheet.plist");
	//Get the sprite frames from the cache
	auto frames2 = SpriteSheetLoader::GetSpriteFrames("playerTwoAttack", 8);
	//Get the first sprite from the sprite frame
	auto spritePlayerTwo = Sprite::createWithSpriteFrame(frames2.front());;
	//set scale of the sprite
	spritePlayerTwo->setScale(spritePlayerTwo->getScaleX() *3.0f, spritePlayerTwo->getScaleY() * 3.0f);
	//set position of the sprite
	spritePlayerTwo->setPosition(visibleSize.width - spritePlayerTwo->getBoundingBox().size.width / 2.0f, 
		origin.y + visibleSize.height / 2.0f);
	//Create the animation
	auto  animation2 = Animation::createWithSpriteFrames(frames2, 2.0f / 8);

	auto sequence2 = Sequence::create(Animate::create(animation2), delay, nullptr);
	spritePlayerTwo->runAction(RepeatForever::create(sequence2));
	this->addChild(spritePlayerTwo);



	return true;
}

void PlayerModeSelectionScene::update(float dt)
{
}

void PlayerModeSelectionScene::SetSinglePlayMode(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {
		//Button * button = (Button*)sender;
	//	button->setEnabled(false);
		singlePlayButton->loadTextureNormal("singePlayerSelectedLogo.png");
		multiplePlayButton->loadTextureNormal("multiPlayerLogo.png");
		b_isSinglePlayer = true;
		m_selectMode = SELECT_SINGLE;
	}

}

void PlayerModeSelectionScene::SetMultiPlayerMode(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {
		singlePlayButton->loadTextureNormal("singlePlayerLogo.png");
		multiplePlayButton->loadTextureNormal("multiPlayerSelectedLogo.png");
		b_isSinglePlayer = false;
		m_selectMode = SELECT_MULTIPLAYER;
	}

}

void PlayerModeSelectionScene::ChangeScene(cocos2d::Ref * sender , cocos2d::ui::Widget::TouchEventType eventType)
{
	//user have to select either the singleplayer button or multiplayer button to proceed to level selection scene
	if (m_selectMode != SELECT_NONE)
	{
		auto scene = LevelSelectionScene::createScene();
		Director::getInstance()->replaceScene(TransitionSlideInR::create(TRANSIT_TIME, scene));
	}
}

void PlayerModeSelectionScene::ReturnToMainMenu(cocos2d::Ref * sender, cocos2d::ui::Widget::TouchEventType eventType)
{
	
	//Scale down the button when is pressed
	if (cocos2d::ui::Widget::TouchEventType::BEGAN == eventType) {

		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, false);
	}
	//Scale down the button when is not pressed
	else if (cocos2d::ui::Widget::TouchEventType::ENDED == eventType)
	{
		SpriteSheetLoader::ScaleNode(*backButton, 1.1f, true);
	}

	auto scene = MainMenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionSlideInL::create(TRANSIT_TIME, scene));
}



