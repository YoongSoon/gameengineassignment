#pragma once
#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class PlayerModeSelectionScene : cocos2d::Scene
{
public:
	enum  SELECTION_MODE
	{
		SELECT_NONE,
		SELECT_SINGLE,
		SELECT_MULTIPLAYER,

	};
	SELECTION_MODE m_selectMode = SELECT_NONE;

	static bool b_isSinglePlayer;

	static cocos2d::Scene* createScene();

	virtual bool init();
	void update(float dt);

	void SetSinglePlayMode(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);
	void SetMultiPlayerMode(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

	void ChangeScene(cocos2d::Ref* sender , cocos2d::ui::Widget::TouchEventType eventType);
	void ReturnToMainMenu(cocos2d::Ref* sender, cocos2d::ui::Widget::TouchEventType eventType);

	// implement the "static create()" method manually
	CREATE_FUNC(PlayerModeSelectionScene);

private:
	Button * backButton;
	Button* singlePlayButton;
	Button* multiplePlayButton;
};