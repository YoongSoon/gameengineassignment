#include "PowerUpManager.h"

void PowerUpManager::Init(MapHandler * map) {
	m_Map = map;
}

void PowerUpManager::Update(float dt) {

	m_SpawnCooldown -= dt;

	if (m_SpawnCooldown <= 0) {
		m_SpawnCooldown = m_PowerUpSpawnInterval * 2;

		TilePos randPos = m_Map->GetRandomEmptyTilePos();

		if (randPos.x != -1 && randPos.y != -1)
			SpawnPowerUp(randPos);

		log("Spawned PowerUp at (%i, %i)", randPos.x, randPos.y);
	}

}

void PowerUpManager::SpawnPowerUp(TilePos tilePos) {
	m_Map->AddToMap(new PowerUp((PowerUp::POWERUP_TYPE)RandomHelper::random_int<int>(0, PowerUp::POWER__LAST - 1), m_Map, m_Map->GetPosFromTilePos(tilePos)));
}
