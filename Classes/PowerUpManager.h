#pragma once

#include "MapHandler.h"
#include "GameObject.h"


class PowerUpManager {

public:
	PowerUpManager() {}

	void Init(MapHandler* map);
	void Update(float dt);

	void SpawnPowerUp(TilePos tilePos);

private:
	const float m_PowerUpSpawnInterval = 5;
	float m_SpawnCooldown = m_PowerUpSpawnInterval;

	MapHandler* m_Map;

};


class PowerUp : public GameObject {

public:
	enum POWERUP_TYPE {
		POWER_LIVES,
		POWER_BOMBRANGE,
		POWER_MOVESPEED,
		POWER_BOMBCOUNT,
		POWER__LAST

	};

	PowerUp(POWERUP_TYPE type, MapHandler* map, Vec2 pos) : GameObject(map) {

		switch (type) {
		case PowerUp::POWER_LIVES:
			InitGO("TileMaps/Individual/berry.png");
			break;
		case PowerUp::POWER_BOMBRANGE:
			InitGO("TileMaps/Individual/carrot.png");
			break;
		case PowerUp::POWER_MOVESPEED:
			InitGO("TileMaps/Individual/flower.png");
			break;
		case PowerUp::POWER_BOMBCOUNT:
			InitGO("TileMaps/Individual/corn.png");
			break;
		};

		InitSize(pos, ZORDER_POWERUP);
		setName("PowerUp");
		m_PowerUpType = type;

		unscheduleUpdate(); // addComponent calls scheduleUpdate(), to prevent double scheduling

		// Collider
		auto physicsBody = PhysicsBody::createCircle(6, PhysicsMaterial(1, 0, 1));
		physicsBody->setRotationEnable(false);
		physicsBody->setContactTestBitmask(true);
		addComponent(physicsBody);
	}

	virtual void update(float dt) {

	}

	void Consume(Player* player) {

		switch (m_PowerUpType) {
		case PowerUp::POWER_LIVES:
			if (player->m_Lives > 0)
				player->m_Lives++;
			break;
		case PowerUp::POWER_BOMBRANGE:
			player->m_BombRange++;
			break;
		case PowerUp::POWER_MOVESPEED:
			player->IncreaseMaxVelocity(10);
			break;
		case PowerUp::POWER_BOMBCOUNT:
			player->m_BombCount++;
			break;
		}

		DestroyMe();
	}

	POWERUP_TYPE GetPowerUpType() { return m_PowerUpType; }
private:
	POWERUP_TYPE m_PowerUpType;


};
