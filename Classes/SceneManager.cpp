#include "SceneManager.h"
#include "GameSceneLevel1.h"


using namespace cocos2d;

// Singleton
SceneManager * SceneManager::_instance = NULL;


SceneManager* SceneManager::getInstance()
{
	if (!_instance) _instance = new SceneManager();
	return _instance;
}

SceneManager::SceneManager() :
	_sceneTypeToReturn(SceneType::MAINMENU),
	_currentSceneType(SceneType::MAINMENU)
{

}

SceneManager::~SceneManager()
{

}

void SceneManager::runSceneWithType(const SceneType sceneType)
{
	Scene *sceneToRun = nullptr;

	switch (sceneType) {
	case SceneType::MAINMENU:
		//sceneToRun = ::createScene();
		break;
	case SceneType::GAMEPLAY:
		sceneToRun = GameSceneLevel1::createScene();
		break;

	default:
		log("WARNING! Default value when trying to run scene with id %d", static_cast<int>(sceneType));
		return;
		break;
	}
	SceneType oldScene = _currentSceneType;
	//Set the sceneType as the current scene 
	_currentSceneType = sceneType;
	//Set tag of current scene to run
	sceneToRun->setTag(static_cast<int>(sceneType));

	if (sceneToRun == nullptr) {
		_currentSceneType = oldScene;
		return;
	}

	_sceneTypeToReturn = oldScene;

	if (CCDirector::getInstance()->getRunningScene() == nullptr) {
		CCDirector::getInstance()->runWithScene(sceneToRun);
	}
	else {
		CCDirector::getInstance()->replaceScene(sceneToRun);
	}
}

void SceneManager::returnToLastScene()
{
	this->runSceneWithType(_sceneTypeToReturn);
}