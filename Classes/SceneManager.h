#pragma once

#include "cocos2d.h" // for namespace cocos2d to work


using namespace cocos2d; // for Scene to work

enum class SceneType
{
	NONE,
	MAINMENU,
	GAMEPLAY,
};


class SceneManager
{
public:	// Public singleton
	static SceneManager* getInstance();
	~SceneManager();
	void runSceneWithType(const SceneType sceneType);
	void returnToLastScene();
private:
	SceneType _sceneTypeToReturn;
	SceneType _currentSceneType;
	static SceneManager * _instance;
	SceneManager();
};