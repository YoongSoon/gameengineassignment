#include "SplashScene.h"
#include "MainMenuScene.h"
#include "Definition.h"
#include "SpriteSheetLoader.h"
#include "AudioEngine.h"

cocos2d::Scene * SplashScene::createScene()
{
	// scene is an autorelease object
	auto scene = Scene::create();

	// layer is an autorelease object
	auto layer = SplashScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	//return the scene
	return scene;
}

bool SplashScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Scene::init())
	{
		return false;
	}

	//Add white color background instead of the default black color background
	auto bg = cocos2d::LayerColor::create(Color4B(255, 255, 255, 255));
	this->addChild(bg);


	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();


	//Run once
	this->scheduleOnce(schedule_selector(SplashScene::ChangeScene), DISPLAY_TIME_SPLASH);

	auto backgroundSprite = Sprite::create("nypLogo.png");
	backgroundSprite->setPosition(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y);
	SpriteSheetLoader::RescaleToScreenSize(backgroundSprite);

	//scale the backgroundSprite to fit any screen size
	/*backgroundSprite->setScale(visibleSize.width / backgroundSprite->getContentSize().width,
		visibleSize.height / backgroundSprite->getContentSize().height);*/
	
	//Add the sprite as a child to the scene
	this->addChild(backgroundSprite);
	
	return true;
}

void SplashScene::ChangeScene(float dt)
{
	AudioManagerManager::tempid = experimental::AudioEngine::play2d("Audio/ThemeSong.mp3", true, AudioManagerManager::GlobalBGMvalue);
	for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
		experimental::AudioEngine::stop(AudioManagerManager::BGM_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		AudioManagerManager::BGM_IDs.push_back(AudioManagerManager::tempid);

	for (int a = 0; a < AudioManagerManager::SFX_IDs.size(); a++)
	{
		if (!UserDefault::getInstance()->getBoolForKey("SFXMuted"))
			experimental::AudioEngine::setVolume(AudioManagerManager::SFX_IDs[a], (UserDefault::getInstance()->getFloatForKey("GlobalSFXvalue")));
		else
			experimental::AudioEngine::setVolume(AudioManagerManager::SFX_IDs[a], 0);
	}
	for (int a = 0; a < AudioManagerManager::BGM_IDs.size(); a++)
	{
		if (!UserDefault::getInstance()->getBoolForKey("BGMMuted"))
			experimental::AudioEngine::setVolume(AudioManagerManager::BGM_IDs[a], (UserDefault::getInstance()->getFloatForKey("GlobalBGMvalue")));
		else
			experimental::AudioEngine::setVolume(AudioManagerManager::BGM_IDs[a], 0);
	}

	auto scene = MainMenuScene::createScene();

	Director::getInstance()->replaceScene(TransitionFade::create(TRANSIT_TIME, scene));

}


