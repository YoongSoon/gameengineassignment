#pragma once

#include "cocos2d.h"

using namespace cocos2d;

class SplashScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	void ChangeScene(float dt);

	// implement the "static create()" method manually
	CREATE_FUNC(SplashScene);

};
