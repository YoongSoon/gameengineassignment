#include "SpriteSheetLoader.h"
#include "AppDelegate.h"


Vector<SpriteFrame*> SpriteSheetLoader::GetSpriteFrames(string format, int count)
{
	auto spritecache = SpriteFrameCache::getInstance();
	Vector<SpriteFrame*> animFrames;
	string fileName;

	for (int i = 1; i <= count; i++)
	{
		//For example : fileName == "playerOneMoveRight (1).png"
		fileName = format + " (" + to_string(i) + ")" + ".png";
		animFrames.pushBack(spritecache->getSpriteFrameByName(fileName));
	}
	return animFrames;
}

void SpriteSheetLoader::ScaleNode(Node & nodeObject, float scaleFactor, bool isScaleDown)
{
	if(isScaleDown == false)
	nodeObject.setScale(nodeObject.getScaleX() * scaleFactor, nodeObject.getScaleY() * scaleFactor);
	else
		nodeObject.setScale(nodeObject.getScaleX() / scaleFactor, nodeObject.getScaleY() / scaleFactor);
}

void SpriteSheetLoader::RescaleToScreenSize(Node * node)
{
	//actual window screen size
	Size actualWinSize = Director::getInstance()->getWinSizeInPixels();

	node->setScaleX(node->getScaleX() * (actualWinSize.width / AppDelegate::m_DefaultSceenSize.width));
	node->setScaleY(node->getScaleY() *actualWinSize.height / AppDelegate::m_DefaultSceenSize.height);

}


