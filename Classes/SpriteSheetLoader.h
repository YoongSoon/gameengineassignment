#pragma once

#include <string>
#include "cocos2d.h"

using namespace cocos2d;
using std::string;
using std::to_string;


namespace SpriteSheetLoader
{
	//For example :"playerOneMoveRight (1).png" , where format == playerOneMoveRight
	Vector<SpriteFrame*> GetSpriteFrames(string format, int count);

	//use for scaling button , images and other UI element
	void ScaleNode(Node & nodeObject, float scaleFactor, bool isScaleDown = false);

	//resize UI elements like images , sprites to fit screen size
	//  1st parameter is the node to be rescale
	void RescaleToScreenSize(Node *node);
	
} 