#include "StateDFS.h"
#include "EnemyBase.h"

StateDFS::StateDFS(const string &stateID ,GameObject * go)
	:State(stateID),
	m_go(go)
{
}

StateDFS::~StateDFS()
{
}

void StateDFS::Enter()
{


}

void StateDFS::Update(float dt)
{
	EnemyBase * m_enemy = static_cast<EnemyBase*>(m_go);

	//When moving action ended , there is still sprite animation
	if (m_enemy->getNumberOfRunningActions() == 1) {
		m_enemy->theStrategy->DFSOnce(m_enemy);

		// caculate the next current enum direction
		m_enemy->CaculateTheNewAnimationDir();

		MapHandler * theMapHandler = m_enemy->GetMapHandler();

		//Clear the visited vector once the m_stack is empty(means that all the tiles have been visited)
		// ---TO restart the DFS---
		if (m_enemy->m_stack.empty() == true)
		{
			m_enemy->m_visited.resize(theMapHandler->MAP_WIDTH * theMapHandler->MAP_HEIGHT, false);
			std::fill(m_enemy->m_visited.begin(), m_enemy->m_visited.end(), false);
		}

		//Set the Vec2 pos of the sprite
		m_enemy->pos = theMapHandler->m_Map[m_enemy->m_curr.x][m_enemy->m_curr.y].tileSprite->getPosition();

		m_enemy->SetDirectionAnimation(m_enemy->m_SpriteCurrentDir);

		//Move action 
		float timeToMove = (m_enemy->pos - m_enemy->getPosition()).length() / m_enemy->GetSpeed() ;
		auto moveEvent = MoveBy::create(timeToMove, m_enemy->pos - m_enemy->getPosition());

		m_enemy->runAction(moveEvent);
	}


}

void StateDFS::Exit()
{
}

