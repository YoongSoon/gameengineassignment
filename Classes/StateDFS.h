#pragma once

#include "EnemyBase.h"
#include "StateMachine.h"

class StateDFS : public State
{
	GameObject *m_go;
public:
	StateDFS(const string &stateID ,GameObject *go);
	~StateDFS();

	virtual void Enter();
	virtual void Update(float dt);
	virtual void Exit();

};