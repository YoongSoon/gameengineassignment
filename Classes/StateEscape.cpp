#include "StateEscape.h"
#include "EnemyBase.h"

StateEscape::StateEscape(const string & stateID, GameObject * go)
	:State(stateID),
	m_go(go)
{
}

StateEscape::~StateEscape()
{
}

void StateEscape::Enter()
{
	m_enemy = static_cast<EnemyBase*>(m_go);
}

void StateEscape::Update(float dt)
{

	// Move to next tile only when there is no moving actions(only left the sprite animation actions thats why  getNumberOfRunningActions() == 1)
	if (m_enemy->m_path.empty() == false && m_enemy->getNumberOfRunningActions() == 1)
	{
		// caculate the next current enum direction
		m_enemy->CaculateTheNewAnimationDir();

		//get the next current TilePos of enemy
		m_enemy->m_curr = m_enemy->m_path[0];

		//Check whether the next tile is a danger zone
		if (m_enemy->theStrategy->IsThereImpendingDangerZone(m_enemy->m_curr) == false)
		{
			//Set the Vec2 pos of the sprite
			m_enemy->pos = m_enemy->GetMapHandler()->m_Map[m_enemy->m_curr.x][m_enemy->m_curr.y].tileSprite->getPosition();

			//set the current sprite animation 
			m_enemy->SetDirectionAnimation(m_enemy->m_SpriteCurrentDir);

			//Move action 
			float timeToMove = (m_enemy->pos - m_enemy->getPosition()).length() / m_enemy->GetSpeed();
			auto moveEvent = MoveBy::create(timeToMove, m_enemy->pos - m_enemy->getPosition());
			m_enemy->runAction(moveEvent);

			m_enemy->m_path.erase(m_enemy->m_path.begin());
		}


	}
}

void StateEscape::Exit()
{
}
