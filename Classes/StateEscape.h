#pragma once

#include "EnemyBase.h"
#include "StateMachine.h"

class StateEscape : public State
{
	GameObject *m_go;
	EnemyBase * m_enemy;
public:
	StateEscape(const string &stateID, GameObject *go);
	~StateEscape();

	virtual void Enter();
	virtual void Update(float dt);
	virtual void Exit();

};