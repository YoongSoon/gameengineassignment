#include "StateFireMissile.h"
#include "Missile.h"


StateFireMissile::StateFireMissile(const string &stateID, GameObject * go)
	:State(stateID),
	m_go(go)
{
}

StateFireMissile::~StateFireMissile()
{
}

void StateFireMissile::Enter()
{
	
}

void StateFireMissile::Update(float dt)
{
	EnemyBase * m_enemy = static_cast<EnemyBase*>(m_go);

	//Stop the movement sprite animation when the boss reaches it latest location from the DFS
	if (m_enemy->pos.distance(m_enemy->getPosition()) <= 0.5f)
	{
		m_enemy->stopActionByTag(11);
	}

     //wait for  a short delay before the boss start firing the missile
	if ( b_missileFired == false && m_enemy->getNumberOfRunningActions() == 0)
	{
		m_pastTime += dt;

		if (m_pastTime >= SLIGHT_DELAY)
		{

			//create the missile
			Missile * theMissile = new Missile(m_go->GetMapHandler(), m_go->GetMapHandler()->GetPosFromTilePos(m_enemy->m_curr));

			//Set the player instance in the missile
			theMissile->SetPlayerInst(m_enemy->theStrategy->GetPlayerInstance());

			theMissile->Init();
			theMissile->b_startMoving = true;
			b_missileFired = true;
			m_pastTime = 0.0f;
		}
	}
  	
}

void StateFireMissile::Exit()
{
	b_missileFired = false;
}
