#pragma once

#include "EnemyBase.h"
#include "StateMachine.h"

class Missile;

class StateFireMissile : public State
{
	GameObject *m_go;
	const float SLIGHT_DELAY = 1.5f;
	float m_pastTime = 0.0f;
public:
	StateFireMissile(const string &stateID ,GameObject *go);
	~StateFireMissile();

	virtual void Enter();
	virtual void Update(float dt);
	virtual void Exit();

	//indicator to check whether missile has been fired
	bool b_missileFired = false;
};