#include "StateMachine.h"

StateMachine::StateMachine()
	:m_currentState(nullptr),
	m_nextState(nullptr)
{

}

StateMachine::~StateMachine()
{
	for (map<string, State*>::iterator it = m_stateMap.begin(); it != m_stateMap.end(); ++it)
	{
		delete it->second;
	}
	m_stateMap.clear();

}

void StateMachine::Update(float dt)
{
	if (m_nextState != m_currentState)
	{
		m_currentState->Exit();
		m_currentState = m_nextState;
		m_currentState->Enter();
	}

	m_currentState->Update(dt);
}

void StateMachine::AddState(State * newState)
{
	if (!newState) 	//newState is nullptr 
		return;
	if (m_stateMap.find(newState->GetStateID()) != m_stateMap.end()) //newState does not exists 
		return;
	if (!m_currentState)
		m_currentState = m_nextState = newState;

	//Otherwise insert into the map container
	m_stateMap.insert(std::pair<string, State*>(newState->GetStateID(), newState));
}

void StateMachine::SetNextState(const string & nextStateID)
{
	map<string, State*>::iterator it = m_stateMap.find(nextStateID);
	if (it != m_stateMap.end())
	{
		m_nextState = (State *)it->second;
	}
}

const string & StateMachine::GetCurrentState()
{
	if (m_currentState)
		return m_currentState->GetStateID();
	return "<No states>";
}

State * StateMachine::GetCurrentStatePointer()
{
	return m_currentState;
}

