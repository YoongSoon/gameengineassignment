#pragma once

#include <map>
#include <string>

using std::map;
using std::string;

class State
{
private:
	const string m_stateID;
protected:
	State(const string &stateID) :m_stateID(stateID){}
public:
	virtual ~State(){}
	const string& GetStateID() { return m_stateID; }

	//To be implemented by concrete states
	virtual void Enter() = 0;
	virtual void Update(float dt) = 0;
	virtual void Exit() = 0;
};


class StateMachine
{
private:
	State * m_currentState;
	State * m_nextState;
	map<string, State*> m_stateMap;
public:
	StateMachine();
	~StateMachine();

	void AddState(State *newState);
	void SetNextState(const string &nextStateID);

	void Update(float dt);

	const string & GetCurrentState();
	State * GetCurrentStatePointer();

};

