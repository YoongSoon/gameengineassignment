#include "Weather.h"
#include "SpriteSheetLoader.h"


Weather::Weather(Scene* currentScene, string weatherName, string weatherIconPath, string weatherPlistPath) : WeatherName(weatherName) {

	m_Scene = currentScene;

	auto m_GameSize = Director::getInstance()->getWinSize();

	// Weather Text and Icon
	if (weatherIconPath != "") {
		m_WeatherText = Label::createWithTTF(WeatherName, "fonts/Marker Felt.ttf", 10);
		m_WeatherText->setAlignment(TextHAlignment::CENTER);
		m_WeatherText->setPosition(Vec2(m_GameSize.width * 0.065f, m_GameSize.height * 0.8f));
		m_WeatherText->setVisible(false); // Hide the texture first

		m_WeatherIcon = Sprite::create(weatherIconPath);
		m_WeatherIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_WeatherIcon->setPositionNormalized(Vec2(0.5f, 2.5f));
		m_WeatherIcon->runAction(RepeatForever::create(Sequence::createWithTwoActions(ScaleTo::create(0.4f, 1.05f), ScaleTo::create(0.4f, 0.95f))));
		
		currentScene->addChild(m_WeatherText);
		m_WeatherText->addChild(m_WeatherIcon);
	}

	// Weather Particles
	if (weatherPlistPath != "") {
		m_WeatherParticle = ParticleSystemQuad::create(weatherPlistPath);
		m_WeatherParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_WeatherParticle->setPosition(Vec2(m_GameSize.width * 0.5f, m_GameSize.height * 0.5f));
		m_WeatherParticle->stop();

		currentScene->addChild(m_WeatherParticle);
	}

}

void Weather::OnWeatherStart(Player* player) {
	if (m_WeatherText)
		m_WeatherText->setVisible(true);

	if (m_WeatherParticle)
		m_WeatherParticle->start();
}

void Weather::OnWeatherUpdate(Player* player, float dt) {

}

void Weather::OnWeatherExit(Player* player) {
	if (m_WeatherText)
		m_WeatherText->setVisible(false);

	if (m_WeatherParticle)
		m_WeatherParticle->stop();
}
