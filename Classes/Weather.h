#pragma once

#include <string>
#include "cocos2d.h"
#include "Player.h"

using std::string;
USING_NS_CC;

class Weather {

public:
	// arg weatherIconPath: Sprite Icon to be rendered in player's GUI
	// arg weatherPlistPath: .plist file for the particle effect of the weather
	Weather(Scene* currentScene, string weatherName, string weatherIconPath = "", string weatherPlistPath = "");
	virtual ~Weather() {}

	virtual void OnWeatherStart(Player* player);
	virtual void OnWeatherUpdate(Player* player, float dt);
	virtual void OnWeatherExit(Player* player);

	const string WeatherName;
	
protected:
	Scene* m_Scene = nullptr;

	Sprite* m_WeatherIcon = nullptr;
	Label* m_WeatherText = nullptr;
	ParticleSystem* m_WeatherParticle = nullptr;
		
};