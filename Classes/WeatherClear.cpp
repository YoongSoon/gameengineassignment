#include "WeatherClear.h"
#include "MapHandler.h"
#include "GameObject.h"
#include "Player.h"

WeatherClear::WeatherClear(Scene* currentScene) : Weather(currentScene, "Clear", "Weather/icon_clear.png", "") {
}

void WeatherClear::OnWeatherStart(Player* player) {
	Weather::OnWeatherStart(player);
}

void WeatherClear::OnWeatherUpdate(Player* player, float dt) {
	Weather::OnWeatherUpdate(player, dt);	
}

void WeatherClear::OnWeatherExit(Player* player) {
	Weather::OnWeatherExit(player);
}
