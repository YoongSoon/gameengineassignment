#pragma once

#include "Weather.h"

class WeatherClear : public Weather {

public:
	WeatherClear(Scene* currentScene);
	virtual ~WeatherClear() {}

	virtual void OnWeatherStart(Player* player) ;
	virtual void OnWeatherUpdate(Player* player, float dt);
	virtual void OnWeatherExit(Player* player);


};