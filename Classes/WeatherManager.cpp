#include "WeatherManager.h"
#include "WeatherRain.h"
#include "WeatherClear.h"
#include "WeatherSunny.h"
#include "WeatherWindy.h"
#include "WeatherSnow.h"


WeatherManager::~WeatherManager() {
	for (auto it : m_WeatherList) {
		delete it;
	}
}

void WeatherManager::Init(Scene* currentScene, Player * player) {
	m_Player = player;

	m_WeatherList.push_back(new WeatherClear(currentScene));
	m_WeatherList.push_back(new WeatherRain(currentScene));
	m_WeatherList.push_back(new WeatherSunny(currentScene));
	m_WeatherList.push_back(new WeatherWindy(currentScene));
	m_WeatherList.push_back(new WeatherSnow(currentScene));

	m_WeatherPlayList.push_back("");
	m_WeatherPlayList.push_back("Audio/Rain.mp3");
	m_WeatherPlayList.push_back("Audio/Sunny.mp3");
	m_WeatherPlayList.push_back("Audio/Wind.mp3");
	m_WeatherPlayList.push_back("Audio/Snow.mp3");

	ChangeWeather("Clear");

	m_NextWeatherTime = m_ElapsedTime + M_WEATHER_INTERVAL;
}

void WeatherManager::Update(float dt) {	
	m_ElapsedTime += dt;

	// Change weather every X seconds
	if (m_ElapsedTime >= m_NextWeatherTime) {

		m_NextWeatherTime = m_ElapsedTime + M_WEATHER_INTERVAL;
		
		for (int a = 0; a < WeatherBGM_IDs.size(); a++)
			experimental::AudioEngine::stop(WeatherBGM_IDs[a]);

		if (m_CurrentWeather) {			
			// Bounce back and forth between clear and non-clear weather
			if (m_CurrentWeather->WeatherName == "Clear")
				RandomizeWeather();
			else
				ChangeWeather("Clear");
		}
	}
	
	if (m_CurrentWeather)
		m_CurrentWeather->OnWeatherUpdate(m_Player, dt);
}

void WeatherManager::ChangeWeather(string weatherName) {
	if (m_CurrentWeather)
		m_CurrentWeather->OnWeatherExit(m_Player);

	Weather* weather = GetWeather(weatherName);

	if (weather != nullptr) {
		m_CurrentWeather = weather;
		weather->OnWeatherStart(m_Player);
		log("Weather changed to: %s", weather->WeatherName.c_str());
	}
	else {
		log("Can't find the given weather\n");
	}
}

Weather* WeatherManager::GetWeather(string weatherName) {
	for (auto weather : m_WeatherList) {
		if (weather->WeatherName == weatherName)
			return weather;
	}

	return nullptr;
}

void WeatherManager::RandomizeWeather(bool allowSameWeather) {
	if (m_WeatherList.empty())
		return;

	int randNum;

	if (allowSameWeather || m_CurrentWeather == nullptr) {
		randNum = RandomHelper::random_int<int>(1, m_WeatherList.size() - 1);
	}
	else {
		do {
			randNum = RandomHelper::random_int<int>(1, m_WeatherList.size() - 1);
		} while (m_WeatherList[randNum]->WeatherName == m_CurrentWeather->WeatherName);
	}
	m_CurrentWeatherMusic = m_WeatherPlayList[randNum];
	AudioManagerManager::tempid = experimental::AudioEngine::play2d(m_CurrentWeatherMusic, true, AudioManagerManager::GlobalBGMvalue);
	for (int a = 0; a < WeatherBGM_IDs.size(); a++)
		experimental::AudioEngine::stop(WeatherBGM_IDs[a]);
	if (AudioManagerManager::tempid != -1)
		WeatherBGM_IDs.push_back(AudioManagerManager::tempid);
	ChangeWeather(m_WeatherList[randNum]->WeatherName);
}
