#pragma once

#include "cocos2d.h"
#include "AudioEngine.h"
#include "AudioManagerManager.h"
USING_NS_CC;
using std::string;
using std::vector;

class Player;
class Weather;

class WeatherManager {

public:
	WeatherManager() {}
	~WeatherManager();

	void Init(Scene* currentScene, Player* player);

	void Update(float dt);

	void ChangeWeather(string weatherName);

	Weather* GetWeather(string weatherName);

	// Changes the current weather to a new weather
	// arg allowSameWeather: If TRUE, allows randomizer to generate the weather as the current one
	void RandomizeWeather(bool allowSameWeather = false);

	// Changes the weather every X seconds
	const float M_WEATHER_INTERVAL = 10;

private:
	Player* m_Player;

	Weather* m_CurrentWeather;
	const char* m_CurrentWeatherMusic;

	vector<Weather*> m_WeatherList;
	vector<const char*> m_WeatherPlayList;

	float m_NextWeatherTime = 0;
	float m_ElapsedTime = 0;

	vector<int> WeatherBGM_IDs;

};