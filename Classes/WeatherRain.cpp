#include "WeatherRain.h"
#include "MapHandler.h"
#include "GameObject.h"
#include "Player.h"

WeatherRain::WeatherRain(Scene* currentScene) : Weather(currentScene, "Rain", "Weather/icon_rain.png", "Weather/particle_rain.plist") {

}

void WeatherRain::OnWeatherStart(Player* player) {
	Weather::OnWeatherStart(player);


}

void WeatherRain::OnWeatherUpdate(Player* player, float dt) {
	Weather::OnWeatherUpdate(player, dt);

	auto bombs = player->m_Map->FindObjectsOfName("bomb");

	// Freeze all player controlled bombs
	if (!bombs.empty()) {
		for (auto it : bombs) {
			it->setColor(Color3B(0, 200, 255)); // change color while frozen lulz
			it->pause();
		}
	}

}

void WeatherRain::OnWeatherExit(Player* player) {
	Weather::OnWeatherExit(player);

	auto bombs = player->m_Map->FindObjectsOfName("bomb");

	// Resume all frozen bombs
	if (!bombs.empty()) {
		for (auto it : bombs) {
			it->setColor(Color3B(255, 255, 255));
			it->resume();
		}
	}
}
