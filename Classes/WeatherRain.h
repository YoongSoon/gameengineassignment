#pragma once

#include "Weather.h"

class WeatherRain : public Weather {

public:
	WeatherRain(Scene* currentScene);
	virtual ~WeatherRain() {}

	virtual void OnWeatherStart(Player* player) ;
	virtual void OnWeatherUpdate(Player* player, float dt);
	virtual void OnWeatherExit(Player* player);


};