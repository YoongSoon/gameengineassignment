#include "WeatherSnow.h"
#include "MapHandler.h"
#include "GameObject.h"
#include "EnemyBase.h"
#include "Player.h"


WeatherSnow::WeatherSnow(Scene* currentScene) : Weather(currentScene, "Snow", "Weather/icon_snow.png", "Weather/particle_snow.plist") {
}

void WeatherSnow::OnWeatherStart(Player* player) {
	Weather::OnWeatherStart(player);

	const Color3B freezeColor(51, 255, 255);

	// Slows all enemy and the player
	auto enemies = player->m_Map->FindObjectsOfName("Enemy", true);

	for (auto it : enemies) {
		EnemyBase* enemy = static_cast<EnemyBase*>(it);
		enemy->SetSpeed(enemy->GetSpeed() * M_SNOW_SPEED_LOSS_PERCENT);
		enemy->setColor(freezeColor);
	}

	player->SetVelocityCap(1 - M_SNOW_SPEED_LOSS_PERCENT);
	player->GetPlayerSprite()->setColor(freezeColor);

}

void WeatherSnow::OnWeatherUpdate(Player* player, float dt) {
	Weather::OnWeatherUpdate(player, dt);
}

void WeatherSnow::OnWeatherExit(Player* player) {
	Weather::OnWeatherExit(player);

	// Reset everyone's speed and color
	auto enemies = player->m_Map->FindObjectsOfName("Enemy", true);

	for (auto it : enemies) {
		EnemyBase* enemy = static_cast<EnemyBase*>(it);
		enemy->SetSpeed(enemy->GetSpeed() / M_SNOW_SPEED_LOSS_PERCENT);
		enemy->setColor(Color3B(255, 255, 255));
	}

	player->SetVelocityCap(1);
	player->GetPlayerSprite()->setColor(Color3B(255, 255, 255));
}
