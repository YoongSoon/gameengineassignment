#pragma once

#include "Weather.h"

class WeatherSnow : public Weather {

public:
	WeatherSnow(Scene* currentScene);
	virtual ~WeatherSnow() {}

	virtual void OnWeatherStart(Player* player);
	virtual void OnWeatherUpdate(Player* player, float dt);
	virtual void OnWeatherExit(Player* player);

	// How much percentage speed to reduce when weather is active?
	const float M_SNOW_SPEED_LOSS_PERCENT = 0.5f;

};