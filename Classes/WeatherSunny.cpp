#include "WeatherSunny.h"
#include "MapHandler.h"
#include "GameObject.h"
#include "Player.h"

WeatherSunny::WeatherSunny(Scene* currentScene) : Weather(currentScene, "Sunny", "Weather/icon_sunny.png", "Weather/particle_sunny.plist") {

}

void WeatherSunny::OnWeatherStart(Player* player) {
	Weather::OnWeatherStart(player);


}

void WeatherSunny::OnWeatherUpdate(Player* player, float dt) {
	Weather::OnWeatherUpdate(player, dt);

	auto playerBombs = player->m_Map->FindObjectsOfName("bomb");

	if (!playerBombs.empty()) {
		for (auto it : playerBombs) {
			it->setColor(Color3B(255, 120, 0));
			static_cast<Bomb*>(it)->SetSunnyBomb(true);
		}
	}

}

void WeatherSunny::OnWeatherExit(Player* player) {
	Weather::OnWeatherExit(player);

	auto playerBombs = player->m_Map->FindObjectsOfName("bomb");

	if (!playerBombs.empty()) {
		for (auto it : playerBombs) {
			it->setColor(Color3B::WHITE);
			static_cast<Bomb*>(it)->SetSunnyBomb(false);
		}
	}
}
