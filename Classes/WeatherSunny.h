#pragma once

#include "Weather.h"

class WeatherSunny : public Weather {

public:
	WeatherSunny(Scene* currentScene);
	virtual ~WeatherSunny() {}

	virtual void OnWeatherStart(Player* player) ;
	virtual void OnWeatherUpdate(Player* player, float dt);
	virtual void OnWeatherExit(Player* player);


};