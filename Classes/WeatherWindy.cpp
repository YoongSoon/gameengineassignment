#include "WeatherWindy.h"
#include "MapHandler.h"
#include "GameObject.h"
#include "Player.h"

WeatherWindy::WeatherWindy(Scene* currentScene) : Weather(currentScene, "Windy", "Weather/icon_windy.png", "Weather/particle_windy.plist") {

}

void WeatherWindy::OnWeatherStart(Player* player) {
	Weather::OnWeatherStart(player);
	m_WindDirection = (WIND_DIRECTION)RandomHelper::random_int<int>(DIR_EAST, DIR_NORTH);

	float windAngle = GetWindAngle();

	m_WeatherIcon->setRotation(windAngle);
	m_WeatherParticle->setRotation(windAngle);
}

void WeatherWindy::OnWeatherUpdate(Player* player, float dt) {
	Weather::OnWeatherUpdate(player, dt);

	auto bombs = player->m_Map->FindObjectsOfName("bomb");

	// Freeze all player controlled bombs
	if (!bombs.empty()) {

		float windAngle = -GetWindAngle();
		const float offsetLength = 16;

		for (auto it : bombs) {
			
			// Calculate the offset's direction
			Vec2 offset = it->getPosition().getNormalized();
			offset.x = offsetLength * cosf(CC_DEGREES_TO_RADIANS(windAngle));
			offset.y = offsetLength * sinf(CC_DEGREES_TO_RADIANS(windAngle));

			// Only push the bomb if the next offset is a walkable tile
			MapTile* nextTile = player->m_Map->GetTileFromPos(it->getPosition() + (it->getContentSize() / 2) + (offset));

			if (nextTile->tileType == MapTile::TILE_FLOOR) {
				it->setPosition(it->getPosition() + (offset * dt));
			}

		}
	}

}

void WeatherWindy::OnWeatherExit(Player* player) {
	Weather::OnWeatherExit(player);
}

float WeatherWindy::GetWindAngle() {
	float windAngle = 0;

	for (int i = 0; i < m_WindDirection; ++i) {
		windAngle += 90;
	}
	return windAngle;
}