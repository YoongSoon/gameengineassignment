#pragma once

#include "Weather.h"

class WeatherWindy : public Weather {

public:
	WeatherWindy(Scene* currentScene);
	virtual ~WeatherWindy() {}

	virtual void OnWeatherStart(Player* player) ;
	virtual void OnWeatherUpdate(Player* player, float dt);
	virtual void OnWeatherExit(Player* player);
	
private:
	enum WIND_DIRECTION {
		DIR_EAST = 0,
		DIR_SOUTH,
		DIR_WEST,
		DIR_NORTH
	};

	WIND_DIRECTION m_WindDirection;

	float GetWindAngle();

};
